﻿using BusinessLogic.Controllers;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Utils;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace WebChepiApi.Controllers
{
    public class AlertasController : ApiController
    {
        [System.Web.Http.ActionName("todas")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            try
            {
                if (new ControllerToken().DecipherToken(request.Headers) == null)
                {
                    throw new UnauthorizedAccessException("Access denied");
                }
                
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerAlerta().GetAlertasAll() }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }

        // GET: api/admins/filtrar
        [System.Web.Http.ActionName("filtrar")]
        public HttpResponseMessage Get(HttpRequestMessage request, Dictionary<string, object> data)
        {
            try
            {
                if (new ControllerToken().DecipherToken(request.Headers) == null)
                {
                    throw new UnauthorizedAccessException("Access denied");
                }
                
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerAlerta().GetAlertasFiltradas(data) }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }

        // GET api/recicladores?nickname={nickname}
        public HttpResponseMessage Get(HttpRequestMessage request, long id, string nickname)
        {
            return null;
        }

        // POST api/values
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]Dictionary<string,object> data)
        {
            try
            {
                long id = (long)data["id"];
                new ControllerToken().GrantRecicladorAccess(request.Headers);
                UserData userData = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter);
                Dictionary<string, object> claveAlerta = new ControllerAlerta().AddAlerta(id, userData.nickname);
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (ArgumentNullException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (ArgumentException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            
        }

        // DELETE api/values?nickname={nickname}
        public HttpResponseMessage Delete(HttpRequestMessage request)
        {
            return null;
        }
    }
}