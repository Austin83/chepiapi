﻿using BusinessLogic.Controllers;
using CrossCutConcerns.Utils;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace WebChepiApi.Controllers
{
    public class ContenedoresController : ApiController
    {
        // GET: api/contenedores
        [System.Web.Http.ActionName("todos")]
        public HttpResponseMessage GetTodos(HttpRequestMessage request, string materiales = null)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantBasicAccess(request.Headers);
                UserData userdata = controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter);
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerContenedor().GetContenedoresAll(userdata, materiales) }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
        }

        // GET: api/admins/filtrar
        [System.Web.Http.ActionName("filtrar")]
        public HttpResponseMessage Get(HttpRequestMessage request, Dictionary<string, object> data)
        {
            try
            {
                if (new ControllerToken().DecipherToken(request.Headers) == null)
                {
                    throw new UnauthorizedAccessException("Access denied");
                }
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerContenedor().GetContenedoresFiltrados(data) }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        public HttpResponseMessage Get(HttpRequestMessage request, long id)
        {
            try
            {
                return request.CreateResponse(HttpStatusCode.OK, new ControllerContenedor().GetContenedor(id));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
        }
        
        public HttpResponseMessage Get(HttpRequestMessage request, string nickname)
        {
            try
            {
                return request.CreateResponse(HttpStatusCode.OK, new ControllerContenedor().GetContenedoresByAdmin(nickname));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
        }
        
        public HttpResponseMessage Post(HttpRequestMessage request, Dictionary<string,object> data )
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantAdminAccess(request.Headers);
                long id = new ControllerContenedor().AddContenedor(controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname,data);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (ArgumentNullException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [System.Web.Http.ActionName("editar")]
        public HttpResponseMessage Patch(HttpRequestMessage request, [FromBody] Dictionary<string,object> data)
        {
            try
            {
                new ControllerContenedor().UpdateContenedor(data);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
        }
        
        public HttpResponseMessage Delete(HttpRequestMessage request, [FromBody]long id)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                new ControllerContenedor().GetContenedor(id);
                new ControllerContenedor().RemoveContenedor(id);

                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (ArgumentNullException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [System.Web.Http.ActionName("alertados")]
        public HttpResponseMessage GetAlertados(HttpRequestMessage request, string materiales = null)
        {
            try
            {
                new ControllerToken().GrantBasicAccess(request.Headers);
                return request.CreateResponse(HttpStatusCode.OK, new ControllerContenedor().GetContenedoresAlertados(materiales));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
        }

        [System.Web.Http.ActionName("llenar")]
        public HttpResponseMessage PatchAlertados(HttpRequestMessage request, Dictionary<string,object> data)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                new ControllerContenedor().LlenarAlertado(data);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
        }
    }
}