﻿using BusinessLogic.Controllers;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Utils;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace WebChepiApi.Controllers
{
    public class UsuariosController : ApiController
    {
        [System.Web.Http.ActionName("login")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]Dictionary<string,object> login)
        {
            try
            {
                List<string> palabras = new List<string>
                {
                     (string)login["key"],
                     (string)login["value"],
                     (string)login["contra"]
                };

                if (new UtilsHelper().UselessStrings(palabras))
                {
                    throw new KeyNotFoundException("Credenciales incompletas");
                }

                string token = new ControllerUsuario().Login(login);
                if (token == null)
                {
                    throw new UnauthorizedAccessException("Access denied!");
                }

                Dictionary<string, object> resultData = new Dictionary<string, object>
                {
                    { "token", token },
                    { "payload", new ControllerToken().DecipherTokenPayload(token) }
                };
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", resultData }
                };
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }

        [System.Web.Http.ActionName("login_google")]
        public HttpResponseMessage PostGoogle(HttpRequestMessage request, [FromBody]Dictionary<string,object> data)
        {
            try
            {
                string chepitoken = new ControllerUsuario().LoginGoogle(data);
                if (chepitoken == null)
                {
                    throw new UnauthorizedAccessException("Access denied!");
                }
                
                return Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>
                {
                    { "token", chepitoken },
                    { "payload", new ControllerToken().DecipherTokenPayload(chepitoken) }
                });
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        public HttpResponseMessage Get( HttpRequestMessage request, string palabras = null, int? edad = null, int? tipo = null)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                return request.CreateResponse(HttpStatusCode.OK, new ControllerUsuario().GetUsuariosAll(palabras, edad, tipo));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        
        public HttpResponseMessage Get(HttpRequestMessage request, string key, string value)
        {
            try
            {
                List<string> palabras = new List<string>
                {
                     key,
                     value
                };

                if (new UtilsHelper().UselessStrings(palabras))
                {
                    throw new KeyNotFoundException("Argumentos incompletos");
                }

                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerUsuario().ExisteUsuario(key,value) }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }

        [System.Web.Http.ActionName("Bloquear")]
        public HttpResponseMessage Patch(HttpRequestMessage request, [FromBody]Dictionary<string,object> data)
        {
            try
            {
                string nickname_bloqueado = (string)data["nickname"];
                if ( new UtilsHelper().UselessString( nickname_bloqueado ))
                {
                    throw new ArgumentNullException("nickname");
                }
                new ControllerToken().GrantCommonAccess(request.Headers);
                UserData userData = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter);
                new ControllerUsuario().Bloquear(userData.nickname, nickname_bloqueado);

                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerUsuario().GetBloqueo(userData.nickname, nickname_bloqueado) }
                };
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }

        [System.Web.Http.ActionName("Desbloquear")]
        public HttpResponseMessage PatchDes(HttpRequestMessage request, [FromBody]Dictionary<string,object> data)
        {
            try
            {
                string nickname_bloqueado = (string)data["nickname"];
                if (new UtilsHelper().UselessString(nickname_bloqueado))
                {
                    throw new ArgumentNullException("nickname");
                }

                new ControllerToken().GrantCommonAccess(request.Headers);
                UserData userData = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter);
                
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerUsuario().Desbloquear(userData.nickname, nickname_bloqueado) }
                };
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }

        [System.Web.Http.ActionName("Actualizar")]
        public HttpResponseMessage PatchActualizar(HttpRequestMessage request, [FromBody] Dictionary<string, object> data)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantCommonAccess(request.Headers);
                string nickname = controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;
                new ControllerUsuario().UpdateUsuario( nickname , data);
                return request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }
    }
}