﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic.Controllers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Net;
using CrossCutConcerns.Utils;
using System.Net.Http.Headers;
using CrossCutConcerns.Constants;
using System.Web.Http.Cors;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace WebChepiApi.Controllers
{
    public class RecicladoresController : ApiController
    {
        [System.Web.Http.ActionName("todos")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            try
            {
                new ControllerToken().GrantCommonAccess( request.Headers );
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerReciclador().GetRecicladoresAll() }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        [System.Web.Http.ActionName("mis_recolectores")]
        public HttpResponseMessage GetMisRecolectores(HttpRequestMessage request, bool? nicknames_only = null)
        {
            try
            {
                new ControllerToken().GrantRecicladorAccess(request.Headers);
                string nickname =  new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;
                Dictionary<string, object> result = new Dictionary<string, object>();

                if( nicknames_only != null && nicknames_only == true)
                {
                    result.Add("success", true);
                    result.Add( "data", new ControllerRecolector().GetMisRecolectores(nickname, (bool)nicknames_only));
                }
                else
                {
                    result.Add("success", true);
                    result.Add("data", new ControllerRecolector().GetMisRecolectores(nickname));
                }
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        public HttpResponseMessage Get(HttpRequestMessage request, string nickname)
        {
            try
            {
                if (new UtilsHelper().UselessString(nickname))
                {
                    throw new KeyNotFoundException("nickname is null !");
                }
                new ControllerToken().GrantCommonAccess(request.Headers);
                UserData userData = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter);
                
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerReciclador().GetReciclador(nickname, userData) }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        [System.Web.Http.ActionName("filtrar")]
        public HttpResponseMessage Get(HttpRequestMessage request, Dictionary<string,object> data)
        {
            try
            {
                new ControllerToken().GrantCommonAccess(request.Headers);
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerReciclador().GetRecicladoresFiltrados( data ) }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        [System.Web.Http.ActionName("registrar")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]Dictionary<string, object> data)
        {
            try
            {
                List<string> palabras = new List<string>
                {
                    (string)data["nickname"],
                    (string)data["nombre"],
                    (string)data["apellido"],
                    (string)data["mail"],
                    (string)data["contra"],
                };

                List<object> resto = new List<object>
                {
                    byte.Parse( data["edad"].ToString() )
                };
                UtilsHelper utils = new UtilsHelper();
                if (utils.UselessStrings(palabras) || utils.AnyNulls(resto))
                {
                    throw new ArgumentException("Invalid arguments in body");
                }

                string nickname = new ControllerReciclador().AddReciclador(data);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ArgumentNullException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (ArgumentException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (InvalidCastException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        // DELETE api/recicladores?nickname={nickname}
        public HttpResponseMessage Delete(HttpRequestMessage request, string nickname)
        {
            try
            {
                new ControllerToken().GrantRecicladorAccess(request.Headers);
                //new ControllerToken().IdentityCheck(token, nickname);
                UserData userData = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter);
                new ControllerReciclador().GetReciclador(nickname, userData);
                new ControllerReciclador().RemoveReciclador(nickname);

                return request.CreateResponse(HttpStatusCode.OK, "Reciclador de nickname ={ " + nickname + " } fue borrado exitosamente.");
            }
            catch (Exception ex)
            {

                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
        }

        [System.Web.Http.ActionName("top")]
        public HttpResponseMessage GetTopRecicladores(HttpRequestMessage request, string nickname = null)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                return request.CreateResponse(HttpStatusCode.OK, new ControllerReciclador().GetTopRecicladores(nickname));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}