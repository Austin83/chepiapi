﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CrossCutConcerns.Utils;
using CrossCutConcerns.Constants;
using BusinessLogic.Controllers;
using System.Net.Http;
using System.Web.Http.Description;
using System.Net;
using System.Net.Http.Headers;
using Microsoft.IdentityModel.Tokens;

namespace WebChepiApi.Controllers
{
    public class AdminsController : ApiController
    {
        
        [System.Web.Http.ActionName("todos")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                List<Dictionary<string,object>> data = new ControllerAdministrador().GetAdministradoresAll();
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", data }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        [System.Web.Http.ActionName("filtrar")]
        public HttpResponseMessage Get(HttpRequestMessage request, Dictionary<string, object> data)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                List<Dictionary<string,object>> admins = new ControllerAdministrador().GetAdministradoresFiltrados(data);
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", admins }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        public HttpResponseMessage Get(HttpRequestMessage request, string nickname)
        {
            try
            {
                if (new UtilsHelper().UselessString(nickname))
                {
                    throw new KeyNotFoundException("nickname is null !");
                }
                new ControllerToken().GrantAdminAccess(request.Headers);
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerAdministrador().GetAdministrador(nickname) }
                };
                return request.CreateResponse(HttpStatusCode.OK, result );
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }

        [System.Web.Http.ActionName("login")]
        public HttpResponseMessage Login(HttpRequestMessage request, [FromBody]Dictionary<string, object> data)
        {
            try
            {
                List<string> palabras = new List<string>
                {
                     (string)data["key"],
                     (string)data["value"],
                     (string)data["contra"]
                };

                if (new UtilsHelper().UselessStrings(palabras))
                {
                    throw new KeyNotFoundException("Credenciales incompletas");
                }

                string token = new ControllerAdministrador().Login( data );
                if ( token == null )
                {
                    throw new UnauthorizedAccessException("Access denied!");
                }
                Dictionary<string, object> resultData = new Dictionary<string, object>
                {
                    { "token", token },
                    { "payload", new ControllerToken().DecipherTokenPayload(token) }
                };
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", resultData }
                };
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        [System.Web.Http.ActionName("registrar")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]Dictionary<string,object> data)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                List<string> palabras = new List<string>
                {
                    (string)data["nickname"],
                    (string)data["mail"],
                    (string)data["contra"]
                };

                if (new UtilsHelper().UselessStrings(palabras))
                {
                    throw new ArgumentException("Empty or null strings");
                }
                string nickname = new ControllerAdministrador().AddAdministrador(data);
                Dictionary<string,object> admin = new ControllerAdministrador().GetAdministrador(nickname);
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", admin }
                };
                return request.CreateResponse(HttpStatusCode.OK, result );
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (ArgumentNullException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (ArgumentException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        public HttpResponseMessage Patch(HttpRequestMessage request, [FromBody]Dictionary<string,object> data )
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                UserData userData = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter);
                data.Add("nickname", userData.nickname);
                new ControllerAdministrador().UpdateAdministrador(data);
                Dictionary<string, object> admin = new ControllerAdministrador().GetAdministrador(userData.nickname);
                Dictionary<string, object> aux = new Dictionary<string, object>
                {
                    { "mail", admin["mail"] }
                };

                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", aux }
                };

                return request.CreateResponse(HttpStatusCode.OK, result );

            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (ArgumentNullException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (ArgumentException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        public HttpResponseMessage Delete(HttpRequestMessage request)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                UserData userData = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter);
                new ControllerAdministrador().RemoveAdministrador(userData.nickname);
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true }
                };
                return request.CreateResponse(HttpStatusCode.OK, result );
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (ArgumentNullException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (ArgumentException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }

        [System.Web.Http.ActionName("Bloquear")]
        public HttpResponseMessage PatchBloquear(HttpRequestMessage request, [FromBody] Dictionary<string,object> data)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                new ControllerAdministrador().Bloquear(data);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [System.Web.Http.ActionName("Desbloquear")]
        public HttpResponseMessage PatchDesbloquear(HttpRequestMessage request, [FromBody] Dictionary<string, object> data)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                new ControllerAdministrador().Desbloquear(data);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}