﻿using BusinessLogic.Controllers;
using CrossCutConcerns.Utils;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace WebChepiApi.Controllers
{
    public class UbicacionesController : ApiController
    {
        [System.Web.Http.ActionName("todas")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            try
            {
                new ControllerToken().GrantCommonAccess(request.Headers);
                return request.CreateResponse(HttpStatusCode.OK, new UtilsHelper().CreateSuccessfulDictionary(new ControllerUbicacion().GetUbicacionesAll()));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }
        
        [System.Web.Http.ActionName("filtrar")]
        public HttpResponseMessage Get(HttpRequestMessage request, long? id = null, string nickname_reciclador = null,
                                                                   long? id_contenedor = null, string nombre = null,
                                                                   string direccion = null, double? latitud = null,
                                                                   double? longitud = null, string rango = null)
        {
            try
            {
                new ControllerToken().GrantCommonAccess(request.Headers);
                Dictionary<string, object> data = new Dictionary<string, object>
                {
                    { "id", id },
                    { "nickname_reciclador", nickname_reciclador },
                    { "id_contenedor", id_contenedor },
                    { "nombre", nombre },
                    { "direccion", direccion },
                    { "latitud", latitud },
                    { "longitud", longitud },
                    { "string", rango }
                };
                return request.CreateResponse(HttpStatusCode.OK,new UtilsHelper().CreateSuccessfulDictionary(new ControllerUbicacion().GetUbicacionesFiltradas(data)));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message) );
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }
        
        public HttpResponseMessage Get(HttpRequestMessage request, long id)
        {
            try
            {
                new ControllerToken().GrantCommonAccess(request.Headers);
                return request.CreateResponse(HttpStatusCode.OK, new UtilsHelper().CreateSuccessfulDictionary(new ControllerUbicacion().GetUbicacion(id)));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }
        
        [System.Web.Http.ActionName("test")]
        public HttpResponseMessage GetTest(HttpRequestMessage request, [FromBody]Dictionary<string, object>data)
        {
            try
            {
                Coordinates a = new Coordinates((double)data["a_latitude"], (double)data["a_longitude"]);
                Coordinates b = new Coordinates((double)data["b_latitude"], (double)data["b_longitude"]);
                double distancia = new ControllerUbicacion().GetDistance(a, b);
                string mensaje = null;

                string radioText = null;
                double? radio = null;
                if ( data.ContainsKey("radio") )
                {
                    radioText = data["radio"].ToString();
                    bool cond = double.TryParse(radioText + ".00", out double aux);
                    if( cond )
                    {
                        radio = aux;
                    }
                }
                else
                {
                    throw new ArgumentNullException("radio is null!");
                }

                if (distancia < double.Parse(data["radio"].ToString() + ".00")) 
                {
                    mensaje = "En rango";
                }
                else
                {
                    mensaje = "Fuera de rango";
                }
                return request.CreateResponse(HttpStatusCode.OK, mensaje);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
        }
        
        [System.Web.Http.ActionName("recicladores")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody] Dictionary<string, object> data)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantCommonAccess(request.Headers);

                if (data.ContainsKey("id_contenedor"))
                {
                    throw new ArgumentException("id_contenedor is an invalid parameter !");
                }

                List<string> palabras = new List<string>
                {
                    (string)data["nombre"],
                    (string)data["direccion"]
                };

                if(new UtilsHelper().EmptyStrings(palabras))
                {
                    throw new ArgumentException("Empty string");
                }
                string nickname = controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;
                data.Add("nickname_reciclador", nickname);
                long id = new ControllerUbicacion().AddUbicacion(data);
                new ControllerUbicacion().GetUbicacion(id);
                return request.CreateResponse(HttpStatusCode.OK, new UtilsHelper().CreateSuccessfulDictionary(id) );
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (ArgumentNullException ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (ArgumentException ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }

        }
        
        [System.Web.Http.ActionName("recicladores")]
        public HttpResponseMessage DeleteByRecicladores(HttpRequestMessage request, [FromBody]Dictionary<string,object> data)
        {
            try
            {
                new ControllerToken().GrantRecicladorAccess(request.Headers);
                UserData userData = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter);
                new ControllerUbicacion().RemoveUbicacion(data, userData.nickname);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (ArgumentNullException ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (ArgumentException ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }

        }
    }
}