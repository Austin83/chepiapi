﻿using BusinessLogic.Controllers;
using CrossCutConcerns.Utils;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace WebChepiApi.Controllers
{
    public class MaterialesController : ApiController
    {
        [System.Web.Http.ActionName("todos")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantBasicAccess(request.Headers);
                string userType = controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter).userType;
                return request.CreateResponse(HttpStatusCode.OK, new UtilsHelper().CreateSuccessfulDictionary(new ControllerMaterial().GetMaterialesAll(userType)));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }

        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody] Dictionary<string,object> data)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                string nickname = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;
                data.Add("nickname_admin", nickname);
                long id = new ControllerMaterial().AddMaterial(data);
                return request.CreateResponse(HttpStatusCode.OK, new UtilsHelper().CreateSuccessfulDictionary(new ControllerMaterial().GetMaterial(id)));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (ArgumentNullException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (ArgumentException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }

        [System.Web.Http.ActionName("Editar")]
        public HttpResponseMessage Patch(HttpRequestMessage request, [FromBody] Dictionary<string, object> data)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                new ControllerMaterial().EditarMaterial(data);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [System.Web.Http.ActionName("filtrar")]
        public HttpResponseMessage Get(HttpRequestMessage request, long? id = null, string nickname_admin = null,
                                                                   string nombre = null, bool? cuantificable = null,
                                                                   string color = null)
        {
            try
            {
                Dictionary<string, object> data = new Dictionary<string, object>
                {
                    { "id", id },
                    { "nickname_admin", nickname_admin },
                    { "nombre", nombre },
                    { "cuantificable", cuantificable },
                    { "color", color }
                };

                new ControllerToken().GrantBasicAccess(request.Headers);
                return request.CreateResponse(HttpStatusCode.OK, 
                    new UtilsHelper().CreateSuccessfulDictionary(new ControllerMaterial().GetMaterialesFiltrados(data)));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }

        [System.Web.Http.ActionName("top")]
        public HttpResponseMessage GetTop(HttpRequestMessage request)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                return request.CreateResponse(HttpStatusCode.OK, new ControllerMaterial().GetTopMateriales());
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }
    }
}
