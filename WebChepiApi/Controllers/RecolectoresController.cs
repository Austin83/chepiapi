﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic.Controllers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Net;
using CrossCutConcerns.Utils;
using System.Net.Http.Headers;
using Microsoft.IdentityModel.Tokens;

namespace WebChepiApi.Controllers
{
    public class RecolectoresController : ApiController
    {
        [System.Web.Http.ActionName("todos")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            try
            {
                new ControllerToken().GrantCommonAccess(request.Headers);
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerRecolector().GetRecolectoresAll() }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }

        [System.Web.Http.ActionName("rutas")]
        public HttpResponseMessage GetRutas(HttpRequestMessage request, int? tiempo = null)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantRecolectorAccess(request.Headers);
                string nickname = controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;
                return request.CreateResponse(HttpStatusCode.OK, new ControllerRecolector().GetRecoleccionesRuta( nickname, tiempo ));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException)
            {
                return request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }

        public HttpResponseMessage Get(HttpRequestMessage request, string nickname)
        {
            try
            {
                if (new UtilsHelper().UselessString(nickname))
                {
                    throw new KeyNotFoundException("nickname is null !");
                }
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantCommonAccess(request.Headers);
                UserData userData = controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter);
                return request.CreateResponse(HttpStatusCode.OK, 
                    new UtilsHelper().CreateSuccessfulDictionary(new ControllerRecolector().GetRecolector(nickname, userData)));
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        [System.Web.Http.ActionName("mis_recolecciones")]
        public HttpResponseMessage GetMisRecolecciones(HttpRequestMessage request, int tiempo)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantRecolectorAccess(request.Headers);
                string nickname = controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;
                return request.CreateResponse(HttpStatusCode.OK, 
                    new UtilsHelper().CreateSuccessfulDictionary(new ControllerRecolector().GetMisRecolecciones(nickname, tiempo)));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }

        [System.Web.Http.ActionName("residuos")]
        public HttpResponseMessage GetMisRecoleccionesFiltradas(HttpRequestMessage request, int tiempo)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantRecolectorAccess(request.Headers);
                string nickname = controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;
                return request.CreateResponse(HttpStatusCode.OK,
                    new UtilsHelper().CreateSuccessfulDictionary(new ControllerRecolector().GetCantidadMateriales(nickname, tiempo)));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }
        
        [System.Web.Http.ActionName("registrar")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]Dictionary<string,object> data)
        {
            try
            {
                List<string> palabras = new List<string>
                {
                    (string)data["nickname"],
                    (string)data["nombre"],
                    (string)data["apellido"],
                    (string)data["mail"],
                    (string)data["contra"],
                };

                List<object> resto = new List<object>
                {
                    byte.Parse( data["edad"].ToString() )
                };

                UtilsHelper utils = new UtilsHelper();
                if (utils.UselessStrings(palabras) || utils.AnyNulls(resto))
                {
                    throw new ArgumentException("Invalid arguments in body");
                }

                string nickname = new ControllerRecolector().AddRecolector(data);
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (ArgumentNullException ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (ArgumentException ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (InvalidCastException ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }
        
        public HttpResponseMessage Patch(HttpRequestMessage request, [FromBody] Dictionary<string,object> data)
        {
            try
            {
                new ControllerToken().GrantRecolectorAccess(request.Headers);
                //new ControllerToken().IdentityCheck(token, builder.nickname);
                new ControllerRecolector().UpdateRecolector(data);
                return request.CreateResponse(HttpStatusCode.OK, new ControllerRecolector().GetRecolector((string)data["nickname"], 
                                                                    new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter)));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
        }
        
        public HttpResponseMessage Delete(HttpRequestMessage request, string nickname)
        {
            try
            {
                new ControllerToken().GrantRecolectorAccess(request.Headers);
                //new ControllerToken().IdentityCheck(token, nickname);
                new ControllerRecolector().GetRecolector(nickname, new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter));
                new ControllerRecolector().RemoveRecolector(nickname);

                return request.CreateResponse(HttpStatusCode.OK, "Recolector de nickname ={ " + nickname + " } fue borrado exitosamente.");
            }
            catch (Exception ex)
            {

                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
        }

        [System.Web.Http.ActionName("top")]
        public HttpResponseMessage GetTopRecicladores(HttpRequestMessage request, string nickname = null, int? orden = null)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                return request.CreateResponse(HttpStatusCode.OK, new ControllerRecolector().GetTop(nickname, orden));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}