﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Net;
using BusinessLogic.Controllers;
using CrossCutConcerns.Utils;
using System.IO;

namespace WebChepiApi.Controllers
{
    public class TokensController : ApiController
    {
        // GET api/tokens
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            try
            {
                return request.CreateResponse(HttpStatusCode.OK, new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
        }

        // POST api/tokens
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]UserData userData)
        {
            try
            {
                return request.CreateResponse(HttpStatusCode.OK, new ControllerToken().CreateJWT(userData));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
        }

        // POST api/tokens
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]UserData userData, int algo)
        {
            try
            {
                return request.CreateResponse(HttpStatusCode.OK, new ControllerToken().CreateInvalidJWT(userData));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ex.Message);
            }
        }
        
    }
}