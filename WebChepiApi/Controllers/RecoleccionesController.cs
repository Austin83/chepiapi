﻿using BusinessLogic.Controllers;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Utils;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace WebChepiApi.Controllers
{
    public class RecoleccionesController : ApiController
    {
        [System.Web.Http.ActionName("registrar")]
        public HttpResponseMessage Post(HttpRequestMessage request, [FromBody]Dictionary<string, object> data)
        {
            try
            {
                new ControllerToken().GrantRecicladorAccess(request.Headers);
                string nickname = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;
                data.Add("nickname_reciclador", nickname);
                long id = new ControllerRecoleccion().AddRecoleccion(data);
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerRecoleccion().GetRecoleccion(id) }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (ArgumentNullException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (ArgumentException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Conflict, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        [System.Web.Http.ActionName("Reservar")]
        public HttpResponseMessage Patch(HttpRequestMessage request, [FromBody]Dictionary<string, object> data)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantRecolectorAccess(request.Headers);
                string nickname = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;
                data.Add("nickname_recolector", nickname);
                new ControllerRecoleccion().ReservarRecoleccion(data);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (ArgumentNullException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (ArgumentException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }
        
        [System.Web.Http.ActionName("todos")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            try
            {
                new ControllerToken().GrantCommonAccess(request.Headers);
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerRecoleccion().GetRecoleccionesAll() }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }

        [System.Web.Http.ActionName("filtrar")]
        public HttpResponseMessage Get(HttpRequestMessage request, long? id = null, string nickname_reciclador = null, string desde = null,
                                        long? id_ubicacion = null, string hora = null, string observacion = null, string estado = null, string materiales = null)
        {
            try
            {
                new ControllerToken().GrantCommonAccess(request.Headers);
                Dictionary<string, object> data = new Dictionary<string, object>
                {
                    { "id", id },
                    { "nickname_reciclador", nickname_reciclador},
                    { "desde", desde },
                    { "id_ubicacion", id_ubicacion },
                    { "hora", hora },
                    { "observacion", observacion },
                    { "estado", estado},
                    { "materiales", materiales}
                };
                return request.CreateResponse(HttpStatusCode.OK, new UtilsHelper().
                    CreateSuccessfulDictionary(new ControllerRecoleccion().GetRecoleccionesFiltradas(data)));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }

        [System.Web.Http.ActionName("Ruta")]
        public HttpResponseMessage GetRuta(HttpRequestMessage request, string ids)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantRecolectorAccess(request.Headers);
                string nickname = controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;
                string[] parts = ids.Split(',');
                List<long> aux = new List<long>();
                foreach( string s in parts)
                {
                    aux.Add(long.Parse(s));
                }
                
                return request.CreateResponse(HttpStatusCode.OK, new ControllerRecoleccion().GetRuta( nickname, aux.ToArray() ));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }

        [System.Web.Http.ActionName("confirmar_ruta")]
        public HttpResponseMessage ConfirmarRuta(HttpRequestMessage request, [FromBody]Dictionary<string,object> data)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantRecolectorAccess(request.Headers);
                string nickname = controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;
                return request.CreateResponse(HttpStatusCode.OK, new ControllerRecoleccion().ConfirmarRuta( nickname,data));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (ArgumentNullException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (FormatException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }

        [System.Web.Http.ActionName("liberar")]
        public HttpResponseMessage PatchLiberar(HttpRequestMessage request, [FromBody]Dictionary<string, object> data)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantRecolectorAccess(request.Headers);
                new ControllerRecoleccion().LiberarRecolecciones(data);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (ArgumentNullException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (ArgumentException ex)
            {
                return request.CreateResponse(HttpStatusCode.Conflict, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (FormatException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }


        [System.Web.Http.ActionName("reciclador")]
        public HttpResponseMessage GetRecoleccionesRecicladores(HttpRequestMessage request, int? tiempo = null, 
                                                                                            int? estado = null, string nickname_reco = null )
        {
            try
            {
                new ControllerToken().GrantRecicladorAccess(request.Headers);
                string nickname = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;
                return request.CreateResponse(HttpStatusCode.OK, new ControllerRecoleccion().GetRecoleccionesReciclador(nickname,tiempo,estado,nickname_reco));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        
        // GET: api/recolecciones
        [System.Web.Http.ActionName("recolector_completas")]
        public HttpResponseMessage GetRecoleccionesRecolector(HttpRequestMessage request)
        {
            try
            {
                new ControllerToken().GrantRecolectorAccess(request.Headers);
                string nickname = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;

                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerRecoleccion().GetRecoleccionesRecolectorCompletas(nickname) }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }

        [System.Web.Http.ActionName("top_materiales")]
        public HttpResponseMessage GetTopResiduos(HttpRequestMessage request)
        {
            try
            {
                new ControllerToken().GrantAdminAccess(request.Headers);
                string nickname = new ControllerToken().DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;

                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", true },
                    { "data", new ControllerRecoleccion().GetTopResiduos() }
                };
                return request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (SecurityTokenExpiredException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (UnauthorizedAccessException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.Unauthorized, result);
            }
            catch (KeyNotFoundException ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception ex)
            {
                Dictionary<string, object> result = new Dictionary<string, object>
                {
                    { "success", false },
                    { "error", ex.Message }
                };
                return request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
        }

        [System.Web.Http.ActionName("reservables")]
        public HttpResponseMessage GetRecoleccionesReservables(HttpRequestMessage request, string fecha = null, string hora = null, string materiales = null)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantRecolectorAccess(request.Headers);
                string nickname = controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname;

                return request.CreateResponse(HttpStatusCode.OK, new ControllerRecoleccion().GetRecoleccionesReservables(nickname, fecha, materiales, hora));
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }

        [System.Web.Http.ActionName("puntuar")]
        public HttpResponseMessage PatchPuntuar(HttpRequestMessage request, [FromBody]Dictionary<string, object> data)
        {
            try
            {
                ControllerToken controllerToken = new ControllerToken();
                controllerToken.GrantRecicladorAccess(request.Headers);
                new ControllerRecoleccion().RateRecoleccion(data, controllerToken.DecipherTokenPayload(request.Headers.Authorization.Parameter).nickname );
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (SecurityTokenExpiredException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (UnauthorizedAccessException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (ArgumentNullException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (ArgumentException ex)
            {
                return request.CreateResponse(HttpStatusCode.Conflict, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (FormatException ex)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (KeyNotFoundException ex)
            {
                return request.CreateResponse(HttpStatusCode.NotFound, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.InternalServerError, new UtilsHelper().CreateErrorDictionary(ex.Message));
            }
        }
    }
}