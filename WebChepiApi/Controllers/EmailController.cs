﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Web.Http.Description;
using System.Net;
using BusinessLogic.Controllers;
using System.Web.Http;
using CrossCutConcerns.Utils;

namespace WebChepiApi.Controllers
{
    public class EmailController : ApiController
    {
        public HttpResponseMessage Post(HttpRequestMessage request, EmailData emailData)
        {
            try
            {
                new ControllerEmail().SendEmail(emailData);
                return request.CreateResponse(HttpStatusCode.OK, "tried to send email");
            }
            catch (Exception ex)
            {
                return request.CreateResponse(HttpStatusCode.Forbidden, ex.Message);
            }
        }
    }
}