﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;

namespace DataAccess
{
    public partial class Recoleccion_Material
    {
        private Recoleccion_Material() { }

        public Recoleccion_Material(long id_recoleccion, Dictionary<string,object> data)
        {
            this.id_recoleccion = id_recoleccion;

            if (data.ContainsKey("id"))
            {
                this.id_material = long.Parse( data["id"].ToString() );
            }
            else
            {
                throw new ArgumentNullException("id_material");
            }

            if (data.ContainsKey("cantidad"))
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    Material material = context.Materials.SingleOrDefault(m => m.id == id_material);

                    if ( material != null )
                    {
                        if (material.cuantificable)
                        {
                            this.cantidad = int.Parse( data["cantidad"].ToString() );
                        }
                        else
                        {
                            this.cantidad = 0;
                        }
                    }
                    else
                    {
                        throw new KeyNotFoundException("Material de id={ " + id_material + " } NO fue encontrado");
                    }
                }
                
            }
            else
            {
                throw new ArgumentNullException("cantidad");
            }
        }

        public Dictionary<string, object> GetInfo(InfoFilter rmFilter)
        {
            if( rmFilter == null)
            {
                return this.GetInfo();
            }

            Dictionary<string, object> info = null;
            if( rmFilter.filter.Any())
            {
                Recoleccion_MaterialSelect recoleccion_MaterialSelect = new Recoleccion_MaterialSelect();

                switch( rmFilter.mode )
                {
                    case GetInfoModes.Modes.Optimistic:
                        info = new Dictionary<string, object>();
                        if (rmFilter.filter.Contains(recoleccion_MaterialSelect.id_recoleccion))
                        {
                            info.Add("id_recoleccion", this.id_recoleccion);
                        }

                        if (rmFilter.filter.Contains(recoleccion_MaterialSelect.id_material))
                        {
                            info.Add("id_material", this.id_material);
                        }

                        if (rmFilter.filter.Contains(recoleccion_MaterialSelect.cantidad))
                        {
                            info.Add("cantidad", this.cantidad);
                        }
                        break;
                    case GetInfoModes.Modes.Pessimistic:
                        info = this.GetInfo();
                        if (rmFilter.filter.Contains(recoleccion_MaterialSelect.id_recoleccion))
                        {
                            info.Remove("id_recoleccion");
                        }

                        if (rmFilter.filter.Contains(recoleccion_MaterialSelect.id_material))
                        {
                            info.Remove("id_material");
                        }

                        if (rmFilter.filter.Contains(recoleccion_MaterialSelect.cantidad))
                        {
                            info.Remove("cantidad");
                        }
                        break;
                }
            }
            else
            {
                info = this.GetInfo();
            }
            return info;
        }

        public Dictionary<string, object> GetInfo()
        {
            return new Dictionary<string, object>
            {
                { "id_recoleccion", this.id_recoleccion },
                { "id_material", this.id_material },
                { "cantidad", this.cantidad }
            };
        }
    }
}
