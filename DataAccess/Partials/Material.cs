﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;

namespace DataAccess
{
    public partial class Material
    {
        public Material(Dictionary<string, object> data)
        {
            this.bloqueado = false;
            if (data.ContainsKey("nombre"))
            {
                this.nombre = (string)data["nombre"];
            }
            else
            {
                throw new ArgumentNullException("nombre");
            }

            if (data.ContainsKey("nickname_admin"))
            {
                this.nickname_admin = (string)data["nickname_admin"];
            }
            else
            {
                throw new ArgumentNullException("nickname_admin");
            }

            if (data.ContainsKey("cuantificable"))
            {
                this.cuantificable = (bool)data["cuantificable"];
            }
            else
            {
                throw new ArgumentNullException("cuantificable");
            }

            if (data.ContainsKey("color"))
            {
                this.color = (string)data["color"];
            }
            else
            {
                throw new ArgumentNullException("color");
            }
        }

        public Dictionary<string, object> GetInfo(InfoFilter infoFilter)
        {
            if (infoFilter == null)
            {
                return this.GetInfo();
            }
            
            Dictionary<string, object> info = null;

            if( infoFilter.filter.Any())
            {
                MaterialSelect materialSelect = new MaterialSelect();
                switch (infoFilter.mode)
                {
                    case GetInfoModes.Modes.Optimistic:
                        info = new Dictionary<string, object>();
                        if (infoFilter.filter.Contains(materialSelect.id))
                        {
                            info.Add("id", this.id);
                        }

                        if (infoFilter.filter.Contains(materialSelect.nombre))
                        {
                            info.Add("nombre", this.nombre);
                        }

                        if (infoFilter.filter.Contains(materialSelect.nickname_admin))
                        {
                            info.Add("nickname_admin", this.nickname_admin);
                        }

                        if (infoFilter.filter.Contains(materialSelect.cuantificable))
                        {
                            info.Add("cuantificable", this.cuantificable);
                        }

                        if (infoFilter.filter.Contains(materialSelect.color))
                        {
                            info.Add("color", this.color);
                        }
                        if (infoFilter.filter.Contains(materialSelect.bloqueado))
                        {
                            info.Add("bloqueado", this.bloqueado);
                        }
                        break;
                    case GetInfoModes.Modes.Pessimistic:
                        info = this.GetInfo();
                        if (infoFilter.filter.Contains(materialSelect.id))
                        {
                            info.Remove("id");
                        }

                        if (infoFilter.filter.Contains(materialSelect.nombre))
                        {
                            info.Remove("nombre");
                        }

                        if (infoFilter.filter.Contains(materialSelect.nickname_admin))
                        {
                            info.Remove("nickname_admin");
                        }

                        if (infoFilter.filter.Contains(materialSelect.cuantificable))
                        {
                            info.Remove("cuantificable");
                        }

                        if (infoFilter.filter.Contains(materialSelect.color))
                        {
                            info.Remove("color");
                        }
                        if (infoFilter.filter.Contains(materialSelect.bloqueado))
                        {
                            info.Remove("bloqueado");
                        }
                        break;
                }
            }
            else
            {
                info = this.GetInfo();
            }
            
            return info;
        }

        public Dictionary<string, object> GetInfo()
        {
            return new Dictionary<string, object>
            {
                { "id", this.id },
                { "nombre", this.nombre },
                { "nickname_admin", this.nickname_admin },
                { "cuantificable", this.cuantificable },
                { "color", this.color },
                { "bloqueado", this.bloqueado }
            };
        }
    }
}
