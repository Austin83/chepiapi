﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;

namespace DataAccess
{
    public partial class Recolector
    {
        public Recolector(Dictionary<string,object> data)
        {
            this.nickname = (string)data["nickname"];
            this.puntaje = 0;
        }
        
        public Dictionary<string, object> GetInfo()
        {
            Usuario usuario = null;
            Ubicacion ubicacion = null;
            using (ChepiEntities context = new ChepiEntities())
            {
                usuario = context.Usuarios.SingleOrDefault(x => x.nickname == nickname);
                ubicacion = context.Ubicacions.SingleOrDefault(x => x.nickname_recolector == nickname);
            }
            Dictionary<string,object> result = usuario.GetInfo();
            result.Add("puntaje", this.puntaje);
            result.Add("ubicacion", new double[] { ubicacion.latitud, ubicacion.longitud });
            return result;
        }

        public Dictionary<string,object> GetInfo( InfoFilter filtroRecolector, InfoFilter filtroUsuario = null)
        {
            if( filtroRecolector == null)
            {
                return this.GetInfo();
            }

            Dictionary<string, object> info = null;
            if (filtroRecolector.filter.Any())
            {
                RecolectorSelect selectRecolector = new RecolectorSelect();

                if (filtroRecolector.filter.Contains(selectRecolector.usuario))
                {
                    Usuario usuario = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        usuario = context.Usuarios.SingleOrDefault(u => u.nickname == this.nickname);
                    }

                    if (filtroUsuario != null && filtroUsuario.filter.Any())
                    {
                        info = usuario.GetInfo(filtroUsuario);
                    }
                    else
                    {
                        info = usuario.GetInfo();
                    }
                }

                if (filtroRecolector.filter.Contains(selectRecolector.puntaje))
                {
                    info.Add("puntaje", this.puntaje);
                }

                if (filtroRecolector.filter.Contains(selectRecolector.ubicacion))
                {
                    Ubicacion ubicacion = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        ubicacion = context.Ubicacions.SingleOrDefault(x => x.nickname_recolector == nickname);
                    }
                    info.Add("ubicacion", new double[] { ubicacion.latitud, ubicacion.longitud });
                }
            }
            else
            {
                info = this.GetInfo();
            }

            return info;
        }
    }
}
