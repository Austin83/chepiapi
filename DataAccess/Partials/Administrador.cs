﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Utils;
using System.Security.Cryptography;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Constants;

namespace DataAccess
{
    public partial class Administrador
    {
        public Administrador(Dictionary<string,object> data)
        {
            if (data.ContainsKey("nickname"))
            {
                this.nickname = (string)data["nickname"];
            }
            else
            {
                throw new ArgumentNullException("Administrador constructor: 'nickname' is null");
            }

            if (data.ContainsKey("mail"))
            {
                this.mail = (string)data["mail"];
            }
            else
            {
                throw new ArgumentNullException("Administrador constructor: 'mail' is null");
            }

            if (data.ContainsKey("contra"))
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    this.contra = new UtilsHelper().GetMd5Hash( md5Hash, (string)data["contra"] );
                }
            }
            else
            {
                throw new ArgumentNullException("Administrador constructor: 'contra' is null");
            }
        }

        public Dictionary<string, object> GetInfo(InfoFilter filtroAdmin, InfoFilter filtroContenedor = null, InfoFilter filtroMaterial = null)
        {
            if (filtroAdmin == null)
            {
                return this.GetInfo();
            }

            Dictionary<string, object> info = null;
            if ( filtroAdmin.filter.Any())
            {
                AdminSelect selectAdmin = new AdminSelect();
                
                switch (filtroAdmin.mode)
                {
                    case GetInfoModes.Modes.Optimistic:
                        info = new Dictionary<string, object>();
                        if (filtroAdmin.filter.Contains(selectAdmin.nickname))
                        {
                            info.Add("nickname", this.nickname);
                        }

                        if (filtroAdmin.filter.Contains(selectAdmin.mail))
                        {
                            info.Add("mail", this.mail);
                        }
                        break;
                    case GetInfoModes.Modes.Pessimistic:
                        info = this.GetInfo();
                        if (filtroAdmin.filter.Contains(selectAdmin.nickname))
                        {
                            info.Remove("nickname");
                        }

                        if (filtroAdmin.filter.Contains(selectAdmin.mail))
                        {
                            info.Remove("mail");
                        }
                        break;
                }

                if (filtroAdmin.filter.Contains(selectAdmin.contenedores))
                {
                    List<Contenedor> listaContenedores = null;
                    List<Dictionary<string, object>> contenedores = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        listaContenedores = context.Contenedors.Where(c => c.nickname_admin == this.nickname).ToList();
                    }
                    
                    if (filtroContenedor != null && filtroContenedor.filter.Any())
                    {
                        switch (filtroContenedor.mode)
                        {
                            case GetInfoModes.Modes.Optimistic:
                                contenedores = new List<Dictionary<string, object>>();
                                foreach ( Contenedor contenedor in listaContenedores)
                                {
                                    contenedores.Add(contenedor.GetInfo(filtroContenedor));
                                }
                                info.Add("contenedores", contenedores);
                                break;

                            case GetInfoModes.Modes.Pessimistic:
                                info.Remove("contenedores");
                                break;
                        }
                    }
                    else
                    {
                        contenedores = new List<Dictionary<string, object>>();
                        foreach (Contenedor contenedor in listaContenedores)
                        {
                            contenedores.Add(contenedor.GetInfo());
                        }
                        info.Add("contenedores", contenedores);
                    }
                }

                if (filtroAdmin.filter.Contains(selectAdmin.materiales))
                {
                    List<Material> listaMateriales = null;
                    List<Dictionary<string, object>> materiales = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        listaMateriales = context.Materials.Where(m => m.nickname_admin == this.nickname).ToList();
                    }

                    if (filtroMaterial != null && filtroMaterial.filter.Any())
                    {
                        switch (filtroMaterial.mode)
                        {
                            case GetInfoModes.Modes.Optimistic:
                                materiales = new List<Dictionary<string, object>>();
                                foreach (Material material in listaMateriales)
                                {
                                    materiales.Add(material.GetInfo(filtroMaterial));
                                }
                                info.Add("materiales", materiales);
                                break;

                            case GetInfoModes.Modes.Pessimistic:
                                info.Remove("materiales");
                                break;
                        }
                    }
                    else
                    {
                        materiales = new List<Dictionary<string, object>>();
                        foreach (Material material in listaMateriales)
                        {
                            materiales.Add(material.GetInfo());
                        }
                        info.Add("materiales", materiales);
                    }
                }
            }
            else
            {
                info = this.GetInfo();
            }
            
            return info;
        }

        public Dictionary<string, object> GetInfo()
        {
            List<Contenedor> listaContenedores = null;
            List<Material> listaMateriales = null;
            List<Dictionary<string, object>> contenedores = new List<Dictionary<string, object>>();
            List<Dictionary<string, object>> materiales = new List<Dictionary<string, object>>();
            using (ChepiEntities context = new ChepiEntities())
            {
                listaContenedores = context.Contenedors.Where( c => c.nickname_admin == this.nickname ).ToList();
                listaMateriales = context.Materials.Where(m => m.nickname_admin == this.nickname).ToList();
            }

            foreach( Contenedor contenedor in listaContenedores)
            {
                contenedores.Add(contenedor.GetInfo());
            }

            foreach( Material material in listaMateriales)
            {
                materiales.Add(material.GetInfo());
            }

            return new Dictionary<string, object>
            {
                { "nickname", this.nickname },
                { "mail", this.mail },
                { "contenedores", contenedores },
                { "materiales", materiales }
            };
        }
    }
}
