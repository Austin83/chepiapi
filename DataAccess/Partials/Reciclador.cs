﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;

namespace DataAccess
{
    public partial class Reciclador
    {
        public Reciclador(string nickname)
        {
            this.nickname = nickname;
        }

        public Dictionary<string,object> GetInfo()
        {
            Usuario usuario = null;
            List<Ubicacion> listaUbis = null;
            List<Alerta> listaAlertas = null;
            List<Recoleccion> listaRecolecciones = null;
            List<Dictionary<string,object>> aux = null;
            using (ChepiEntities context = new ChepiEntities())
            {
                usuario = context.Usuarios.SingleOrDefault(u => u.nickname == nickname);
                listaUbis = context.Ubicacions.Where(u => u.nickname_reciclador == this.nickname).ToList();
                listaAlertas = context.Alertas.Where(a => a.nickname_reciclador == this.nickname).ToList();
                listaRecolecciones = context.Recoleccions.Where(r => r.nickname_reciclador == this.nickname).ToList();
            }
            Dictionary<string, object> info = usuario.GetInfo();

            aux = new List<Dictionary<string, object>>();
            foreach( Ubicacion ubicacion in listaUbis )
            {
                aux.Add(ubicacion.GetInfo());
            }
            info.Add("ubicaciones", aux );

            aux = new List<Dictionary<string, object>>();
            foreach( Alerta alerta in listaAlertas)
            {
                aux.Add(alerta.GetInfo());
            }
            info.Add("alertas", aux );

            aux = new List<Dictionary<string, object>>();
            foreach(Recoleccion recoleccion in listaRecolecciones)
            {
                aux.Add(recoleccion.GetInfo());
            }
            info.Add("recolecciones", aux );
            return info;
        }

        public Dictionary<string,object> GetInfo( InfoFilter filtroReciclador, InfoFilter filtroUsuario = null, InfoFilter filtroUbicacion = null,
                                                    InfoFilter filtroAlerta = null, InfoFilter filtroRecoleccion = null)
        {
            if( filtroReciclador == null)
            {
                return this.GetInfo();
            }

            Dictionary<string, object> info = null;

            if (filtroReciclador.filter.Any())
            {
                RecicladorSelect selectReciclador = new RecicladorSelect();

                if (filtroReciclador.filter.Contains(selectReciclador.usuario))
                {
                    Usuario usuario = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        usuario = context.Usuarios.SingleOrDefault(u => u.nickname == this.nickname);
                    }

                    if (filtroUsuario != null && filtroUsuario.filter.Any())
                    {
                        info = usuario.GetInfo(filtroUsuario);
                    }
                    else
                    {
                        info = usuario.GetInfo();
                    }
                }

                if (filtroReciclador.filter.Contains(selectReciclador.ubicaciones))
                {
                    List<Ubicacion> listaUbicaciones = null;
                    List<Dictionary<string, object>> ubicaciones = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        listaUbicaciones = context.Ubicacions.Where( u => u.nickname_reciclador == this.nickname).ToList();
                    }

                    if (filtroUbicacion != null && filtroUbicacion.filter.Any())
                    {
                        switch (filtroUbicacion.mode)
                        {
                            case GetInfoModes.Modes.Optimistic:
                                ubicaciones = new List<Dictionary<string, object>>();
                                foreach (Ubicacion ubi in listaUbicaciones)
                                {
                                    ubicaciones.Add(ubi.GetInfo(filtroUbicacion));
                                }
                                info.Add("ubicaciones", ubicaciones);
                                break;

                            case GetInfoModes.Modes.Pessimistic:
                                info.Remove("ubicaciones");
                                break;
                        }
                    }
                    else
                    {
                        ubicaciones = new List<Dictionary<string, object>>();
                        foreach (Ubicacion ubi in listaUbicaciones)
                        {
                            ubicaciones.Add(ubi.GetInfo());
                        }
                        info.Add("ubicaciones", ubicaciones);
                    }
                }

                if (filtroReciclador.filter.Contains(selectReciclador.alertas))
                {
                    List<Alerta> listaAlertas = null;
                    List<Dictionary<string, object>> alertas = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        listaAlertas = context.Alertas.Where(a => a.nickname_reciclador == this.nickname).ToList();
                    }

                    if (filtroAlerta != null && filtroAlerta.filter.Any())
                    {
                        switch (filtroAlerta.mode)
                        {
                            case GetInfoModes.Modes.Optimistic:
                                alertas = new List<Dictionary<string, object>>();
                                foreach (Alerta alerta in listaAlertas)
                                {
                                    alertas.Add(alerta.GetInfo(filtroAlerta));
                                }
                                info.Add("alertas", alertas);
                                break;

                            case GetInfoModes.Modes.Pessimistic:
                                info.Remove("alertas");
                                break;
                        }
                    }
                    else
                    {
                        alertas = new List<Dictionary<string, object>>();
                        foreach (Alerta alerta in listaAlertas)
                        {
                            alertas.Add(alerta.GetInfo());
                        }
                        info.Add("alertas", alertas);
                    }
                }

                if (filtroReciclador.filter.Contains(selectReciclador.recolecciones))
                {
                    List<Recoleccion> listaRecolecciones = null;
                    List<Dictionary<string, object>> recolecciones = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        listaRecolecciones = context.Recoleccions.Where(r => r.nickname_reciclador == this.nickname).ToList();
                    }

                    if (filtroRecoleccion != null && filtroRecoleccion.filter.Any())
                    {
                        switch (filtroRecoleccion.mode)
                        {
                            case GetInfoModes.Modes.Optimistic:
                                recolecciones = new List<Dictionary<string, object>>();
                                foreach (Recoleccion recoleccion in listaRecolecciones)
                                {
                                    recolecciones.Add(recoleccion.GetInfo(filtroRecoleccion));
                                }
                                info.Add("recolecciones", recolecciones);
                                break;

                            case GetInfoModes.Modes.Pessimistic:
                                info.Remove("alertas");
                                break;
                        }
                    }
                    else
                    {
                        recolecciones = new List<Dictionary<string, object>>();
                        foreach (Recoleccion recoleccion in listaRecolecciones)
                        {
                            recolecciones.Add(recoleccion.GetInfo());
                        }
                        info.Add("recolecciones", recolecciones);
                    }
                }

            }
            else
            {
                info = this.GetInfo();
            }
            return info;
        }
        
    }
}
