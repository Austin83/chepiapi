﻿using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public partial class Alerta
    {
        private Alerta() { }

        public Alerta(long id_contenedor, string nickname_reciclador)
        {
            this.id_contenedor = id_contenedor;
            this.nickname_reciclador = nickname_reciclador;
            this.fecha_hora = DateTime.Now;
        }

        public Dictionary<string, object> GetInfo(InfoFilter infoFilter)
        {
            if (infoFilter == null)
            {
                return this.GetInfo();
            }
            
            Dictionary<string, object> info = null;

            if( infoFilter.filter.Any())
            {
                AlertaSelect alertaSelect = new AlertaSelect();
                switch (infoFilter.mode)
                {
                    case GetInfoModes.Modes.Optimistic:
                        info = new Dictionary<string, object>();
                        if (infoFilter.filter.Contains(alertaSelect.id_contenedor))
                        {
                            info.Add("id_contenedor", this.id_contenedor);
                        }

                        if (infoFilter.filter.Contains(alertaSelect.nickname_reciclador))
                        {
                            info.Add("nickname_reciclador", this.nickname_reciclador);
                        }

                        if (infoFilter.filter.Contains(alertaSelect.fecha_hora))
                        {
                            info.Add("fecha_hora", this.fecha_hora.ToString("yyyy-MM-dd HH:mm", DateTimeFormatInfo.InvariantInfo));
                        }
                        break;
                    case GetInfoModes.Modes.Pessimistic:
                        info = this.GetInfo();
                        if (infoFilter.filter.Contains(alertaSelect.id_contenedor))
                        {
                            info.Remove("id_contenedor");
                        }

                        if (infoFilter.filter.Contains(alertaSelect.nickname_reciclador))
                        {
                            info.Remove("nickname_reciclador");
                        }

                        if (infoFilter.filter.Contains(alertaSelect.fecha_hora))
                        {
                            info.Remove("fecha_hora");
                        }
                        break;
                }
            }
            else
            {
                info = this.GetInfo();
            }
            
            return info;
        }

        public Dictionary<string, object> GetInfo()
        {
            return new Dictionary<string, object>
            {
                { "id_contenedor", this.id_contenedor },
                { "nickname_reciclador", this.nickname_reciclador },
                { "fecha_hora", this.fecha_hora.ToString("yyyy-MM-dd HH:mm", DateTimeFormatInfo.InvariantInfo) }
            };
        }
    }
}
