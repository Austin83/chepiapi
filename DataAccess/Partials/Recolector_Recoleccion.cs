﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;

namespace DataAccess
{
    public partial class Recolector_Recoleccion
    {
        private Recolector_Recoleccion(){ }

        public Recolector_Recoleccion(Dictionary<string,object> data)
        {
            this.calificacion = 0;

            if (data.ContainsKey("id_recoleccion"))
            {
                this.id_recoleccion = (long)data["id_recoleccion"];
            }
            else
            {
                throw new ArgumentNullException("id_recoleccion");
            }

            if (data.ContainsKey("nickname_recolector"))
            {
                this.nickname_recolector = (string)data["nickname_recolector"];
            }
            else
            {
                throw new ArgumentNullException("nickname_recolector");
            }

            if (data.ContainsKey("id_ruta"))
            {
                this.id_ruta = (long)data["id_ruta"];
            }
            else
            {
                throw new ArgumentNullException("id_ruta");
            }

            if (data.ContainsKey("orden_ruta"))
            {
                this.orden_ruta = (int)data["orden_ruta"];
            }
            else
            {
                throw new ArgumentNullException("orden_ruta");
            }
        }
        
        public Dictionary<string, object> GetInfo()
        {
            return new Dictionary<string, object>
            {
                { "id_recoleccion", this.id_recoleccion },
                { "nickname_recolector", this.nickname_recolector},
                { "calificacion", this.calificacion },
                { "id_ruta", this.id_ruta },
                { "orden_ruta", this.orden_ruta }
            };
        }

        public Dictionary<string, object> GetInfo(InfoFilter rrFilter)
        {
            if (rrFilter == null)
            {
                return this.GetInfo();
            }

            Dictionary<string, object> info = null;
            if (rrFilter.filter.Any())
            {
                Recolector_RecoleccionSelect select = new Recolector_RecoleccionSelect();

                switch (rrFilter.mode)
                {
                    case GetInfoModes.Modes.Optimistic:
                        info = new Dictionary<string, object>();
                        if (rrFilter.filter.Contains(select.id_recoleccion))
                        {
                            info.Add("id_recoleccion", this.id_recoleccion);
                        }

                        if (rrFilter.filter.Contains(select.nickname_recolector))
                        {
                            info.Add("nickname_recolector", this.nickname_recolector);
                        }

                        if (rrFilter.filter.Contains(select.calificacion))
                        {
                            info.Add("calificacion", this.calificacion);
                        }

                        if (rrFilter.filter.Contains(select.id_ruta))
                        {
                            info.Add("id_ruta", this.id_ruta);
                        }

                        if (rrFilter.filter.Contains(select.orden_ruta))
                        {
                            info.Add("orden_ruta", this.orden_ruta);
                        }
                        break;
                    case GetInfoModes.Modes.Pessimistic:
                        info = this.GetInfo();
                        if (rrFilter.filter.Contains(select.id_recoleccion))
                        {
                            info.Remove("id_recoleccion");
                        }

                        if (rrFilter.filter.Contains(select.nickname_recolector))
                        {
                            info.Remove("nickname_recolector");
                        }

                        if (rrFilter.filter.Contains(select.calificacion))
                        {
                            info.Remove("calificacion");
                        }

                        if (rrFilter.filter.Contains(select.id_ruta))
                        {
                            info.Remove("id_ruta");
                        }

                        if (rrFilter.filter.Contains(select.orden_ruta))
                        {
                            info.Remove("orden_ruta");
                        }
                        break;
                }
            }
            else
            {
                info = this.GetInfo();
            }
            return info;
        }
    }
}
