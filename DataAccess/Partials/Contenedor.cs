﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;

namespace DataAccess
{
    public partial class Contenedor
    {
        public Contenedor(string nickname_admin, long id_material, bool lleno)
        {
            this.nickname_admin = nickname_admin;
            this.id_material = id_material;
            this.lleno = lleno;
        }
        
        public Dictionary<string, object> GetInfo(InfoFilter filtroContenedor, InfoFilter filtroUbicacion = null,
                                                    InfoFilter filtroAlerta = null, InfoFilter filtroMaterial = null, InfoFilter filtroAdmin = null)
        {
            if( filtroContenedor == null)
            {
                return this.GetInfo();
            }

            Dictionary<string, object> info = new Dictionary<string, object>(); ;

            if( filtroContenedor.filter.Any())
            {
                ContenedorSelect selectContenedor = new ContenedorSelect();
                switch (filtroContenedor.mode)
                {
                    case GetInfoModes.Modes.Optimistic:
                        if (filtroContenedor.filter.Contains(selectContenedor.id))
                        {
                            info.Add("id", this.id);
                        }

                        if (filtroContenedor.filter.Contains(selectContenedor.id_material))
                        {
                            info.Add("id_material", this.id_material);
                        }

                        if (filtroContenedor.filter.Contains(selectContenedor.lleno))
                        {
                            info.Add("lleno", this.lleno);
                        }

                        if (filtroContenedor.filter.Contains(selectContenedor.nickname_admin))
                        {
                            info.Add("nickname_admin", this.nickname_admin);
                        }

                        break;
                    case GetInfoModes.Modes.Pessimistic:
                        if ( ! filtroContenedor.filter.Contains(selectContenedor.id))
                        {
                            info.Add("id", this.id);
                        }

                        if ( ! filtroContenedor.filter.Contains(selectContenedor.id_material))
                        {
                            info.Add("id_material", this.id_material);
                        }

                        if ( ! filtroContenedor.filter.Contains(selectContenedor.lleno))
                        {
                            info.Add("lleno", this.lleno);
                        }

                        if ( ! filtroContenedor.filter.Contains(selectContenedor.nickname_admin))
                        {
                            info.Add("nickname_admin", this.nickname_admin);
                        }
                        break;
                }

                bool condition_contains = filtroContenedor.filter.Contains(selectContenedor.ubicacion);
                bool condition_mode = filtroContenedor.mode == GetInfoModes.Modes.Optimistic;

                if ((condition_contains && condition_mode) || (!condition_contains && !condition_mode))
                {
                    Ubicacion ubi = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        ubi = context.Ubicacions.SingleOrDefault(u => u.id_contenedor == this.id);
                    }
                    if (filtroUbicacion != null && filtroUbicacion.filter.Any())
                    {
                        info.Add("ubicacion", ubi.GetInfo(filtroUbicacion));
                    }
                    else
                    {
                        info.Add("ubicacion", ubi.GetInfo());
                    }
                }

                condition_contains = filtroContenedor.filter.Contains(selectContenedor.material);
                condition_mode = filtroContenedor.mode == GetInfoModes.Modes.Optimistic;

                if ((condition_contains && condition_mode) || (!condition_contains && !condition_mode))
                {
                    Material material = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        material = context.Materials.SingleOrDefault(m => m.id == this.id_material);
                    }
                    if (filtroMaterial != null && filtroMaterial.filter.Any())
                    {
                        info.Add("material", material.GetInfo(filtroMaterial));
                    }
                    else
                    {
                        info.Add("material", material.GetInfo());
                    }
                }

                condition_contains = filtroContenedor.filter.Contains(selectContenedor.alertas);
                condition_mode = filtroContenedor.mode == GetInfoModes.Modes.Optimistic;

                if ((condition_contains && condition_mode) || (!condition_contains && !condition_mode))
                {
                    List<Dictionary<string, object>> alertas = new List<Dictionary<string, object>>();
                    List<Alerta> listaAlertas = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        listaAlertas = context.Alertas.Where(a => a.id_contenedor == this.id).ToList();
                    }

                    if (filtroAlerta != null && filtroAlerta.filter.Any())
                    {
                        foreach (Alerta alerta in listaAlertas)
                        {
                            alertas.Add(alerta.GetInfo(filtroAlerta));
                        }

                        info.Add("alertas", alertas);
                    }
                    else
                    {
                        foreach (Alerta alerta in listaAlertas)
                        {
                            alertas.Add(alerta.GetInfo());
                        }

                        info.Add("alertas", alertas);
                    }
                }

                condition_contains = filtroContenedor.filter.Contains(selectContenedor.admin);
                condition_mode = filtroContenedor.mode == GetInfoModes.Modes.Optimistic;

                if ((condition_contains && condition_mode) || (!condition_contains && !condition_mode))
                {
                    Administrador admin = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        admin = context.Administradors.SingleOrDefault(a => a.nickname == this.nickname_admin);
                    }

                    if (filtroAdmin != null && filtroAdmin.filter.Any())
                    {
                        info.Add("alertas", admin.GetInfo(filtroAdmin));
                    }
                    else
                    {
                        info.Add("alertas", admin.GetInfo());
                    }
                }
            }
            else
            {
                return this.GetInfo();
            }
            
            return info;
        }

        public Dictionary<string, object> GetInfo()
        {
            Material material = null;
            Ubicacion ubi = null;
            List<Alerta> listaAlertas = null;
            List<Dictionary<string, object>> alertas = new List<Dictionary<string, object>>();

            using (ChepiEntities context = new ChepiEntities())
            {
                material = context.Materials.SingleOrDefault(m => m.id == this.id_material);
                ubi = context.Ubicacions.SingleOrDefault(u => u.id_contenedor == this.id);
                listaAlertas = context.Alertas.Where(a => a.id_contenedor == this.id).ToList();
            }

            foreach (Alerta alerta in listaAlertas)
            {
                alertas.Add(alerta.GetInfo());
            }

                return new Dictionary<string, object>
            {
                { "id", this.id },
                { "id_material", this.id_material },
                { "nickname_admin", this.nickname_admin },
                { "lleno", this.lleno },
                { "ubicacion", ubi.GetInfo() },
                { "material", material.GetInfo() },
                { "alertas", alertas }
            };
        }
    }
}
