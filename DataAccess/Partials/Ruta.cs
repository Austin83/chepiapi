﻿using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public partial class Ruta
    {
        public Ruta( string nickname_recolector, Dictionary<string,object> data)
        {
            this.nickname_recolector = nickname_recolector;

            if (DateTime.
                TryParseExact((string)data["fecha"], "yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out DateTime result))
            {
                this.fecha = result;
            }
            else
            {
                throw new FormatException("Recoleccion constructor : 'fecha' does not conform to format 'dd-MM-yyyy'");
            }


            if (data.ContainsKey("hora"))
            {
                byte hora = Byte.Parse(data["hora"].ToString());
                if( hora >= HorasDelDia.range[0] && hora <= HorasDelDia.range[1])
                {
                    this.hora = hora;
                }
            }
            else
            {
                throw new ArgumentNullException("hora");
            }
        }

        public Dictionary<string, object> GetInfo()
        {
            Dictionary<string, object> info = new Dictionary<string, object>
            {
                { "id", this.id },
                { "nickname_recolector", this.nickname_recolector },
                { "fecha", this.fecha.ToString("dd-MM-yyyy", DateTimeFormatInfo.InvariantInfo) },
                { "hora", this.hora }
            };

            List<Recoleccion> listaRecolecciones = null;
            List<Dictionary<string, object>> recolecciones = new List<Dictionary<string, object>>();
            using (ChepiEntities context = new ChepiEntities())
            {
                listaRecolecciones = context.Recoleccions.Where( r => r.Recolector_Recoleccion.id_ruta == this.id ).ToList();
            }
            foreach (Recoleccion recoleccion in listaRecolecciones)
            {
                recoleccion.GetInfo();
            }

            info.Add("recolecciones", recolecciones);
            
            return info;
        }

        public Dictionary<string, object> GetInfo( InfoFilter filtroRuta, InfoFilter filtroRecoleccion = null,
                                                    InfoFilter filtroUbicacion = null, InfoFilter filtroMaterial = null)
        {
            if (filtroRuta == null)
            {
                return this.GetInfo();
            }

            Dictionary<string, object> info = new Dictionary<string, object>(); ;

            if (filtroRuta.filter.Any())
            {
                RutaSelect selectRuta = new RutaSelect();
                switch (filtroRuta.mode)
                {
                    case GetInfoModes.Modes.Optimistic:
                        if (filtroRuta.filter.Contains(selectRuta.id))
                        {
                            info.Add("id", this.id);
                        }

                        if (filtroRuta.filter.Contains(selectRuta.nickname_recolector))
                        {
                            info.Add("nickname_recolector", this.nickname_recolector);
                        }

                        if (filtroRuta.filter.Contains(selectRuta.fecha))
                        {
                            info.Add("fecha", this.fecha);
                        }

                        if (filtroRuta.filter.Contains(selectRuta.hora))
                        {
                            info.Add("hora", this.hora);
                        }
                        break;
                    case GetInfoModes.Modes.Pessimistic:
                        if ( ! filtroRuta.filter.Contains(selectRuta.id))
                        {
                            info.Add("id", this.id);
                        }

                        if ( ! filtroRuta.filter.Contains(selectRuta.nickname_recolector))
                        {
                            info.Add("nickname_recolector", this.nickname_recolector);
                        }

                        if ( ! filtroRuta.filter.Contains(selectRuta.fecha))
                        {
                            info.Add("fecha", this.fecha);
                        }

                        if ( ! filtroRuta.filter.Contains(selectRuta.hora))
                        {
                            info.Add("hora", this.hora);
                        }
                        break;
                }
                bool condition_contains = filtroRuta.filter.Contains(selectRuta.recolecciones);
                bool condition_mode = filtroRuta.mode == GetInfoModes.Modes.Optimistic;
                
                if ((condition_contains && condition_mode) || (!condition_contains && !condition_mode))
                {
                    List<Recoleccion> listaRecoleccion = null;
                    List<Dictionary<string, object>> recolecciones = new List<Dictionary<string, object>>();
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        listaRecoleccion = context.Recoleccions.Where(r => r.Recolector_Recoleccion.id_ruta == this.id)
                            .OrderBy( r => r.Recolector_Recoleccion.orden_ruta ).ToList();
                    }

                    if (filtroRecoleccion != null && filtroRecoleccion.filter.Any())
                    {
                        foreach( Recoleccion rec in listaRecoleccion)
                        {
                            recolecciones.Add(rec.GetInfo(filtroRecoleccion, filtroUbicacion, filtroMaterial));
                        }
                    }
                    else
                    {
                        foreach( Recoleccion rec in listaRecoleccion)
                        {
                            recolecciones.Add(rec.GetInfo());
                        }
                    }
                    info.Add("recolecciones", recolecciones);
                }
            }
            else
            {
                info = this.GetInfo();
            }

            return info;
        }
    }
}
