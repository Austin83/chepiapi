﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public partial class Bloqueo
    {
        private Bloqueo() { }

        public Bloqueo(string nickname_bloqueador, string nickname_bloqueado)
        {
            this.nickname_bloqueador = nickname_bloqueador;
            this.nickname_bloqueado = nickname_bloqueado;
        }
        
        public Dictionary<string,object> GetInfo()
        {
            return new Dictionary<string, object>
            {
                { "nickname_bloqueador" , this.nickname_bloqueador },
                { "nickname_bloqueado" , this.nickname_bloqueado }
            };
        }
    }
}
