﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Utils;
using CrossCutConcerns.Constants;
using System.Security.Cryptography;
using CrossCutConcerns.Selects;

namespace DataAccess
{
    public partial class Usuario
    {
        public Usuario(Dictionary<string, object> data)
        {
            if (data.ContainsKey("nickname"))
            {
                this.nickname = (string)data["nickname"];
            }
            else
            {
                throw new ArgumentNullException("'nickname'");
            }

            if (data.ContainsKey("nombre"))
            {
                this.nombre = (string)data["nombre"];
            }
            else
            {
                throw new ArgumentNullException("'nombre'");
            }

            if (data.ContainsKey("apellido"))
            {
                this.apellido = (string)data["apellido"];
            }
            else
            {
                throw new ArgumentNullException("'apellido'");
            }

            if (data.ContainsKey("mail"))
            {
                this.mail = (string)data["mail"];
            }
            else
            {
                throw new ArgumentNullException("'mail'");
            }

            if (data.ContainsKey("contra"))
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    this.contra = new UtilsHelper().GetMd5Hash(md5Hash, (string)data["contra"]);
                }
            }
            else
            {
                this.contra = null;
            }

            if (data.ContainsKey("edad"))
            {
                this.edad = Byte.Parse( data["edad"].ToString() );
            }
            else
            {
                throw new ArgumentNullException("'edad'");
            }

            this.creditos = 0;
            this.estado = true;

            if (data.ContainsKey("imagen"))
            {
                this.imageData = (string)data["imagen"];
            }
            else
            {
                this.imageData = null;
            }
        }

        public string GetUserType()
        {
            string type = null;
            using (ChepiEntities context = new ChepiEntities())
            {
                if ( context.Recicladors.Where(x => x.nickname == this.nickname).SingleOrDefault() != null)
                {
                    type = new UserTypes().reciclador;
                }

                if (context.Recolectors.Where(x => x.nickname == this.nickname).SingleOrDefault() != null)
                {
                    type = new UserTypes().recolector;
                }

            }
            return type;
        }
    
        public Dictionary<string, object> GetInfo(InfoFilter filtroUsuario, InfoFilter filtroUbicacion = null)
        {
            if (filtroUsuario == null)
                return this.GetInfo();

            Dictionary<string, object> info = new Dictionary<string, object>(); ;

            if (filtroUsuario.filter.Any())
            {
                UsuarioSelect selectUsuario = new UsuarioSelect();

                switch (filtroUsuario.mode)
                {
                    case GetInfoModes.Modes.Optimistic:
                        if (filtroUsuario.filter.Contains(selectUsuario.nickname))
                        {
                            info.Add("nickname", this.nickname);
                        }

                        if (filtroUsuario.filter.Contains(selectUsuario.nombre))
                        {
                            info.Add("nombre", this.nombre);
                        }

                        if (filtroUsuario.filter.Contains(selectUsuario.apellido))
                        {
                            info.Add("apellido", this.apellido);
                        }

                        if (filtroUsuario.filter.Contains(selectUsuario.mail))
                        {
                            info.Add("mail", this.mail);
                        }

                        if (filtroUsuario.filter.Contains(selectUsuario.edad))
                        {
                            info.Add("edad", this.edad);
                        }

                        if (filtroUsuario.filter.Contains(selectUsuario.creditos))
                        {
                            info.Add("creditos", this.creditos);
                        }

                        if (filtroUsuario.filter.Contains(selectUsuario.estado))
                        {
                            info.Add("estado", this.estado);
                        }

                        if (filtroUsuario.filter.Contains(selectUsuario.type))
                        {
                            info.Add("type", this.GetUserType());
                        }

                        if (filtroUsuario.filter.Contains(selectUsuario.imagen))
                        {
                            info.Add("imagen", this.imageData);
                        }
                        break;
                    case GetInfoModes.Modes.Pessimistic:
                        if ( ! filtroUsuario.filter.Contains(selectUsuario.nickname))
                        {
                            info.Add("nickname", this.nickname);
                        }

                        if ( ! filtroUsuario.filter.Contains(selectUsuario.nombre))
                        {
                            info.Add("nombre", this.nombre);
                        }

                        if ( !filtroUsuario.filter.Contains(selectUsuario.apellido))
                        {
                            info.Add("apellido", this.apellido);
                        }

                        if ( !filtroUsuario.filter.Contains(selectUsuario.mail))
                        {
                            info.Add("mail", this.mail);
                        }

                        if ( !filtroUsuario.filter.Contains(selectUsuario.edad))
                        {
                            info.Add("edad", this.edad);
                        }

                        if ( !filtroUsuario.filter.Contains(selectUsuario.creditos))
                        {
                            info.Add("creditos", this.creditos);
                        }

                        if ( !filtroUsuario.filter.Contains(selectUsuario.estado))
                        {
                            info.Add("estado", this.estado);
                        }

                        if ( !filtroUsuario.filter.Contains(selectUsuario.type))
                        {
                            info.Add("type", this.GetUserType());
                        }

                        if ( !filtroUsuario.filter.Contains(selectUsuario.imagen))
                        {
                            info.Add("imagen", this.imageData);
                        }
                        break;
                }

                bool condition_contains = filtroUsuario.filter.Contains(selectUsuario.ubicacion);
                bool condition_mode = filtroUsuario.mode == GetInfoModes.Modes.Optimistic;

                if ((condition_contains && condition_mode) || (!condition_contains && !condition_mode))
                {
                    List<Ubicacion> ubis = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        UserTypes userTypes = new UserTypes();

                        if ( this.GetUserType() == userTypes.reciclador )
                        {
                            ubis = context.Ubicacions.Where(u => u.nickname_reciclador == this.nickname).ToList();
                        }
                        if (this.GetUserType() == userTypes.recolector)
                        {
                            ubis = context.Ubicacions.Where(u => u.nickname_recolector == this.nickname).ToList();
                            if( ubis.Count > 1 )
                            {
                                throw new Exception("Recolector con más de una ubicación");
                            }
                        }

                    }
                    if( ubis != null && ubis.Any())
                    {
                        if (filtroUbicacion != null && filtroUbicacion.filter.Any())
                        {
                            if( ubis.Count == 1 )
                            {
                                info.Add("ubicacion", ubis[0].GetInfo(filtroUbicacion));
                            }
                            else
                            {
                                List<Dictionary<string, object>> ubicaciones = new List<Dictionary<string, object>>();
                                foreach (Ubicacion ubi in ubis)
                                {
                                    ubicaciones.Add(ubi.GetInfo(filtroUbicacion));
                                }
                                info.Add("ubicaciones", ubicaciones.ToArray());
                            }
                        }
                        else
                        {
                            if (ubis.Count == 1)
                            {
                                info.Add("ubicacion", ubis[0].GetInfo());
                            }
                            else
                            {
                                List<Dictionary<string, object>> ubicaciones = new List<Dictionary<string, object>>();
                                foreach (Ubicacion ubi in ubis)
                                {
                                    ubicaciones.Add(ubi.GetInfo());
                                }
                                info.Add("ubicaciones", ubicaciones.ToArray());
                            }
                        }
                    }
                }
            }
            else
            {
                info = this.GetInfo();
            }
            
            return info;
        }
       
        public Dictionary<string, object> GetInfo()
        {
            return new Dictionary<string, object>
            {
                { "nickname", this.nickname },
                { "nombre", this.nombre },
                { "apellido", this.apellido },
                { "mail", this.mail },
                { "edad", this.edad },
                { "creditos", this.creditos },
                { "estado", this.estado },
                { "imagen", this.imageData },
                { "type", this.GetUserType() }
            };
        }

        public bool Bloqueado(string nickname_bloqueador)
        {
            using (ChepiEntities context = new ChepiEntities())
            {
                return context.Bloqueos.Any(b => b.nickname_bloqueador == nickname_bloqueador && b.nickname_bloqueado == nickname);
            }
        }

        public void Update(string nombre, string apellido, byte? edad, string imagen)
        {
            if (nombre != null  && this.nombre != nombre)
            {
                this.nombre = nombre;
            }

            if (apellido != null && this.apellido != apellido)
            {
                this.apellido = apellido;
            }

            if (edad != null && this.edad != edad)
            {
                this.edad = (byte)edad;
            }

            if (imagen != null )
            {
                this.imageData = imagen;
            }
        }
        
    }
}