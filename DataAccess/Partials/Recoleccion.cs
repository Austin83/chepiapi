﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using System.Globalization;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;

namespace DataAccess
{
    public partial class Recoleccion
    {
        public Recoleccion(Dictionary<string, object> data)
        {
            nickname_recolector = null;
            this.publicacion = DateTime.Now;
            try
            {
                this.estado = (byte)EstadosRecoleccion.Estados.disponible;
            }
            catch (InvalidCastException ex)
            {
                throw ex;
            }

            if (DateTime.
                TryParseExact((string)data["fecha"], "yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out DateTime result))
            {
                this.fecha = result;
            }
            else
            {
                throw new FormatException("Recoleccion constructor : 'fecha' does not conform to format 'dd-MM-yyyy'");
            }

            if (data.ContainsKey("id_ubicacion"))
            {
                this.id_ubicacion = (long)data["id_ubicacion"];
            }
            else
            {
                throw new ArgumentNullException("Ubicacion constructor : 'id_ubicacion' is null");
            }

            if (data.ContainsKey("nickname_reciclador"))
            {
                this.nickname_reciclador = (string)data["nickname_reciclador"];
            }
            else
            {
                throw new ArgumentNullException("Recoleccion constructor : 'nickname_reciclador' is null");
            }

            if (data.ContainsKey("hora"))
            {
                this.hora = Byte.Parse(data["hora"].ToString());
            }
            else
            {
                throw new ArgumentNullException("Recoleccion constructor : 'hora' is null");
            }

            if (data.ContainsKey("observacion"))
            {
                this.observacion = (string)data["observacion"];
            }

        }
        
        public Dictionary<string, object> GetInfo(InfoFilter filtroRecoleccion, InfoFilter filtroUbicacion = null, InfoFilter filtroMaterial = null)
        {
            if (filtroRecoleccion == null)
            {
                return this.GetInfo();
            }

            Dictionary<string, object> info = new Dictionary<string, object>();

                RecoleccionSelect selectRecoleccion = new RecoleccionSelect();

                switch (filtroRecoleccion.mode)
                {
                    case GetInfoModes.Modes.Optimistic:
                        if (filtroRecoleccion.filter.Contains(selectRecoleccion.id))
                        {
                            info.Add("id", this.id);
                        }

                        if (filtroRecoleccion.filter.Contains(selectRecoleccion.nickname_reciclador))
                        {
                            info.Add("nickname_reciclador", this.nickname_reciclador);
                        }

                        if (filtroRecoleccion.filter.Contains(selectRecoleccion.id_ubicacion))
                        {
                            info.Add("id_ubicacion", this.id_ubicacion);
                        }

                        if (filtroRecoleccion.filter.Contains(selectRecoleccion.hora))
                        {
                            info.Add("hora", this.hora);
                        }

                        if (filtroRecoleccion.filter.Contains(selectRecoleccion.observacion))
                        {
                            info.Add("observacion", this.observacion);
                        }

                        if (filtroRecoleccion.filter.Contains(selectRecoleccion.estado))
                        {
                            info.Add("estado", this.estado);
                        }

                        if (filtroRecoleccion.filter.Contains(selectRecoleccion.fecha))
                        {
                            info.Add("fecha", this.fecha);
                        }

                        if (filtroRecoleccion.filter.Contains(selectRecoleccion.publicacion))
                        {
                            info.Add("publicacion", this.publicacion);
                        }

                        if (filtroRecoleccion.filter.Contains(selectRecoleccion.calificacion))
                        {
                            double? calificacion = null;
                            using (ChepiEntities context = new ChepiEntities())
                            {
                                calificacion = context.Recolector_Recoleccion.SingleOrDefault(rr => rr.id_recoleccion == this.id).calificacion;
                            }
                            if (calificacion != null)
                            {
                                info.Add("calificacion", calificacion);
                            }
                        }
                        break;
                    case GetInfoModes.Modes.Pessimistic:
                        if ( ! filtroRecoleccion.filter.Contains(selectRecoleccion.id))
                        {
                            info.Add("id", this.id);
                        }

                        if ( ! filtroRecoleccion.filter.Contains(selectRecoleccion.nickname_reciclador))
                        {
                            info.Add("nickname_reciclador", this.nickname_reciclador);
                        }

                        if ( ! filtroRecoleccion.filter.Contains(selectRecoleccion.id_ubicacion))
                        {
                            info.Add("id_ubicacion", this.id_ubicacion);
                        }

                        if ( ! filtroRecoleccion.filter.Contains(selectRecoleccion.hora))
                        {
                            info.Add("hora", this.hora);
                        }

                        if ( ! filtroRecoleccion.filter.Contains(selectRecoleccion.observacion))
                        {
                            info.Add("observacion", this.observacion);
                        }

                        if ( ! filtroRecoleccion.filter.Contains(selectRecoleccion.estado))
                        {
                            info.Add("estado", this.estado);
                        }

                        if ( ! filtroRecoleccion.filter.Contains(selectRecoleccion.fecha))
                        {
                            info.Add("fecha", this.fecha);
                        }

                        if ( ! filtroRecoleccion.filter.Contains(selectRecoleccion.publicacion))
                        {
                            info.Add("publicacion", this.publicacion);
                        }

                        if ( ! filtroRecoleccion.filter.Contains(selectRecoleccion.calificacion))
                        {
                            double? calificacion = null;
                            using (ChepiEntities context = new ChepiEntities())
                            {
                                Recolector_Recoleccion recolector_Recoleccion = context.Recolector_Recoleccion.SingleOrDefault(rr => rr.id_recoleccion == this.id);
                                if ( recolector_Recoleccion != null)
                                {
                                    calificacion = recolector_Recoleccion.calificacion;
                                }
                                
                            }
                            if (calificacion != null)
                            {
                                info.Add("calificacion", calificacion);
                            }
                        }

                        break;
                }
                bool condition_contains = filtroRecoleccion.filter.Contains(selectRecoleccion.ubicacion);
                bool condition_mode = filtroRecoleccion.mode == GetInfoModes.Modes.Optimistic;

                if ( (condition_contains && condition_mode) || (! condition_contains && !condition_mode))
                {
                    Ubicacion ubi = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        ubi = context.Ubicacions.SingleOrDefault(u => u.id == this.id_ubicacion);
                    }
                    if (filtroUbicacion != null && filtroUbicacion.filter.Any())
                    {
                         info.Add("ubicacion", ubi.GetInfo(filtroUbicacion));
                    }
                    else
                    {
                        info.Add("ubicacion", ubi.GetInfo());
                    }
                }

                condition_contains = filtroRecoleccion.filter.Contains(selectRecoleccion.materiales);
                condition_mode = filtroRecoleccion.mode == GetInfoModes.Modes.Optimistic;

                if ((condition_contains && condition_mode) || (!condition_contains && !condition_mode))
                {
                    List<Recoleccion_Material> listaMateriales = null;
                    List<Dictionary<string, object>> materiales = null;
                    using (ChepiEntities context = new ChepiEntities())
                    {
                        listaMateriales = context.Recoleccion_Material.Where(rm => rm.id_recoleccion == this.id).ToList();
                    }
                    materiales = new List<Dictionary<string, object>>();
                    if (filtroMaterial != null && filtroMaterial.filter.Any())
                    {
                        foreach (Recoleccion_Material material in listaMateriales)
                        {
                            materiales.Add(material.GetInfo(filtroMaterial));
                        }
                        info.Add("materiales", materiales);
                    }
                    else
                    {
                        materiales = new List<Dictionary<string, object>>();
                        foreach (Recoleccion_Material material in listaMateriales)
                        {
                            materiales.Add(material.GetInfo());
                        }
                        info.Add("materiales", materiales);
                    }
                }
            return info;
        }

        public Dictionary<string, object> GetInfo()
        {
            Dictionary<string, object> info = new Dictionary<string, object>
            {
                { "id", this.id },
                { "nickname_reciclador", this.nickname_reciclador },
                { "id_ubicacion", this.id_ubicacion },
                { "fecha", this.fecha },
                { "hora", this.hora },
                { "observacion", this.observacion },
                { "estado", this.estado },
                { "publicacion", this.publicacion }
            };

            Ubicacion ubi;
            double? calificacion = null;
            using (ChepiEntities context = new ChepiEntities())
            {
                ubi = context.Ubicacions.SingleOrDefault(u => u.id == id_ubicacion);
                Recolector_Recoleccion aux = context.Recolector_Recoleccion.SingleOrDefault(rr => rr.id_recoleccion == this.id);
                if( aux != null)
                {
                    calificacion = aux.calificacion;
                }
            }
            info.Add("ubicacion", ubi.GetInfo());
            if( calificacion != null)
            {
                info.Add("calificacion", calificacion);
            }
            using (ChepiEntities context = new ChepiEntities())
            {
                List<Recoleccion_Material> recolecciones_materiales = context.Recoleccion_Material.Where(rm => rm.id_recoleccion == this.id).ToList();
                if (recolecciones_materiales != null && recolecciones_materiales.Any())
                {
                    List<Dictionary<string, object>> materiales = new List<Dictionary<string, object>>();

                    foreach (Recoleccion_Material rm in recolecciones_materiales)
                    {
                        materiales.Add(rm.GetInfo());
                    }
                    info.Add("materiales", materiales);
                }
            }

            return info;
        }

        //Criteria : estrictamente todos los materiales o menos, no puede contener materiales no presentes en el filtro
        public bool Contains(long[] ids_materiales_filtro)
        {
            try
            {
                long[] this_materiales = this.GetMateriales();
                return ids_materiales_filtro.Intersect(this_materiales).Any() && !this_materiales.Except(ids_materiales_filtro).Any(); 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long[] GetMateriales()
        {
            using (ChepiEntities context = new ChepiEntities())
            {
                return context.Recoleccion_Material.Where(r => r.id_recoleccion == this.id).Select(rm => rm.id_material).Distinct().ToArray();
            }
        }
        
        public List<Recoleccion> Closest(List<Recoleccion> recolecciones)
        {
            int closestPointIndex = 0;
            double closestDistance = double.MaxValue;
            Ubicacion currentPoint = null;
            long id = recolecciones[0].id_ubicacion;
            using(ChepiEntities context = new ChepiEntities())
            {
                currentPoint = context.Ubicacions.SingleOrDefault(u => u.id == id);
            }
            double distance;

            Ubicacion aux = null;
            for (int i = 0; i < recolecciones.Count; i++)
            {
                id = recolecciones[i].id_ubicacion;
                using (ChepiEntities context = new ChepiEntities())
                {
                    aux = context.Ubicacions.SingleOrDefault(u => u.id == id);
                }
                distance = new UbicacionHelper().GetDistance(currentPoint.Coordinates, aux.Coordinates);

                if (distance < closestDistance)
                {
                    closestPointIndex = i;
                    closestDistance = distance;
                }
            }

            long id_ubicacion = recolecciones[closestPointIndex].id_ubicacion;
            List<Recoleccion> result = recolecciones.Where(r => r.id_ubicacion == id_ubicacion).ToList();
            return result;
        }
        
    }
}
