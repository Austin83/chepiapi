﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;

namespace DataAccess
{
    public partial class Ubicacion
    {
        public Coordinates Coordinates
        {
            get { return new Coordinates(latitud, longitud); }
        }

        public Ubicacion(Dictionary<string, object> data)
        {
            if (data.ContainsKey("nombre"))
            {
                this.nombre = (string)data["nombre"];
            }
            else
            {
                this.nombre = null;
            }

            if (data.ContainsKey("direccion"))
            {
                this.direccion = (string)data["direccion"];
            }
            
            int aux = 0;
            if (data.ContainsKey("nickname_reciclador"))
                ++aux;
            if (data.ContainsKey("nickname_recolector"))
                ++aux;
            if (data.ContainsKey("id_contenedor"))
                ++aux;

            if ( aux > 1 )
            {
                throw new ArgumentNullException("Ubicacion constructor : either 'nickname_reciclador' or 'id_contenedor' has to be null");
            }

            UtilsHelper helper = new UtilsHelper();
            if (data.ContainsKey("nickname_reciclador"))
            {
                if (helper.UselessString((string)data["nickname_reciclador"]))
                {
                    this.nickname_reciclador = null;
                }
                else
                {
                    this.nickname_reciclador = (string)data["nickname_reciclador"];
                }
            }

            if (data.ContainsKey("nickname_recolector"))
            {
                if (helper.UselessString((string)data["nickname_recolector"]))
                {
                    this.nickname_recolector = null;
                }
                else
                {
                    this.nickname_recolector = (string)data["nickname_recolector"];
                }
            }

            if (data.ContainsKey("id_contenedor"))
            {
                if ((long?)data["id_contenedor"] == null)
                {
                    this.id_contenedor = null;
                }
                else
                {
                    this.id_contenedor = (long)data["id_contenedor"];
                }
            }

            ChepiConstants constants = new ChepiConstants();

            if (data.ContainsKey("latitud"))
            {
                if (new UtilsHelper().InRange(((double)data["latitud"]), constants.minLat, constants.maxLat))
                {
                    this.latitud = (double)data["latitud"];
                }
                else
                {
                    throw new ArgumentException("Latitude invalid");
                }
            }
            else
            {
                throw new ArgumentNullException("Ubicacion constructor : 'latitud' is null");
            }

            if (data.ContainsKey("longitud"))
            {
                if (new UtilsHelper().InRange(((double)data["longitud"]), constants.minLng, constants.maxLng))
                {
                    this.longitud = (double)data["longitud"];
                }
                else
                {
                    throw new ArgumentException("Longitude invalid");
                }
            }
            else
            {
                throw new ArgumentNullException("Ubicacion constructor : 'longitud' is null");
            }
        }

        public Dictionary<string, object> GetInfo(InfoFilter infoFilter)
        {
            if( infoFilter == null)
            {
                return this.GetInfo();
            }

            Dictionary<string, object> info = null;

            if( infoFilter.filter.Any())
            {
                UbicacionSelect ubicacionSelect = new UbicacionSelect();
                switch (infoFilter.mode)
                {
                    case GetInfoModes.Modes.Optimistic:
                        info = new Dictionary<string, object>();
                        if (infoFilter.filter.Contains(ubicacionSelect.id))
                        {
                            info.Add("id", this.id);
                        }

                        if (infoFilter.filter.Contains(ubicacionSelect.nickname_reciclador))
                        {
                            info.Add("nickname_reciclador", this.nickname_reciclador);
                        }

                        if (infoFilter.filter.Contains(ubicacionSelect.id_contenedor))
                        {
                            info.Add("id_contenedor", this.id_contenedor);
                        }

                        if (infoFilter.filter.Contains(ubicacionSelect.nombre))
                        {
                            info.Add("nombre", this.nombre);
                        }

                        if (infoFilter.filter.Contains(ubicacionSelect.latitud))
                        {
                            info.Add("latitud", this.latitud);
                        }

                        if (infoFilter.filter.Contains(ubicacionSelect.longitud))
                        {
                            info.Add("longitud", this.longitud);
                        }

                        if (infoFilter.filter.Contains(ubicacionSelect.direccion))
                        {
                            info.Add("direccion", this.direccion);
                        }
                        break;
                    case GetInfoModes.Modes.Pessimistic:
                        info = this.GetInfo();
                        if (infoFilter.filter.Contains(ubicacionSelect.id))
                        {
                            info.Remove("id");
                        }

                        if (infoFilter.filter.Contains(ubicacionSelect.nickname_reciclador))
                        {
                            info.Remove("nickname_reciclador");
                        }

                        if (infoFilter.filter.Contains(ubicacionSelect.id_contenedor))
                        {
                            info.Remove("id_contenedor");
                        }

                        if (infoFilter.filter.Contains(ubicacionSelect.nombre))
                        {
                            info.Remove("nombre");
                        }

                        if (infoFilter.filter.Contains(ubicacionSelect.latitud))
                        {
                            info.Remove("latitud");
                        }

                        if (infoFilter.filter.Contains(ubicacionSelect.longitud))
                        {
                            info.Remove("longitud");
                        }

                        if (infoFilter.filter.Contains(ubicacionSelect.direccion))
                        {
                            info.Remove("direccion");
                        }
                        break;
                }
            }
            else
            {
                info = this.GetInfo();
            }
            
            return info;
        }

        public Dictionary<string, object> GetInfo()
        {
            return new Dictionary<string, object>
            {
                { "id", this.id },
                { "nickname_reciclador", this.nickname_reciclador },
                { "id_contenedor", this.id_contenedor },
                { "nombre", this.nombre },
                { "latitud", this.latitud },
                { "longitud", this.longitud },
                { "direccion", this.direccion }
            };
        }

        public List<Recoleccion> Closest(List<Recoleccion> recolecciones)
        {
            int closestPointIndex = 0;
            double closestDistance = double.MaxValue;
            Ubicacion currentPoint = this;
            double distance;
            List<Ubicacion> ubis = new List<Ubicacion>();

            using (ChepiEntities context = new ChepiEntities())
            {
                long[] ubi_ids = recolecciones.Select(r => r.id_ubicacion).Distinct().ToArray();
                foreach ( long i in ubi_ids)
                {
                    ubis.Add( context.Ubicacions.SingleOrDefault( u => u.id == i) );
                }
            }
            
            for (int i = 0; i < ubis.Count; i++)
            {
                distance = new UbicacionHelper().GetDistance(currentPoint.Coordinates, ubis[i].Coordinates);
                if (distance < closestDistance)
                {
                    closestPointIndex = i;
                    closestDistance = distance;
                }
            }
            long id = ubis[closestPointIndex].id;
            using (ChepiEntities context = new ChepiEntities())
            {
                List<Recoleccion> result = recolecciones.Where(r => r.id_ubicacion == id ).ToList();
                return result;
            }
        }
    }
}
