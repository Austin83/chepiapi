﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ChepiEntities : DbContext
    {
        public ChepiEntities()
            : base("name=ChepiEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Administrador> Administradors { get; set; }
        public virtual DbSet<Alerta> Alertas { get; set; }
        public virtual DbSet<Bloqueo> Bloqueos { get; set; }
        public virtual DbSet<Contenedor> Contenedors { get; set; }
        public virtual DbSet<Material> Materials { get; set; }
        public virtual DbSet<Reciclador> Recicladors { get; set; }
        public virtual DbSet<Recoleccion> Recoleccions { get; set; }
        public virtual DbSet<Recoleccion_Material> Recoleccion_Material { get; set; }
        public virtual DbSet<Recolector> Recolectors { get; set; }
        public virtual DbSet<Recolector_Recoleccion> Recolector_Recoleccion { get; set; }
        public virtual DbSet<Ruta> Rutas { get; set; }
        public virtual DbSet<Ubicacion> Ubicacions { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
    }
}
