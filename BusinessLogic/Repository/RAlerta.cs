﻿using CrossCutConcerns.Utils;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Repository
{
    public class RAlerta
    {
        public Dictionary<string, object> AddAlerta(long id, string nickname_reciclador)
        {
            try
            {
                Contenedor contenedor;
                Reciclador reciclador;
                Alerta alerta;
                using (ChepiEntities context = new ChepiEntities())
                {
                    if( context.Alertas.SingleOrDefault( a => a.id_contenedor == id && a.nickname_reciclador == nickname_reciclador) != null)
                    {
                        throw new ArgumentException();
                    }
                    alerta = new Alerta(id, nickname_reciclador);
                    contenedor = context.Contenedors.SingleOrDefault(c => c.id == id);
                    reciclador = context.Recicladors.SingleOrDefault(r => r.nickname == nickname_reciclador);
                    context.Alertas.Add(alerta);
                    contenedor.Alertas.Add(alerta);
                    reciclador.Alertas.Add(alerta);
                    context.SaveChanges();
                }
                return new Dictionary<string, object>
                {
                    { "id_contenedor", alerta.id_contenedor },
                    { "nickname_reciclador", alerta.nickname_reciclador }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Dictionary<string,object> GetAlerta(long id, string nickname_reciclador)
        {
            try
            {
                Alerta alerta;
                using (ChepiEntities context = new ChepiEntities())
                {
                    alerta = context.Alertas.SingleOrDefault( a => a.id_contenedor == id && a.nickname_reciclador == nickname_reciclador );
                }
                return alerta.GetInfo();

            }
            catch
            {
                throw new KeyNotFoundException("Alerta con id_contenedor ={ " + id + " } y " +
                    "nickname_reciclador ={ " + nickname_reciclador + " } NO fue encontrado.");
            }
        }

        public List<Dictionary<string, object>> GetAlertasAll()
        {
            try
            {
                List<Alerta> alertas = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    alertas = context.Alertas.ToList();
                }
                List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();
                foreach (Alerta alerta in alertas)
                {
                    resultado.Add(alerta.GetInfo());
                }
                return resultado;
            }
            catch
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public List<Dictionary<string,object>> GetAlertasFiltradas( Dictionary<string,object> data )
        {
            try
            {
                List<Alerta> alertas;
                using (ChepiEntities context = new ChepiEntities())
                {
                    long? id_contenedor = null;
                    string nickname_reciclador = null;

                    if (data.ContainsKey("id_contenedor"))
                    {
                        id_contenedor = (long)data["id_contenedor"];
                    }

                    if (data.ContainsKey("nickname_reciclador"))
                    {
                        nickname_reciclador = (string)data["nickname_reciclador"];
                    }

                    IQueryable<Alerta> query = null;

                    if ( id_contenedor != null )
                    {
                        if (query == null)
                        {
                            query = context.Alertas.Where(a => a.id_contenedor == id_contenedor);
                        }
                        else
                        {
                            query = query.Where(a => a.id_contenedor == id_contenedor);
                        }
                    }

                    if (!new UtilsHelper().UselessString(nickname_reciclador))
                    {
                        if (query == null)
                        {
                            query = context.Alertas.Where(a => a.nickname_reciclador.Contains(nickname_reciclador));
                        }
                        else
                        {
                            query = query.Where(a => a.nickname_reciclador.Contains(nickname_reciclador));
                        }
                    }

                    alertas = query.ToList();
                }
                List<Dictionary<string,object>> result = new List<Dictionary<string, object>>();

                foreach (Alerta alerta in alertas)
                {
                    result.Add(alerta.GetInfo());
                }
                return result;
            }
            catch
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }
    }
}
