﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;
using DataAccess;
using Newtonsoft.Json.Linq;

namespace BusinessLogic.Repository
{
    public class RRecoleccion
    {
        public List<Dictionary<string, object>> GetRecoleccionesAll()
        {
            try
            {
                List<Recoleccion> recolecciones = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    recolecciones = context.Recoleccions.ToList();
                }
                List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();

                RecoleccionSelect selectRecoleccion = new RecoleccionSelect();
                List<int> aux = new List<int>();
                InfoFilter filtroRecoleccion = new InfoFilter(aux, GetInfoModes.Modes.Pessimistic);
                UbicacionSelect selectUbicacion = new UbicacionSelect();
                aux = new List<int>
                {
                    selectUbicacion.id,
                    selectUbicacion.nombre
                };
                InfoFilter filtroUbicacion = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                foreach (Recoleccion recoleccion in recolecciones)
                {
                    resultado.Add(recoleccion.GetInfo(filtroRecoleccion, filtroUbicacion));
                }
                return resultado;
            }
            catch(Exception ex)
            {
                throw new KeyNotFoundException("Query failed !");
            }
        }

        public Dictionary<string, object> GetRecoleccion(long id)
        {
            try
            {
                Recoleccion recoleccion = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    recoleccion = context.Recoleccions.SingleOrDefault(r => r.id == id);
                }

                RecoleccionSelect selectRecoleccion = new RecoleccionSelect();
                List<int> aux = new List<int>();
                InfoFilter filtroRecoleccion = new InfoFilter(aux, GetInfoModes.Modes.Pessimistic);
                UbicacionSelect selectUbicacion = new UbicacionSelect();
                aux = new List<int>
                {
                    selectUbicacion.id,
                    selectUbicacion.nombre
                };
                InfoFilter filtroUbicacion = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                Recoleccion_MaterialSelect selectMaterial = new Recoleccion_MaterialSelect();
                aux = new List<int>
                {
                    selectMaterial.id_material,
                    selectMaterial.cantidad
                };
                InfoFilter filtroMaterial = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                return recoleccion.GetInfo(filtroRecoleccion, filtroUbicacion, filtroMaterial);
            }
            catch
            {
                throw new KeyNotFoundException("Recolección con id ={ " + id + " } NO fue encontrada.");
            }
        }

        public long AddRecoleccion(Dictionary<string, object> data)
        {
            try
            {
                Recoleccion recoleccion = new Recoleccion(data);
                long? id_ubicacion = (long)data["id_ubicacion"];
              
                using (ChepiEntities context = new ChepiEntities())
                {
                    long? id_expirada = null;
                    if ( data.ContainsKey("id_expirada"))
                    {
                        id_expirada = (long)data["id_expirada"];
                    }
                    
                    byte hora = byte.Parse(data["hora"].ToString());
                    DateTime? fecha = null;
                    if (DateTime.
                    TryParseExact((string)data["fecha"], "yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo, DateTimeStyles.None, out DateTime result))
                    {
                        fecha = result;
                    }
                    string nickname_reciclador = (string)data["nickname_reciclador"];
                    
                    if (context.Ubicacions.SingleOrDefault(u => u.id == id_ubicacion) != null)
                    {
                        if (!context.Recoleccions.Where(r => r.id_ubicacion == id_ubicacion &&
                                                              r.hora == hora &&
                                                              r.fecha == fecha &&
                                                              r.nickname_reciclador == nickname_reciclador).Any())
                        {
                            Recoleccion to_expire = context.Recoleccions.SingleOrDefault(r => r.id == id_expirada);
                            if( to_expire != null)
                            {
                                to_expire.estado = (int)EstadosRecoleccion.Estados.expirada;
                            }

                            context.Recoleccions.Add(recoleccion);
                            List<Dictionary<string, object>> materiales = new List<Dictionary<string, object>>();
                            JArray array = (JArray)data["materiales"];
                            foreach (JObject material in array)
                            {
                                materiales.Add(new Dictionary<string, object>
                            {
                                { "id", material.GetValue("id") },
                                { "cantidad", material.GetValue("cantidad") }
                            });
                            }

                            foreach (Dictionary<string, object> dic in materiales)
                            {
                                context.Recoleccion_Material.Add(new Recoleccion_Material(recoleccion.id, dic));
                            }
                            context.SaveChanges();
                        }
                        else
                        {
                            throw new ArgumentException("Recolección ya existe con los parámetros dados.");
                        }
                    }
                    else
                    {
                        throw new KeyNotFoundException("Ubicación con id={ " + id_ubicacion + " } NO se encontró");
                    }

                }
                return recoleccion.id;
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (FormatException ex)
            {
                throw ex;
            }
            catch (KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveRecoleccion(long id)
        {
            throw new NotImplementedException();
        }

        public void UpdateRecoleccion(Dictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        public void RateRecoleccion(long id_recoleccion, string nickname_reciclador, int puntaje)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    int confirmada = (int)EstadosRecoleccion.Estados.confirmada;
                    int expirada = (int)EstadosRecoleccion.Estados.expirada;

                    Recoleccion recoleccion = context.Recoleccions.SingleOrDefault(r => r.nickname_reciclador == nickname_reciclador &&
                       r.id == id_recoleccion && (r.estado == confirmada || r.estado == expirada));
                    if (recoleccion == null )
                    {
                        throw new KeyNotFoundException("No valid recoleccion matches your description");
                    }

                    Recolector_Recoleccion recolector_Recoleccion = context.Recolector_Recoleccion.
                        SingleOrDefault(r => r.id_recoleccion == id_recoleccion);

                    string nickname_recolector = recolector_Recoleccion.nickname_recolector;
                    Recolector recolector = context.Recolectors.SingleOrDefault(r => r.nickname == nickname_recolector);
                    if ( recolector_Recoleccion == null || recolector == null || recolector_Recoleccion.calificacion != 0)
                    {
                        throw new KeyNotFoundException("Failed");
                    }
                    recolector_Recoleccion.calificacion = puntaje;
                    
                    if( recoleccion.estado == confirmada)
                    {
                        recoleccion.estado = (int)EstadosRecoleccion.Estados.completa;
                    }
                    context.SaveChanges();
                    UpdateRecolectorPuntaje(recolector.nickname);
                }
            }
            catch(Exception ex)
            { 
                throw new KeyNotFoundException("Failed");
            }
        }

        private void UpdateRecolectorPuntaje(string nickname)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    int estado = (int)EstadosRecoleccion.Estados.completa;
                    List<Recolector_Recoleccion> recolectores_recolecciones = 
                        context.Recolector_Recoleccion.
                        Where( r => r.nickname_recolector == nickname && r.calificacion > 0 && r.Recoleccion.estado == estado).ToList();
                    int? dividendo = recolectores_recolecciones.Sum(r => (int?)r.calificacion) ?? 0;
                    int? divisor = recolectores_recolecciones.Count();
                    if (dividendo <= 0 || divisor <= 0)
                        return;
                    double puntaje = (double)(dividendo / divisor);
                    Recolector recolector = context.Recolectors.SingleOrDefault(r => r.nickname == nickname);
                    if(recolector == null)
                    {
                        throw new Exception("Recolector no encontrado!");
                    }
                    else
                    {
                        recolector.puntaje = (int)Math.Round(puntaje, MidpointRounding.AwayFromZero);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new KeyNotFoundException("Failed");
            }
        } 

        public List<Dictionary<string, object>> GetRecoleccionesFiltradas(Dictionary<string, object> data)
        {
            try
            {
                List<Recoleccion> recolecciones;
                using (ChepiEntities context = new ChepiEntities())
                {
                    long? id = null;
                    string nickname_reciclador = null;
                    long? id_ubicacion = null;
                    byte? hora = null;
                    string observacion = null;
                    byte? estado = null;
                    DateTime? desde = null;
                    DateTime? publicacion = null;
                    string materialesText = null;
                    long[] materiales = null;

                    if (data.ContainsKey("id"))
                    {
                        id = (long?)data["id"];
                    }
                    if (data.ContainsKey("nickname_reciclador"))
                    {
                        nickname_reciclador = (string)data["nickname_reciclador"];
                    }
                    if (data.ContainsKey("id_ubicacion"))
                    {
                        id_ubicacion = (long?)data["id_ubicacion"];
                    }
                    if (data.ContainsKey("materiales"))
                    {
                        string[] parts = null;
                        materialesText = data["materiales"].ToString();
                        if (!materialesText.Contains(','))
                        {
                            parts = new string[1];
                            parts[0] = materialesText;
                        }
                        else
                        {
                            parts = materialesText.Split(',');
                        }
                    }

                    if (data.ContainsKey("hora"))
                    {
                        hora = Byte.Parse(data["hora"].ToString());
                    }
                    if (data.ContainsKey("observacion"))
                    {
                        observacion = (string)data["observacion"];
                    }
                    if (data.ContainsKey("estado"))
                    {
                        estado = Byte.Parse(data["estado"].ToString());
                    }
                    if (data.ContainsKey("desde"))
                    {
                        if (DateTime.
                            TryParseExact((string)data["desde"], "dd-MM-yyyy",
                            DateTimeFormatInfo.CurrentInfo, DateTimeStyles.None, out DateTime result))
                        {
                            desde = result;
                        }
                    }
                    if (data.ContainsKey("publicacion"))
                    {
                        if (DateTime.
                            TryParseExact((string)data["publicacion"], "dd-MM-yyyy H:m",
                            DateTimeFormatInfo.CurrentInfo, DateTimeStyles.None, out DateTime result))
                        {
                            publicacion = result;
                        }
                    }

                    IQueryable<Recoleccion> query = null;

                    if (id != null)
                    {
                        if (query == null)
                        {
                            query = context.Recoleccions.Where(r => r.id == id);
                        }
                        else
                        {
                            query = query.Where(r => r.id == id);
                        }
                    }

                    if (id_ubicacion != null)
                    {
                        if (query == null)
                        {
                            query = context.Recoleccions.Where(r => r.id_ubicacion == id_ubicacion);
                        }
                        else
                        {
                            query = query.Where(r => r.id_ubicacion == id_ubicacion);
                        }
                    }

                    if (!new UtilsHelper().UselessString(nickname_reciclador))
                    {
                        if (query == null)
                        {
                            query = context.Recoleccions.Where(r => r.nickname_reciclador.Contains(nickname_reciclador));
                        }
                        else
                        {
                            query = query.Where(r => r.nickname_reciclador.Contains(nickname_reciclador));
                        }
                    }

                    if (hora != null)
                    {
                        if (query == null)
                        {
                            query = context.Recoleccions.Where(r => r.hora == hora);
                        }
                        else
                        {
                            query = query.Where(r => r.hora == hora);
                        }
                    }

                    if (!new UtilsHelper().UselessString(observacion))
                    {
                        if (query == null)
                        {
                            query = context.Recoleccions.Where(r => r.observacion.Contains(observacion));
                        }
                        else
                        {
                            query = query.Where(r => r.observacion.Contains(observacion));
                        }
                    }

                    if (estado != null)
                    {
                        if (query == null)
                        {
                            query = context.Recoleccions.Where(r => r.estado == estado);
                        }
                        else
                        {
                            query = query.Where(r => r.estado == estado);
                        }
                    }

                    if (desde != null)
                    {
                        if (query == null)
                        {
                            query = context.Recoleccions.Where(r => r.fecha >= desde);
                        }
                        else
                        {
                            query = query.Where(r => r.fecha >= desde);
                        }
                    }

                    if (materiales != null)
                    {
                        if (query == null)
                        {
                            query = context.Recoleccions;
                            foreach (Recoleccion recoleccion in query)
                            {
                                if (!recoleccion.Contains(materiales))
                                {
                                    query = query.Where(r => r.id != recoleccion.id);
                                }
                            }
                        }
                        else
                        {
                            foreach (Recoleccion recoleccion in query)
                            {
                                if (!recoleccion.Contains(materiales))
                                {
                                    query = query.Where(r => r.id != recoleccion.id);
                                }
                            }
                        }
                    }

                    recolecciones = query.ToList();
                }

                List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();

                RecoleccionSelect recoleccionSelect = new RecoleccionSelect();
                UbicacionSelect ubiSelect = new UbicacionSelect();
                Recoleccion_MaterialSelect recoleccion_MaterialSelect = new Recoleccion_MaterialSelect();
                InfoFilter filtroRecoleccion = new InfoFilter(new List<int>{recoleccionSelect.id_ubicacion, recoleccionSelect.estado },
                                                                GetInfoModes.Modes.Pessimistic);
                InfoFilter filtroUbicacion = new InfoFilter(new List<int>
                {
                    ubiSelect.nombre,
                    ubiSelect.latitud,
                    ubiSelect.longitud,
                    ubiSelect.direccion
                }, GetInfoModes.Modes.Optimistic);

                InfoFilter filtroMaterial = new InfoFilter(new List<int>
                {
                    recoleccion_MaterialSelect.id_material,
                    recoleccion_MaterialSelect.cantidad
                }, GetInfoModes.Modes.Optimistic);

                foreach (Recoleccion recoleccion in recolecciones)
                {
                    resultado.Add(recoleccion.GetInfo(filtroRecoleccion, filtroUbicacion, filtroMaterial));
                }
                return resultado;

            }
            catch (Exception ex)
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public List<Dictionary<string, object>> GetRecoleccionesReciclador(string nickname, int? tiempo,
                                                                    int? estado, string nickname_reco)
        {
            try
            {
                List<Recoleccion> recolecciones = null;
                List<Dictionary<string, object>> dics = new List<Dictionary<string, object>>();
                using (ChepiEntities context = new ChepiEntities())
                {
                    recolecciones = context.Recoleccions.Where(r => r.nickname_reciclador == nickname)
                        .OrderByDescending(r => r.fecha).ThenBy(r => r.hora).ToList();


                    if (tiempo != null)
                    {
                        DateTime dateTime = new UtilsHelper().GetFiltroTiempo((int)tiempo);
                        recolecciones = recolecciones.Where(r => r.publicacion >= dateTime).ToList();
                    }

                    if (estado != null)
                    {
                        recolecciones = recolecciones.Where(r => r.estado == estado).ToList();
                    }

                    if (nickname_reco != null)
                    {
                        recolecciones = recolecciones.Where(r => r.Recolector_Recoleccion != null && r.Recolector_Recoleccion.nickname_recolector == nickname_reco).ToList();
                    }
                }

                RecoleccionSelect selectRecoleccion = new RecoleccionSelect();
                List<int> aux = new List<int>
                {
                    selectRecoleccion.id_ubicacion,
                    selectRecoleccion.nickname_reciclador
                };
                InfoFilter filtroRecoleccion = new InfoFilter(aux, GetInfoModes.Modes.Pessimistic);
                UbicacionSelect selectUbicacion = new UbicacionSelect();
                aux = new List<int>
                    {
                        selectUbicacion.id,
                        selectUbicacion.nombre,
                        selectUbicacion.direccion,
                        selectUbicacion.latitud,
                        selectUbicacion.longitud
                    };
                InfoFilter filtroUbicacion = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                Recoleccion_MaterialSelect selectMaterial = new Recoleccion_MaterialSelect();
                aux = new List<int>
                    {
                        selectMaterial.id_material,
                        selectMaterial.cantidad
                    };
                InfoFilter filtroMaterial = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                foreach (Recoleccion recoleccion in recolecciones)
                {
                    dics.Add(recoleccion.GetInfo(filtroRecoleccion, filtroUbicacion, filtroMaterial));
                }
                return dics;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Dictionary<string, object>> GetRecoleccionesRecolectorCompletas(string nickname)
        {
            try
            {
                List<Recoleccion> recolecciones = null;
                List<Dictionary<string, object>> dics = new List<Dictionary<string, object>>();
                int completa = (int)EstadosRecoleccion.Estados.completa;
                using (ChepiEntities context = new ChepiEntities())
                {
                    recolecciones = context.Recoleccions.Where(r => r.Recolector_Recoleccion.nickname_recolector == nickname
                                                                && r.estado == completa).OrderByDescending( r => r.fecha ).ToList();

                    RecoleccionSelect selectRecoleccion = new RecoleccionSelect();
                    List<int> aux = new List<int>();
                    InfoFilter filtroRecoleccion = new InfoFilter(aux, GetInfoModes.Modes.Pessimistic);
                    UbicacionSelect selectUbicacion = new UbicacionSelect();
                    aux = new List<int>
                    {
                        selectUbicacion.id,
                        selectUbicacion.nombre
                    };
                    InfoFilter filtroUbicacion = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                    Recoleccion_MaterialSelect selectMaterial = new Recoleccion_MaterialSelect();
                    aux = new List<int>
                    {
                        selectMaterial.id_material,
                        selectMaterial.cantidad
                    };
                    InfoFilter filtroMaterial = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                    foreach (Recoleccion recoleccion in recolecciones)
                    {
                        dics.Add(recoleccion.GetInfo(filtroRecoleccion, filtroUbicacion, filtroMaterial));
                    }
                }
                return dics;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Dictionary<string, object>> GetRecoleccionesReservables( string nickname_recolector ,Dictionary<string,object> data)
        {
            try
            {
                List<Recoleccion> recolecciones;
                using (ChepiEntities context = new ChepiEntities())
                {
                    DateTime? fecha = null;
                    byte? hora = null;
                    long[] materiales = null;

                    if( data.ContainsKey("materiales"))
                    {
                        materiales = (long[])data["materiales"];
                    }
                    
                    if ( data.ContainsKey("fecha") && 
                        DateTime.TryParseExact((string)data["fecha"], "yyyy-MM-dd", DateTimeFormatInfo.CurrentInfo, DateTimeStyles.None, out DateTime result))
                    {
                        fecha = result;
                    }

                    if ( data.ContainsKey("hora") && Byte.TryParse(data["hora"].ToString(), out byte out_hora))
                    {
                        hora = out_hora;
                    }
                    byte disponible = (byte)EstadosRecoleccion.Estados.disponible;
                    IQueryable<Recoleccion> query = context.Recoleccions.Where(r => r.estado == disponible || r.nickname_recolector == nickname_recolector);
                    
                    if ( hora != null )
                    {
                        query = query.Where(r => r.hora == hora);
                    }
                    
                    if (fecha != null)
                    {
                        query = query.Where(r => r.fecha == fecha);
                    }

                    if (materiales != null)
                    {
                        if (query == null)
                        {
                            query = context.Recoleccions;
                            foreach (Recoleccion recoleccion in query)
                            {
                                if (!recoleccion.Contains(materiales))
                                {
                                    query = query.Where(r => r.id != recoleccion.id );
                                }
                            }
                        }
                        else
                        {
                            foreach (Recoleccion recoleccion in query)
                            {
                                if (!recoleccion.Contains(materiales))
                                {
                                    query = query.Where(r => r.id != recoleccion.id );
                                }
                            }
                        }
                    }
                    List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();

                    if (query.Any())
                    {
                        recolecciones = query.ToList();

                        Usuario usuario = context.Usuarios.SingleOrDefault(u => u.nickname == nickname_recolector);
                        string nickname_reciclador = null;
                        List<Recoleccion> recolecciones_aux = recolecciones.ToList();
                        foreach (Recoleccion recoleccion in recolecciones)
                        {
                            nickname_reciclador = recoleccion.nickname_reciclador;
                            if (usuario.Bloqueado(recoleccion.nickname_reciclador))
                            {
                                recolecciones_aux.RemoveAll(r => r.nickname_reciclador == nickname_reciclador);
                            }
                        }
                        recolecciones = recolecciones_aux;
                        UbicacionSelect ubiSelect = new UbicacionSelect();
                        Recoleccion_MaterialSelect recoleccion_MaterialSelect = new Recoleccion_MaterialSelect();
                        InfoFilter filtroRecoleccion = new InfoFilter(new List<int>(), GetInfoModes.Modes.Pessimistic);
                        InfoFilter filtroUbicacion = new InfoFilter(new List<int>
                    {
                        ubiSelect.id,
                        ubiSelect.nickname_reciclador,
                        ubiSelect.nombre,
                        ubiSelect.latitud,
                        ubiSelect.longitud,
                        ubiSelect.direccion
                    }, GetInfoModes.Modes.Optimistic);

                        InfoFilter filtroMaterial = new InfoFilter(new List<int>
                    {
                        recoleccion_MaterialSelect.id_material,
                        recoleccion_MaterialSelect.cantidad
                    }, GetInfoModes.Modes.Optimistic);

                        foreach (Recoleccion recoleccion in recolecciones)
                        {
                            resultado.Add(recoleccion.GetInfo(filtroRecoleccion, filtroUbicacion, filtroMaterial));
                        }
                    }
                    
                    return resultado;
                }
            }
            catch (Exception ex)
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public void ReservarRecolecion(Dictionary<string, object> data)
        {
            try
            {
                long id_recoleccion = (long)data["id"];
                Recoleccion recoleccion = null;
                Byte reservada = (byte)EstadosRecoleccion.Estados.reservada;
                using (ChepiEntities context = new ChepiEntities())
                {
                    recoleccion = context.Recoleccions.SingleOrDefault(r => r.id == id_recoleccion);
                    if (recoleccion.estado == reservada)
                    {
                        throw new ArgumentException("Recolección no puede ser reservada");
                    }
                    recoleccion.estado = reservada;
                    recoleccion.nickname_recolector = (string)data["nickname_recolector"];
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LiberarRecolecciones( long[] ids )
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    byte disponible = (byte)EstadosRecoleccion.Estados.disponible;
                    Recoleccion rec = null;
                    foreach( long id in ids)
                    {
                        rec = context.Recoleccions.SingleOrDefault(r => r.id == id);
                        rec.estado = disponible;
                        rec.nickname_recolector = null;
                    }
                    context.SaveChanges();
                }
            }
            catch (KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long[] ConfirmarRuta(string nickname, Dictionary<string, object> data)
        {
            try
            {
                JArray parts = (JArray)data["ids"];
                long[] ids = new long[parts.Count()];
                for (int i = 0; i < parts.Count(); i++)
                {
                    ids[i] = (long)parts[i];
                }
                long[] ids_ruta = this.GetRuta( nickname, ids);
                Dictionary<string, object> dictionary = null;
                Recoleccion rec = null;
                Ruta ruta = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    ruta = new Ruta(nickname, data);
                    context.Rutas.Add(ruta);
                    int i = 0;
                    foreach (long id in ids_ruta)
                    {
                        rec = context.Recoleccions.SingleOrDefault(r => r.id == id);
                        if (rec != null)
                        {
                            rec.estado = (byte)EstadosRecoleccion.Estados.confirmada;
                            rec.nickname_recolector = null;
                        }
                        context.Recolector_Recoleccion.Add(new Recolector_Recoleccion(dictionary = new Dictionary<string, object>
                        {
                            { "id_recoleccion", id },
                            { "nickname_recolector", nickname },
                            { "id_ruta", ruta.id },
                            { "orden_ruta", i++ }
                        }));
                    }
                    context.SaveChanges();
                }
                return ids_ruta;
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (FormatException ex)
            {
                throw ex;
            }
            catch (KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long[] GetRuta(string nickname, long[] ids_recolecciones)
        {
            
            Ubicacion inicio = null;
            Recoleccion actual = null;
            List<Recoleccion> restantes = new List<Recoleccion>();
            using (ChepiEntities context = new ChepiEntities())
            {
                inicio = context.Ubicacions.SingleOrDefault(u => u.nickname_recolector == nickname);
                if (ids_recolecciones.Length == 1)
                {
                    return new long[1] { ids_recolecciones[0] };
                }

                foreach (long id in ids_recolecciones)
                {
                    restantes.Add(context.Recoleccions.SingleOrDefault(r => r.id == id));
                }
            }
            List<long> ruta = new List<long>();
            List<Recoleccion> evaluados = null;
            Recoleccion siguiente = null;

            evaluados = GetRutaPrimerNodo(inicio, restantes);
            if (evaluados.Count == 1)
            {
                siguiente = evaluados[0];
                ruta.Add(siguiente.id);
                long id = siguiente.id;
                restantes.Remove(restantes.SingleOrDefault(r => r.id == id));
                ids_recolecciones = ids_recolecciones.Where(i => i != id).ToArray();
            }
            if (evaluados.Count > 1)
            {
                siguiente = evaluados.Last();
                ruta.AddRange(evaluados.Select(e => e.id));

                foreach (Recoleccion rec in evaluados)
                {
                    long id = rec.id;
                    restantes.Remove(restantes.SingleOrDefault(r => r.id == id));
                    ids_recolecciones = ids_recolecciones.Where(i => i != id).ToArray();
                }
            }
            actual = restantes.SingleOrDefault(r => r.id == ids_recolecciones[0]);
            foreach ( long id in ids_recolecciones)
            {
                long aux_id;
                if (!ids_recolecciones.Any())
                    break;
                if ( ids_recolecciones.Count() == 1 )
                {
                    siguiente = restantes.SingleOrDefault(r => r.id == id);
                    ruta.Add(siguiente.id);
                    aux_id = siguiente.id;
                    restantes.Remove(restantes.SingleOrDefault(r => r.id == aux_id));
                    break;
                }

                evaluados = actual.Closest(restantes);
                if (evaluados.Count == 1)
                {
                    ruta.Add(evaluados[0].id);
                    aux_id = evaluados[0].id;
                    restantes.Remove(restantes.SingleOrDefault(r => r.id == aux_id));
                    ids_recolecciones = ids_recolecciones.Where(i => i != aux_id).ToArray();
                    actual = siguiente;
                }
                if (evaluados.Count > 1)
                {
                    siguiente = evaluados.Last();
                    ruta.AddRange(evaluados.Select(e => e.id));

                    foreach (Recoleccion rec in evaluados)
                    {
                        long rec_id = rec.id;
                        restantes.Remove(restantes.SingleOrDefault(r => r.id == rec_id));
                        ids_recolecciones = ids_recolecciones.Where(i => i != rec_id).ToArray();
                    }
                }
            }

            return ruta.ToArray();
        }

        private List<Recoleccion> GetRutaPrimerNodo(Ubicacion inicio, List<Recoleccion> recolecciones)
        {
            return inicio.Closest(recolecciones);
        }
        
        public List<Dictionary<string, object>> GetTopResiduos()
        {
            try
            {
                using(ChepiEntities context = new ChepiEntities())
                {
                    List<long> idMateriales = context.Materials.Select(m => m.id).Distinct().ToList();
                    List<Dictionary<string, object>> materiales = new List<Dictionary<string, object>>();
                    int cantidad;
                    int frecuencia;
                    int estado = (int)EstadosRecoleccion.Estados.completa;
                    List<Recoleccion_Material> query = null;
                    
                    foreach(long id in idMateriales)
                    {
                        query = context.Recoleccion_Material.Where(m => m.id_material == id && m.Recoleccion.estado == estado).ToList();
                        cantidad = query.Sum(r => r.cantidad);
                        frecuencia = query.Count();
                        if(frecuencia > 0)
                        {
                            materiales.Add(new Dictionary<string, object>
                            {
                                { "id", id },
                                { "nombre", context.Materials.SingleOrDefault(m => m.id == id).nombre },
                                { "cantidad", cantidad },
                                { "frecuencia", frecuencia }
                            });
                        }
                    }

                    return materiales;
                }
            }
            catch(Exception ex)
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }
    }
}
