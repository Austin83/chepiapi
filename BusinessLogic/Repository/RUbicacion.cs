﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;
using DataAccess;
using GeoCoordinatePortable;

namespace BusinessLogic.Repository
{
    class RUbicacion
    {
        public List<Dictionary<string,object>> GetUbicacionesAll()
        {
            try
            {
                List<Ubicacion> listaUbicaciones = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    listaUbicaciones = context.Ubicacions.ToList();
                }
                List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();
                foreach (Ubicacion ubi in listaUbicaciones)
                {
                    resultado.Add(ubi.GetInfo());
                }
                return resultado;
            }
            catch
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public Dictionary<string, object> GetUbicacion(long id)
        {
            try
            {
                Ubicacion ubicacion = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    ubicacion = context.Ubicacions.SingleOrDefault(s => s.id == id);
                }
                UbicacionSelect select = new UbicacionSelect();
                List<int> filtro = new List<int>
                {
                    select.id,
                    select.nombre,
                    select.nickname_reciclador
                }; 
                return ubicacion.GetInfo( new InfoFilter(filtro, GetInfoModes.Modes.Optimistic) );
            }
            catch
            {
                throw new KeyNotFoundException("Ubicacion de id ={ " + id + " } NO fue encontrado.");
            }
        }
        
        public long AddUbicacion(Dictionary<string, object> data)
        {
            try
            {
                Ubicacion ubicacion;
                using (ChepiEntities context = new ChepiEntities())
                {
                    if ( data.ContainsKey( "nickname_reciclador" ) )
                    {
                        string nickname_reciclador = (string)data["nickname_reciclador"];
                        string nombre = (string)data["nombre"];
                        Ubicacion ubi = context.Ubicacions.SingleOrDefault(r => r.nickname_reciclador == nickname_reciclador &&
                                                                                r.nombre == nombre );
                        if( ubi != null )
                        {
                            throw new ArgumentException("Already exists !");
                        }
                    }
                    else
                    {
                        if ( data.ContainsKey("id_contenedor") )
                        {
                            long id_contenedor = (long)data["id_contenedor"];
                            Ubicacion ubi = context.Ubicacions.SingleOrDefault(r => r.id_contenedor == id_contenedor );
                            if (ubi != null)
                            {
                                throw new ArgumentException("Already exists !");
                            }
                        }
                    }

                    ubicacion = new Ubicacion(data);
                    context.Ubicacions.Add(ubicacion);
                    context.SaveChanges();
                }
                return ubicacion.id;
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
        }

        public void RemoveUbicacion(long id_contenedor)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    Ubicacion ubicacion = context.Ubicacions.SingleOrDefault( u => u.id_contenedor == id_contenedor );
                    if (ubicacion != null)
                    {
                        context.Ubicacions.Remove(ubicacion);
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new KeyNotFoundException("Ubicacion de id_contenedor ={ " + id_contenedor + " } NO fue borrado");
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveUbicacion( long id_ubicacion, string nickname_reciclador)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    Ubicacion ubicacion = context.Ubicacions.SingleOrDefault(u => u.nickname_reciclador == nickname_reciclador &&
                                                                                u.id == id_ubicacion);
                    if (ubicacion != null)
                    {
                        ubicacion.nickname_reciclador = null;
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new KeyNotFoundException("Ubicacion de nickname_reciclador ={ " + nickname_reciclador + " } NO fue borrado");
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateUbicacion(Dictionary<string,object> data)
        {
            throw new NotImplementedException();
        }

        public List<Dictionary<string,object>> GetUbicacionesFiltradas(Dictionary<string, object> data)
        {
            try
            {
                List<Ubicacion> ubis;
                using (ChepiEntities context = new ChepiEntities())
                {
                    long? id = null;
                    string nickname_reciclador = null;
                    long? id_contenedor = null;
                    string nombre = null;
                    string direccion = null;
                    double? latitud = null;
                    double? longitud = null;
                    string rangoText = null;
                    double? rango = null;
                    
                    if ( data.ContainsKey("id"))
                    {
                        id = (long?)data["id"];
                    }

                    if (data.ContainsKey("nickname_reciclador"))
                    {
                        nickname_reciclador = (string)data["nickname_reciclador"];
                    }

                    if (data.ContainsKey("id_contenedor"))
                    {
                        id_contenedor = (long?)data["id_contenedor"];
                    }

                    if (data.ContainsKey("nombre"))
                    {
                        nombre = (string)data["nombre"];
                    }

                    if (data.ContainsKey("direccion"))
                    {
                        direccion = (string)data["direccion"];
                    }

                    if (data.ContainsKey("latitud"))
                    {
                        latitud = (double?)data["latitud"];
                    }

                    if (data.ContainsKey("longitud"))
                    {
                        longitud = (double?)data["longitud"];
                    }

                    if (data.ContainsKey("rango"))
                    {
                        if (data["rango"] != null)
                        {
                            rangoText = data["rango"].ToString();
                            bool condition = double.TryParse(rangoText + ".00", out double aux);
                            if (condition)
                            {
                                rango = aux;
                            }
                        }
                    }

                    IQueryable<Ubicacion> query = null;

                    if ( id != null)
                    {
                        if( query == null)
                        {
                            query = context.Ubicacions.Where(r => r.id == id );
                        }
                        else
                        {
                            query = query.Where(r => r.id == id );
                        }
                    }

                    if ( ! new UtilsHelper().UselessString( nickname_reciclador ) )
                    {
                        if( query == null)
                        {
                            query = context.Ubicacions.Where(r => r.nickname_reciclador.Contains( nickname_reciclador ));
                        }
                        else
                        {
                            query = query.Where(r => r.nickname_reciclador.Contains( nickname_reciclador ));
                        }
                    }

                    if ( id_contenedor != null)
                    {
                        if( query == null)
                        {
                            query = context.Ubicacions.Where(r => r.id_contenedor == id_contenedor );
                        }
                        else
                        {
                            query = query.Where(r => r.id_contenedor == id_contenedor );
                        }
                    }

                    if ( ! new UtilsHelper().UselessString( nombre ) )
                    {
                        if( query == null)
                        {
                            query = context.Ubicacions.Where(r => r.nombre.Contains( nombre ));
                        }
                        else
                        {
                            query = query.Where(r => r.nombre.Contains( nombre ));
                        }
                        
                    }

                    if ( ! new UtilsHelper().UselessString( direccion ) )
                    {
                        if( query == null)
                        {
                            query = context.Ubicacions.Where(r => r.direccion.Contains( direccion ));
                        }
                        else
                        {
                            query = query.Where(r => r.direccion.Contains( direccion ));
                        }
                    }

                    if( latitud != null && longitud != null && rango != null )
                    {
                        if( query == null)
                        {
                            query = GetUbicacionesInRange(context.Ubicacions, new Coordinates((double)latitud, (double)longitud), (double)rango);
                        }
                        else
                        {
                            query = GetUbicacionesInRange(query, new Coordinates((double)latitud, (double)longitud), (double)rango);
                        }
                    }

                    ubis = query.ToList();
                }

                List<Dictionary<string,object>> result = new List<Dictionary<string, object>>();
                UbicacionSelect ubicacionSelect = new UbicacionSelect();
                List<int> filtro = new List<int>
                {
                    ubicacionSelect.id,
                    ubicacionSelect.nombre,
                    ubicacionSelect.nickname_reciclador
                };

                foreach (Ubicacion ubi in ubis)
                {
                    result.Add(ubi.GetInfo( new InfoFilter(filtro,GetInfoModes.Modes.Optimistic) ));
                }
                return result;
            }
            catch(InvalidCastException)
            {
                throw new InvalidCastException("Bad request!");
            }
            catch(Exception)
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        private IQueryable<Ubicacion> GetUbicacionesInRange( IQueryable<Ubicacion> data , Coordinates origin, double range)
        {
            foreach( Ubicacion ubi in data)
            {
                if ( ! InRange(origin, ubi.Coordinates, range) )
                {
                    data = data.Where( u => u.id != ubi.id );
                }
            }
            return data;
        }

        public double GetDistance(Coordinates a, Coordinates b)
        {
            string msg = null;
            try
            {
                GeoCoordinate geo_a = new GeoCoordinate(a.latitude, a.longitude);
                GeoCoordinate geo_b = new GeoCoordinate(b.latitude, b.longitude);
                msg = geo_a.ToString() +
                    geo_b.ToString();

                return geo_a.GetDistanceTo(geo_b);
            }
            catch (Exception)
            {
                throw new Exception(msg);
            }
        }

        private bool InRange(Coordinates origin, Coordinates other, double range)
        {
            return GetDistance(origin, other) < range;
        }
    }
}
