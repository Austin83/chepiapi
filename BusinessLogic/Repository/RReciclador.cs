﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;
using DataAccess;

namespace BusinessLogic.Repository
{
    class RReciclador : RUsuario
    {
        public List<Dictionary<string, object>> GetRecicladoresAll()
        {
            try
            {
                List<Reciclador> recicladores = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    recicladores = context.Recicladors.ToList();
                }
                List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();

                RecicladorSelect selectReciclador = new RecicladorSelect();
                UsuarioSelect selectUsuario = new UsuarioSelect();
                List<int> aux = new List<int>
                {
                    selectReciclador.usuario
                };
                InfoFilter filtroReciclador = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                aux = new List<int>
                {
                    selectUsuario.imagen
                };
                InfoFilter filtroUsuario = new InfoFilter(aux, GetInfoModes.Modes.Pessimistic);

                foreach(Reciclador reciclador in recicladores)
                {
                    resultado.Add(reciclador.GetInfo( filtroReciclador, filtroUsuario ));
                }
                return resultado;
            }
            catch
            {
                throw new KeyNotFoundException("Query failed !");
            }
        }

        public List<Dictionary<string, object>> GetRecicladoresFiltrados(Dictionary<string,object> data)
        {
            try
            {
                List<Reciclador> recicladores;
                using (ChepiEntities context = new ChepiEntities())
                {
                    string nickname = null;
                    string nombre = null;
                    string apellido = null;
                    string mail = null;
                    byte? edad = null;
                    long? creditos = null;
                    bool? estado = null;

                    if (data.ContainsKey("nickname"))
                    {
                        nickname = (string)data["nickname"];
                    }

                    if (data.ContainsKey("nombre"))
                    {
                        nombre = (string)data["nombre"];
                    }

                    if (data.ContainsKey("apellido"))
                    {
                        apellido = (string)data["apellido"];
                    }

                    if (data.ContainsKey("mail"))
                    {
                        mail = (string)data["mail"];
                    }

                    if (data.ContainsKey("edad"))
                    {
                        edad = Byte.Parse(data["edad"].ToString());
                    }

                    if (data.ContainsKey("creditos"))
                    {
                        creditos = (long)data["creditos"];
                    }

                    if (data.ContainsKey("estado"))
                    {
                        estado = (bool)data["estado"];
                    }

                    IQueryable<Reciclador> query = null;

                    if ( ! new UtilsHelper().UselessString(nickname))
                    {
                        if (query == null)
                        {
                            query = context.Recicladors.Where(r => r.nickname.Contains(nickname));
                        }
                        else
                        {
                            query = query.Where(r => r.nickname.Contains(nickname));
                        }
                    }

                    if ( ! new UtilsHelper().UselessString(nombre))
                    {
                        if (query == null)
                        {
                            query = context.Recicladors.Where(r => r.Usuario.nombre.Contains(nombre));
                        }
                        else
                        {
                            query = query.Where(r => r.Usuario.nombre.Contains(nombre));
                        }
                    }

                    if (!new UtilsHelper().UselessString(apellido))
                    {
                        if (query == null)
                        {
                            query = context.Recicladors.Where(r => r.Usuario.apellido.Contains(apellido));
                        }
                        else
                        {
                            query = query.Where(r => r.Usuario.apellido.Contains(apellido));
                        }
                    }

                    if (!new UtilsHelper().UselessString(mail))
                    {
                        if (query == null)
                        {
                            query = context.Recicladors.Where(r => r.Usuario.mail.Contains(mail));
                        }
                        else
                        {
                            query = query.Where(r => r.Usuario.mail.Contains(mail));
                        }
                    }

                    if ( edad != null )
                    {
                        if (query == null)
                        {
                            query = context.Recicladors.Where(r => r.Usuario.edad == edad);
                        }
                        else
                        {
                            query = query.Where(r => r.Usuario.edad == edad);
                        }
                    }

                    if ( creditos != null )
                    {
                        if (query == null)
                        {
                            query = context.Recicladors.Where(r => r.Usuario.creditos == creditos);
                        }
                        else
                        {
                            query = query.Where(r => r.Usuario.creditos == creditos);
                        }
                    }

                    if (estado != null)
                    {
                        if (query == null)
                        {
                            query = context.Recicladors.Where(r => r.Usuario.estado == estado);
                        }
                        else
                        {
                            query = query.Where(r => r.Usuario.estado == estado);
                        }
                    }

                    recicladores = query.ToList();
                }

                List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();

                RecicladorSelect selectReciclador = new RecicladorSelect();
                UsuarioSelect selectUsuario = new UsuarioSelect();
                List<int> aux = new List<int>
                {
                    selectReciclador.usuario
                };
                InfoFilter filtroReciclador = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                aux = new List<int>
                {
                    selectUsuario.imagen
                };
                InfoFilter filtroUsuario = new InfoFilter(aux, GetInfoModes.Modes.Pessimistic);

                foreach (Reciclador rec in recicladores )
                {
                    resultado.Add( rec.GetInfo( filtroReciclador, filtroUsuario ));
                }
                return resultado;
            }
            catch
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public Dictionary<string, object> GetReciclador(string nickname, UserData userData)
        {
            try
            {
                Reciclador reciclador = null;
                Usuario usuario = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    reciclador = context.Recicladors.SingleOrDefault(s => s.nickname == nickname);
                    usuario = context.Usuarios.SingleOrDefault(u => u.nickname == nickname);
                }

                RecicladorSelect selectReciclador = new RecicladorSelect();
                List<int> aux = null;
                InfoFilter filtroReciclador = null;

                if ( nickname == userData.nickname)
                {
                    aux = new List<int>
                    {
                        selectReciclador.usuario,
                        selectReciclador.ubicaciones
                    };
                    filtroReciclador = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                    return reciclador.GetInfo(filtroReciclador);
                }
                
                aux = new List<int>
                {
                    selectReciclador.usuario
                };
                filtroReciclador = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                
                return reciclador.GetInfo( filtroReciclador );
            }
            catch
            {
                throw new KeyNotFoundException("Reciclador de nickname ={ " + nickname + " } NO fue encontrado.");
            }
        }

        public string AddReciclador(Dictionary<string, object> data)
        {
            Reciclador reciclador = null;
            try
            {
                string nickname;
                if ( data.ContainsKey("nickname") )
                {
                    nickname = (string)data["nickname"];
                }
                else
                {
                    throw new ArgumentNullException("nickname");
                }
                
                Usuario aux = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    aux = context.Usuarios.SingleOrDefault( u => u.nickname == nickname);
                }

                if (aux != null)
                {
                    throw new Exception("Usuario ya existe");
                }

                using (ChepiEntities context = new ChepiEntities())
                {
                    Usuario usuario = new Usuario(data);
                    context.Usuarios.Add(usuario);
                    reciclador = new Reciclador(usuario.nickname);
                    context.Recicladors.Add( reciclador );
                    reciclador.Usuario = usuario;
                    usuario.Reciclador = reciclador;
                    context.SaveChanges();
                }
                return reciclador.nickname;
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveReciclador(string nickname)
        {
            this.InactiveCheck(nickname);

            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    Reciclador reciclador = context.Recicladors.SingleOrDefault(x => x.nickname == nickname);
                    context.Recicladors.Remove(reciclador);
                    Usuario usuario = context.Usuarios.SingleOrDefault(x => x.nickname == nickname);
                    context.Usuarios.Remove(usuario);
                    context.SaveChanges();
                }
            }
            catch
            {
                throw new Exception("Reciclador de nickname ={ " + nickname + " } NO fue borrado.");
            }
        }

        public List<Dictionary<string, object>> GetTopRecicladores(string filtro)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {

                    List<Dictionary<string, object>> recicladores = new List<Dictionary<string, object>>();
                    int estado = (int)EstadosRecoleccion.Estados.completa;
                    int? recolecciones;
                    int? materiales;
                    List<string> nombresRecicladores = null;
                    if(filtro != null)
                    {
                        filtro = filtro.ToLower();
                        nombresRecicladores = context.Recicladors.Where( r => r.nickname.ToLower().Contains(filtro) ).OrderByDescending(
                        r => r.Recoleccions.Count(rr => rr.nickname_reciclador == r.nickname && rr.estado == estado)).Select(r => r.nickname).ToList();
                    }
                    else
                    {
                        nombresRecicladores = context.Recicladors.OrderByDescending(
                        r => r.Recoleccions.Count(rr => rr.nickname_reciclador == r.nickname && rr.estado == estado)).Select(r => r.nickname).ToList();
                    }

                    foreach (string nick in nombresRecicladores)
                    {
                        recolecciones = context.Recoleccions.Count(r => r.nickname_reciclador == nick && r.estado == estado);
                        materiales = context.Recoleccion_Material.Where(r => r.Recoleccion.nickname_reciclador == nick && r.Recoleccion.estado == estado).Sum(r => (int?)r.cantidad) ?? 0;

                        if(recolecciones > 0)
                        {
                            recicladores.Add(new Dictionary<string, object>
                            {
                                { "nickname", nick },
                                { "recolecciones", recolecciones},
                                { "materiales", materiales }
                            });
                        }
                    }

                    return recicladores;
                }
            }
            catch (Exception ex)
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }
        
    }
}
