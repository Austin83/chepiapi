﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;
using DataAccess;

namespace BusinessLogic.Repository
{
    public class RMaterial
    {
        public List<Dictionary<string,object>> GetTop()
        {
            try
            {
                List<Material> listaMateriales = null;
                List<Dictionary<string, object>> materiales = new List<Dictionary<string, object>>();
                using (ChepiEntities context = new ChepiEntities())
                {
                     listaMateriales = context.Materials.OrderByDescending( r => r.Recoleccion_Material.Where(rm => rm.id_material == r.id).Count() ).ToList();
                     foreach( Material material in listaMateriales)
                     {
                        materiales.Add(new Dictionary<string, object> { { "id", material.id }, { "nombre", material.nombre },
                            { "reciclajes", material.Recoleccion_Material.Where(rm => rm.id_material == material.id).Count() } });
                     }
                }
                return materiales;
            }
            catch(Exception ex)
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public List<Dictionary<string, object>> GetMaterialesAll()
        {
            try
            {
                List<Material> materiales = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    materiales = context.Materials.ToList();
                }
                List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();
                MaterialSelect select = new MaterialSelect();
                List<int> filtro = new List<int>
                {
                    select.nickname_admin
                };

                foreach (Material material in materiales)
                {
                    resultado.Add( material.GetInfo( new InfoFilter( filtro, GetInfoModes.Modes.Pessimistic ) ) );
                }
                return resultado;
            }
            catch
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public Dictionary<string, object> GetMaterial(long id)
        {
            try
            {
                Material material = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    material = context.Materials.SingleOrDefault(m => m.id == id);
                }
                MaterialSelect select = new MaterialSelect();
                List<int> filtro = new List<int>
                {
                    select.nickname_admin
                };
                return material.GetInfo(new InfoFilter(filtro, GetInfoModes.Modes.Pessimistic));
            }
            catch
            {
                throw new KeyNotFoundException("Material de id ={ " + id + " } NO fue encontrado.");
            }
        }

        public List<Dictionary<string, object>> GetMaterialesFiltrados(Dictionary<string, object> data)
        {
            try
            {
                List<Material> materiales;
                using (ChepiEntities context = new ChepiEntities())
                {
                    long? id = null;
                    string nombre = null;
                    string nickname_admin = null;
                    bool? cuantificable = null;
                    string color = null;
                    bool? bloqueado = null;

                    if (data.ContainsKey("id"))
                    {
                        id = (long?)data["id"];
                    }

                    if (data.ContainsKey("nombre"))
                    {
                        nombre = (string)data["nombre"];
                    }

                    if (data.ContainsKey("nickname_admin"))
                    {
                        nickname_admin = (string)data["nickname_admin"];
                    }

                    if (data.ContainsKey("cuantificable"))
                    {
                        cuantificable = (bool?)data["cuantificable"];
                    }

                    if (data.ContainsKey("bloqueado"))
                    {
                        bloqueado = (bool?)data["bloqueado"];
                    }

                    if (data.ContainsKey("color"))
                    {
                        color = (string)data["color"];
                    }

                    IQueryable<Material> query = null;

                    if ( id != null )
                    {
                        if (query == null)
                        {
                            query = context.Materials.Where(m => m.id == id);
                        }
                        else
                        {
                            query = query.Where(m => m.id == id);
                        }
                    }

                    if (cuantificable != null)
                    {
                        if (query == null)
                        {
                            query = context.Materials.Where(m => m.cuantificable == cuantificable);
                        }
                        else
                        {
                            query = query.Where(m => m.cuantificable == cuantificable);
                        }
                    }

                    if (bloqueado != null)
                    {
                        if (query == null)
                        {
                            query = context.Materials.Where(m => m.bloqueado == bloqueado);
                        }
                        else
                        {
                            query = query.Where(m => m.bloqueado == bloqueado);
                        }
                    }

                    if (!new UtilsHelper().UselessString(nombre))
                    {
                        if (query == null)
                        {
                            query = context.Materials.Where(m => m.nombre.Contains(nombre));
                        }
                        else
                        {
                            query = query.Where(m => m.nombre.Contains(nombre));
                        }
                    }

                    if (!new UtilsHelper().UselessString(nickname_admin))
                    {
                        if (query == null)
                        {
                            query = context.Materials.Where(m => m.nickname_admin.Contains(nickname_admin));
                        }
                        else
                        {
                            query = query.Where(m => m.nickname_admin.Contains(nickname_admin));
                        }
                    }

                    if (!new UtilsHelper().UselessString(color))
                    {
                        if (query == null)
                        {
                            query = context.Materials.Where(m => m.color.Contains(color));
                        }
                        else
                        {
                            query = query.Where(m => m.color.Contains(color));
                        }
                    }

                    materiales = query.ToList();
                }
                List<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
                MaterialSelect select = new MaterialSelect();
                List<int> filtro = new List<int>
                {
                    select.nickname_admin
                };

                foreach (Material material in materiales)
                {
                    result.Add(material.GetInfo(new InfoFilter(filtro, GetInfoModes.Modes.Pessimistic)));
                }
                return result;
            }
            catch
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public long AddMaterial(Dictionary<string, object> data)
        {
            try
            {
                string nombre = (string)data["nombre"];
                string color = (string)data["color"];
                
                using (ChepiEntities context = new ChepiEntities())
                {

                    if (context.Materials.SingleOrDefault(m => m.nombre == nombre) != null)
                    {
                        throw new Exception("Material ya existe, nombre={ " + nombre + " }");
                    }

                    if (context.Materials.SingleOrDefault(m => m.color == color) != null)
                    {
                        throw new Exception("Color { " + color + " } ocupado !");
                    }
                }

                Material material;
                using (ChepiEntities context = new ChepiEntities())
                {
                    material = new Material(data);
                    context.Materials.Add(material);
                    context.SaveChanges();
                }
                return material.id;
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveMaterial(long id)
        {
            throw new NotImplementedException();
        }

        public void UpdateMaterial(Dictionary<string,object> data)
        {
            try
            {
                Material material = null;
                string nombre = null;
                string color = null;
                long id = (long)data["id"];

                using (ChepiEntities context = new ChepiEntities())
                {
                    material = context.Materials.SingleOrDefault(m => m.id == id);

                    if( material == null)
                    {
                        throw new Exception("Material "+id+" no existe");
                    }

                    if( data.ContainsKey("nombre"))
                    {
                        nombre = (string)data["nombre"];
                        if (context.Materials.SingleOrDefault(m => m.nombre == nombre) != null)
                        {
                            throw new Exception("Material ya existe, nombre={ " + nombre + " }");
                        }
                        material.nombre = nombre;
                    }
                    
                    if( data.ContainsKey("color"))
                    {
                        color = (string)data["color"];
                        if (context.Materials.SingleOrDefault(m => m.color == color) != null)
                        {
                            throw new Exception("Color { " + color + " } ocupado !");
                        }
                        material.color = color;
                    }

                    if(data.ContainsKey("estado"))
                    {
                        bool estado = (bool)data["estado"];
                        if( material.bloqueado != estado)
                        {
                            material.bloqueado = estado;
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
