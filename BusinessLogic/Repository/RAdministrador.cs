﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using System.Security.Cryptography;
using CrossCutConcerns.Utils;
using BusinessLogic.Controllers;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;

namespace BusinessLogic.Repository
{
    class RAdministrador 
    {
        public List<Dictionary<string, object>> GetAdministradoresAll()
        {
            try
            {
                List<Administrador> administradores = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    administradores = context.Administradors.ToList();
                }
                List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();

                AdminSelect selectAdmin = new AdminSelect();
                List<int> filtro = new List<int>
                {
                    selectAdmin.nickname,
                    selectAdmin.mail
                };

                foreach (Administrador admin in administradores)
                {
                    resultado.Add(admin.GetInfo(new InfoFilter(filtro, GetInfoModes.Modes.Optimistic)));
                }
                return resultado;
            }
            catch
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }
        
        public List<Dictionary<string,object>> GetAdministradoresFiltrados( Dictionary<string,object> data )
        {
            try
            {
                List<Administrador> admins;
                using (ChepiEntities context = new ChepiEntities())
                {
                    string nickname = null;
                    string mail = null;

                    if (data.ContainsKey("nickname"))
                    {
                        nickname = (string)data["nickname"];
                    }

                    if (data.ContainsKey("mail"))
                    {
                        mail = (string)data["mail"];
                    }

                    IQueryable<Administrador> query = null;

                    if (!new UtilsHelper().UselessString(nickname))
                    {
                        if (query == null)
                        {
                            query = context.Administradors.Where(a => a.nickname.Contains(nickname));
                        }
                        else
                        {
                            query = query.Where( a => a.nickname.Contains(nickname));
                        }
                    }

                    if (!new UtilsHelper().UselessString(mail))
                    {
                        if (query == null)
                        {
                            query = context.Administradors.Where(a => a.mail.Contains(mail));
                        }
                        else
                        {
                            query = query.Where(a => a.mail.Contains(mail));
                        }

                    }

                    admins = query.ToList();
                }
                List<Dictionary<string,object>> result = new List<Dictionary<string, object>>();

                AdminSelect selectAdmin = new AdminSelect();
                List<int> filtro = new List<int>
                {
                    selectAdmin.nickname,
                    selectAdmin.mail
                };

                foreach (Administrador admin in admins)
                {
                    result.Add(admin.GetInfo(new InfoFilter(filtro, GetInfoModes.Modes.Optimistic)));
                }
                return result;
            }
            catch
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }
        
        public Dictionary<string, object> GetAdministrador(string nickname)
        {
            try
            {
                Administrador admin = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    admin = context.Administradors.SingleOrDefault(s => s.nickname == nickname);
                }

                AdminSelect selectAdmin = new AdminSelect();
                List<int> filtro = new List<int>
                {
                    selectAdmin.nickname, 
                    selectAdmin.mail
                };

                return admin.GetInfo(new InfoFilter(filtro,GetInfoModes.Modes.Optimistic));
            }
            catch
            {
                throw new KeyNotFoundException("Administrador de nickname ={ " + nickname + " } NO fue encontrado.");
            }
        }

        public Dictionary<string, object> GetAdministradorByEmail(string mail)
        {
            try
            {
                Administrador admin = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    admin = context.Administradors.SingleOrDefault(s => s.mail == mail);
                }
                AdminSelect selectAdmin = new AdminSelect();
                List<int> filtro = new List<int>
                {
                    selectAdmin.nickname,
                    selectAdmin.mail
                };

                return admin.GetInfo(new InfoFilter(filtro, GetInfoModes.Modes.Optimistic));
            }
            catch
            {
                throw new Exception("Administrador de email ={ " + mail + " } NO fue encontrado.");
            }
        }

        public string AddAdministrador(Dictionary<string,object> data)
        {
            try
            {
                string nickname = (string)data["nickname"];
                Administrador aux = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    aux = context.Administradors.SingleOrDefault( a => a.nickname == nickname);
                }

                if ( aux != null)
                {
                    throw new Exception("Administrador ya existe");
                }
                Administrador admin = new Administrador(data);
                using (ChepiEntities context = new ChepiEntities())
                {
                    context.Administradors.Add(admin);
                    context.SaveChanges();
                }
                return admin.nickname;
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveAdministrador(string nickname)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    Administrador admin = context.Administradors.SingleOrDefault(s => s.nickname == nickname);
                    if (admin != null)
                    {
                        context.Administradors.Remove(admin);
                        context.SaveChanges();
                    }
                }
            }
            catch
            {
                throw new Exception("Administrador de nickname ={ " + nickname + " } NO fue borrado.");
            }
        }

        public void UpdateAdministrador(Dictionary<string, object> data)
        {
            string nickname = null;
            string mail = null;
            string contra = null;

            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    if (data.ContainsKey("nickname"))
                    {
                        nickname = (string)data["nickname"];
                    }

                    if (data.ContainsKey("mail"))
                    {
                        mail = (string)data["mail"];
                    }

                    if (data.ContainsKey("contra"))
                    {
                        contra = (string)data["contra"];
                    }

                    Administrador administrador = context.Administradors.SingleOrDefault( a => a.nickname == nickname );
                    if( administrador == null)
                    {
                        throw new KeyNotFoundException("Administrador de nickname ={ " + nickname + " } NO se encontró.");
                    }


                    if ( mail != null && administrador.mail != mail)
                    {
                        administrador.mail = mail;
                    }

                    if( contra != null)
                    {
                        string newContra;

                        using (MD5 md5Hash = MD5.Create())
                        {
                            newContra = new UtilsHelper().GetMd5Hash(md5Hash, contra);
                        }

                        if (administrador.contra != newContra )
                        {
                            using (MD5 md5Hash = MD5.Create())
                            {
                                administrador.contra = newContra;
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch(KeyNotFoundException ex)
            {
                throw ex;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string Login(Dictionary<string, object> data)
        {
            bool condition = false;
            string key = (string)data["key"];
            string value = (string)data["value"];
            string contra = (string)data["contra"];
            Administrador admin = null;

            switch (key)
            {
                case "nickname":
                    try
                    {
                        using (ChepiEntities context = new ChepiEntities())
                        {
                            admin = context.Administradors.SingleOrDefault( a => a.nickname == value);
                        }
                    }
                    catch
                    {
                        throw new Exception("Administrador de nickname ={ " + value + " } NO se encontró.");
                    }
                    break;
                case "mail":
                    try
                    {
                        using (ChepiEntities context = new ChepiEntities())
                        {
                            admin = context.Administradors.SingleOrDefault(a => a.mail == value);
                        }
                    }
                    catch
                    {
                        throw new Exception("Administrador de email ={ " + value + " } NO se encontró.");
                    }
                    break;
            }
            if( admin == null )
            {
                throw new UnauthorizedAccessException();
            }

            using (MD5 md5Hash = MD5.Create())
            {
                condition = new UtilsHelper().VerifyMd5Hash(md5Hash, contra, admin.contra);
            }
            if (condition)
            {
                return new ControllerToken().CreateJWT(new UserData(admin.nickname, new UserTypes().admin));
            }
            return null;
        }
    }
}
