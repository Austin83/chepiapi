﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Controllers;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;
using DataAccess;
using Newtonsoft.Json.Linq;

namespace BusinessLogic.Repository
{
    class RUsuario
    {

        public bool IsActive(string nickname)
        {
            try
            {
                Usuario usuario = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    usuario = context.Usuarios.SingleOrDefault(s => s.nickname == nickname);
                }
                return usuario.estado;

            }
            catch
            {
                throw new Exception("Usuario de nickname ={ " + nickname + " } NO fue encontrado.");
            }
        }

        public List<Dictionary<string,object>> GetUsuariosAll(string palabras, int? edad, int? type)
        {
            try
            {
                List<Usuario> listaUsuarios = new List<Usuario>();
                using (ChepiEntities context = new ChepiEntities())
                {
                    UserTypes userTypes = new UserTypes();

                    if( palabras == null && edad == null && type == null)
                    {
                        listaUsuarios = context.Usuarios.ToList();
                    }

                    if( palabras != null && edad != null && type != null)
                    {
                        palabras = palabras.ToLower();

                        listaUsuarios = context.Usuarios.Where(u => u.nickname.ToLower().Contains(palabras)).
                        Union(context.Usuarios.Where(u => u.nombre.ToLower().Contains(palabras)))
                        .Union(context.Usuarios.Where(u => u.apellido.ToLower().Contains(palabras)))
                        .Intersect(context.Usuarios.Where(u => u.edad == edad)).ToList();

                        List<Usuario> aux = listaUsuarios.ToList();

                        if (type == userTypes.n_reciclador)
                        {
                            foreach (Usuario user in aux)
                            {
                                if (user.GetUserType() != userTypes.reciclador)
                                {
                                    listaUsuarios.Remove(user);
                                }
                            }
                        }
                        if (type == userTypes.n_recolector)
                        {
                            foreach (Usuario user in aux)
                            {
                                if (user.GetUserType() != userTypes.recolector)
                                {
                                    listaUsuarios.Remove(user);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (palabras != null)
                        {
                            palabras = palabras.ToLower();
                            listaUsuarios = context.Usuarios.Where(u => u.nickname.ToLower().Contains(palabras)).
                                Union(context.Usuarios.Where(u => u.nombre.ToLower().Contains(palabras)))
                                .Union(context.Usuarios.Where(u => u.apellido.ToLower().Contains(palabras))).ToList();

                            if (edad != null)
                            {
                                listaUsuarios = listaUsuarios.Where(u => u.edad == edad).ToList();
                            }
                        }
                        else
                        {
                            listaUsuarios = context.Usuarios.ToList();

                            if (edad != null)
                            {
                                listaUsuarios = listaUsuarios.Where(u => u.edad == edad).ToList();
                            }
                        }

                        if (type != null)
                        {
                            List<Usuario> aux = new List<Usuario>();
                            if (edad == null && palabras == null)
                            {
                                listaUsuarios = context.Usuarios.ToList();
                                aux = listaUsuarios.ToList();
                                
                            }
                            else
                            {
                                aux = listaUsuarios.ToList();
                            }

                            if (type == userTypes.n_reciclador)
                            {
                                foreach (Usuario user in aux)
                                {
                                    if (user.GetUserType() != userTypes.reciclador)
                                    {
                                        listaUsuarios.Remove(user);
                                    }
                                }
                            }
                            if (type == userTypes.n_recolector)
                            {
                                foreach (Usuario user in aux)
                                {
                                    if (user.GetUserType() != userTypes.recolector)
                                    {
                                        listaUsuarios.Remove(user);
                                    }
                                }
                            }
                        }
                    }
                    
                    
                }
                List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();
                UsuarioSelect select = new UsuarioSelect();
                UbicacionSelect selectUbi = new UbicacionSelect();
                List<int> filtroUsuario = new List<int>
                {
                    select.imagen
                };
                List<int> filtroUbicacion = new List<int>
                {
                    selectUbi.direccion,
                    selectUbi.latitud,
                    selectUbi.longitud,
                    selectUbi.nombre
                };

                foreach (Usuario usuario in listaUsuarios)
                {
                    resultado.Add(usuario.GetInfo( new InfoFilter(filtroUsuario, GetInfoModes.Modes.Pessimistic), 
                                                    new InfoFilter(filtroUbicacion, GetInfoModes.Modes.Optimistic) ));
                }
                return resultado;
            }
            catch(Exception ex)
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public Usuario GetUsuario( string nickname )
        {
            try
            {
                Usuario usuario = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    usuario = context.Usuarios.SingleOrDefault( s => s.nickname == nickname );
                }
                return usuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Usuario GetUsuarioByEmail( string mail)
        {
            try
            {
                Usuario usuario = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    usuario = context.Usuarios.SingleOrDefault(s => s.mail == mail);
                }
                return usuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InactiveCheck(string nickname)
        {
            if (!IsActive(nickname))
            {
                throw new Exception("Inactive account !");
            }
        }

        public string LoginGoogle(Dictionary<string, object> data)
        {
            JObject googleToken = new ControllerToken().RequestGet("https://oauth2.googleapis.com/tokeninfo?id_token=" + (string)data["token"]);
            string email = (string)googleToken.GetValue("email");
            string given_name = (string)googleToken.GetValue("given_name");
            string family_name = (string)googleToken.GetValue("family_name");

            List<string> aux = new List<string>
            {
                email,
                (string)googleToken.GetValue("email_verified"),
                given_name,
                family_name
            };

            UtilsHelper helper = new UtilsHelper();
            if (new UtilsHelper().UselessStrings(aux))
            {
                throw new ArgumentException("Invalid arguments");
            }

            bool email_verified = bool.Parse( (string)googleToken.GetValue("email_verified") );

            if ( email_verified == false)
            {
                throw new UnauthorizedAccessException("Unconfirmed google account");
            }

            if( data.ContainsKey("tipo") && data.ContainsKey("edad") && 
                int.TryParse(data["tipo"].ToString(), out int tipo) && int.TryParse(data["tipo"].ToString(), out int edad))
            {
                UserTypes types = new UserTypes();
                if ( tipo == types.n_reciclador)
                {
                    Dictionary<string, object> nuevo = new Dictionary<string, object>
                    {
                        {"nickname", email.Split('@')[0] },
                        {"nombre", given_name },
                        {"apellido", family_name },
                        {"mail", email },
                        {"edad", edad }
                    };

                    new RReciclador().AddReciclador(nuevo);
                    return new ControllerToken().CreateJWT(new UserData((string)nuevo["nickname"], types.reciclador));
                }

                if( tipo == types.n_recolector)
                {
                    Dictionary<string, object> nuevo = new Dictionary<string, object>
                    {
                        {"nickname", email.Split('@')[0] },
                        {"nombre", given_name },
                        {"apellido", family_name },
                        {"mail", email },
                        {"edad", edad },
                        {"ubicacion", data["ubicacion"] }
                    };

                    new RRecolector().AddRecolector(nuevo);
                    return new ControllerToken().CreateJWT(new UserData((string)nuevo["nickname"], types.recolector));
                }
            }
            else
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    Usuario usuario = context.Usuarios.SingleOrDefault(u => u.mail == email);
                    if ( usuario == null )
                    {
                        throw new UnauthorizedAccessException("No user found");
                    }
                    if (usuario.estado == false)
                    {
                        throw new UnauthorizedAccessException("Bloquedo !");
                    }

                    return new ControllerToken().CreateJWT(new UserData(usuario.nickname, usuario.GetUserType()));
                }
                    
            }
            return null;
        }

        public string Login(Dictionary<string,object> login)
        {
            bool passIsValid = false;
            string contraHash = null;
            string nickname = null;

            string key = (string)login["key"];
            string value = (string)login["value"];
            string contra = (string)login["contra"];

            Usuario usuario = null;

            switch (key)
            {
                case "nickname":
                    usuario = this.GetUsuario(value);
                    if( usuario == null)
                    {
                        throw new UnauthorizedAccessException("Access denied !");
                    }
                    if( usuario.estado == false)
                    {
                        throw new UnauthorizedAccessException("Bloqued !");
                    }
                    contraHash = usuario.contra;
                    nickname = value;
                    break;
                case "email":
                    usuario = GetUsuarioByEmail(value);
                    if (usuario == null)
                    {
                        throw new UnauthorizedAccessException("Access denied !");
                    }
                    if (usuario.estado == false)
                    {
                        throw new UnauthorizedAccessException("Bloqued !");
                    }
                    contraHash = usuario.contra;
                    nickname = usuario.nickname;
                    break;
            }

            using (MD5 md5Hash = MD5.Create())
            {
                passIsValid = new UtilsHelper().VerifyMd5Hash(md5Hash, contra, contraHash);
            }
            if (passIsValid)
            {
                string type = usuario.GetUserType();

                if( type == new UserTypes().reciclador || type == new UserTypes().recolector)
                {
                    return new ControllerToken().CreateJWT(new UserData(nickname, type ));
                }

            }
            return null;
        }

        public void CambiarEstado(string key, string value, bool estado)
        {
            Usuario usuario = null;
            
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    if(key == "mail")
                    {
                        usuario = context.Usuarios.SingleOrDefault(s => s.mail == value);
                    }

                    if (key == "nickname")
                    {
                        usuario = context.Usuarios.SingleOrDefault(s => s.nickname == value);
                    }

                    if( usuario == null)
                    {
                        throw new KeyNotFoundException("Usuario de " + key + " ={ " + value + " } NO se encontró.");
                    }

                    if ( usuario.estado != estado )
                    {
                        usuario.estado = estado;
                        context.SaveChanges();
                    }
                }
            }
            catch(KeyNotFoundException ex)
            {
                throw ex;
            }
        }

        public void Bloquear(string nickname_bloqueador, string nickname_bloqueado)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    Usuario bloqueador = null;
                    Usuario bloqueado = null;
                    Bloqueo bloqueo = null;
                    bloqueador = context.Usuarios.SingleOrDefault(u => u.nickname == nickname_bloqueador);
                    bloqueado = context.Usuarios.SingleOrDefault(u => u.nickname == nickname_bloqueado);

                    if (bloqueado == null)
                    {
                        throw new KeyNotFoundException("Usuario de nickname ={ " + nickname_bloqueado + " } NO se encontró.");
                    }

                    if (bloqueador == null)
                    {
                        throw new KeyNotFoundException("Usuario de nickname ={ " + nickname_bloqueador + " } NO se encontró.");
                    }

                    bloqueo = context.Bloqueos.SingleOrDefault(b => b.nickname_bloqueador == bloqueador.nickname &&
                                                                        b.nickname_bloqueado == bloqueado.nickname);

                    if (bloqueo != null)
                    {
                        throw new KeyNotFoundException("Bloqueo ya existe");
                    }

                    bloqueo = new Bloqueo(bloqueador.nickname, bloqueado.nickname);
                    context.Bloqueos.Add(bloqueo);
                    context.SaveChanges();
                }
            }
            catch (KeyNotFoundException ex)
            {
                throw ex;
            }
        }

        public Dictionary<string,object> Desbloquear(string nickname_bloqueador, string nickname_bloqueado)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    Usuario bloqueador = null;
                    Usuario bloqueado = null;
                    Bloqueo bloqueo = null;
                    bloqueador = context.Usuarios.SingleOrDefault(u => u.nickname == nickname_bloqueador);
                    bloqueado = context.Usuarios.SingleOrDefault(u => u.nickname == nickname_bloqueado);

                    if (bloqueado == null)
                    {
                        throw new KeyNotFoundException("Usuario de nickname ={ " + nickname_bloqueado + " } NO se encontró.");
                    }

                    if (bloqueador == null)
                    {
                        throw new KeyNotFoundException("Usuario de nickname ={ " + nickname_bloqueador + " } NO se encontró.");
                    }

                    bloqueo = context.Bloqueos.SingleOrDefault(b => b.nickname_bloqueador == bloqueador.nickname &&
                                                                        b.nickname_bloqueado == bloqueado.nickname);

                    if (bloqueo == null)
                    {
                        throw new KeyNotFoundException("Bloqueo no existe");
                    }

                    context.Bloqueos.Remove(bloqueo);
                    context.SaveChanges();

                    bloqueo = context.Bloqueos.SingleOrDefault(b => b.nickname_bloqueador == bloqueador.nickname &&
                                                                        b.nickname_bloqueado == bloqueado.nickname);

                    if (bloqueo != null)
                    {
                        throw new KeyNotFoundException("Bloqueo sigue existiendo");
                    }
                }
                return new Dictionary<string, object>
                {
                    { "nickname_desbloqueador", nickname_bloqueador },
                    { "nickname_desbloqueado", nickname_bloqueado}
                };
            }
            catch (KeyNotFoundException ex)
            {
                throw ex;
            }
        }

        public Dictionary<string,object> GetBloqueo(string nickname_bloqueador, string nickname_bloqueado)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    Usuario bloqueador = null;
                    Usuario bloqueado = null;
                    Bloqueo bloqueo = null;
                    bloqueador = context.Usuarios.SingleOrDefault(u => u.nickname == nickname_bloqueador);
                    bloqueado = context.Usuarios.SingleOrDefault(u => u.nickname == nickname_bloqueado);

                    if (bloqueado == null)
                    {
                        throw new KeyNotFoundException("Usuario de nickname ={ " + nickname_bloqueado + " } NO se encontró.");
                    }

                    if (bloqueador == null)
                    {
                        throw new KeyNotFoundException("Usuario de nickname ={ " + nickname_bloqueador + " } NO se encontró.");
                    }

                    bloqueo = context.Bloqueos.SingleOrDefault(b => b.nickname_bloqueador == bloqueador.nickname &&
                                                                        b.nickname_bloqueado == bloqueado.nickname);

                    if (bloqueo == null)
                    {
                        throw new KeyNotFoundException("Bloqueo no existe");
                    }

                    return bloqueo.GetInfo();
                }
            }
            catch (KeyNotFoundException ex)
            {
                throw ex;
            }
        }

        public void UpdateUsuario(string nickname, Dictionary<string, object> data)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    Usuario usuario = null;
                    usuario = context.Usuarios.SingleOrDefault(u => u.nickname == nickname);

                    if (usuario == null)
                    {
                        throw new KeyNotFoundException("Usuario de nickname ={ " + nickname + " } NO se encontró.");
                    }
                    
                    string nombre = null;
                    string apellido = null;
                    byte? edad = null;
                    string imagen = null;

                    if (data.ContainsKey("nombre"))
                    {
                        nombre = (string)data["nombre"];
                    }

                    if (data.ContainsKey("apellido"))
                    {
                        apellido = (string)data["apellido"];
                    }

                    if (data.ContainsKey("edad"))
                    {
                        edad = Byte.Parse(data["edad"].ToString());
                    }

                    if (data.ContainsKey("imagen"))
                    {
                        imagen = (string)data["imagen"];
                    }

                    usuario.Update(nombre, apellido, edad, imagen);
                    context.SaveChanges();
                }
            }
            catch (KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
    }
}
