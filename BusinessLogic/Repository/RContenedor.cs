﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;
using DataAccess;
using Newtonsoft.Json.Linq;

namespace BusinessLogic.Repository
{
    public class RContenedor
    {
        public List<Dictionary<string,object>> GetContenedoresAll(UserData userdata, List<long> materiales = null )
        {
            try
            {
                List<Contenedor> contenedores = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    UserTypes userTypes = new UserTypes();
                    if (userdata.userType == userTypes.admin)
                    {
                        contenedores = context.Contenedors.ToList();
                    }

                    if (userdata.userType == userTypes.reciclador)
                    {
                        contenedores = context.Contenedors.Where( c => c.Material.bloqueado == false ).ToList();
                    }

                    if( materiales != null )
                    {
                        List<long> ids = contenedores.Select(c => c.Material.id).Distinct().ToList();
                        foreach( long id in ids)
                        {
                            if( ! materiales.Contains(id))
                            {
                                contenedores = contenedores.Where(c => c.Material.id != id).ToList();
                            }
                        }
                    }
                }
                List<Dictionary<string, object>> resultado = new List<Dictionary<string,object>>();

                ContenedorSelect selectContenedor = new ContenedorSelect();
                List<int> aux = new List<int>
                {
                    selectContenedor.id,
                    selectContenedor.id_material,
                    selectContenedor.lleno,
                    selectContenedor.ubicacion,
                    selectContenedor.alertas
                };
                InfoFilter filtroContenedor = new InfoFilter( aux, GetInfoModes.Modes.Optimistic);
                UbicacionSelect selectUbicacion = new UbicacionSelect();
                aux = new List<int>
                {
                    selectUbicacion.latitud,
                    selectUbicacion.longitud
                };
                InfoFilter filtroUbicacion = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                AlertaSelect selectAlerta = new AlertaSelect();
                aux = new List<int>
                {
                    selectAlerta.fecha_hora,
                    selectAlerta.nickname_reciclador
                };
                InfoFilter filtroAlerta = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                foreach (Contenedor con in contenedores)
                {
                    resultado.Add(con.GetInfo(filtroContenedor, filtroUbicacion,filtroAlerta ));
                }
                return resultado;
            }
            catch(Exception ex)
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public Dictionary<string,object> GetContenedor(long id)
        {
            try
            {
                Contenedor contenedor = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    contenedor = context.Contenedors.SingleOrDefault(s => s.id == id);
                }

                ContenedorSelect selectContenedor = new ContenedorSelect();
                List<int> aux = new List<int>
                {
                    selectContenedor.id,
                    selectContenedor.id_material,
                    selectContenedor.lleno,
                    selectContenedor.ubicacion
                };
                InfoFilter filtroContenedor = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                return contenedor.GetInfo(filtroContenedor);

            }
            catch
            {
                throw new KeyNotFoundException("Contenedor con id ={ " + id + " } NO fue encontrado.");
            }
        }

        public void MarcarContenedor(long id)
        {
            try
            {
                Contenedor contenedor = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    contenedor = context.Contenedors.SingleOrDefault(s => s.id == id);

                    if (contenedor == null)
                    {
                        throw new KeyNotFoundException("Contenedor " + id + " no existe !");
                    }

                    contenedor.lleno = true;
                    context.Alertas.RemoveRange(context.Alertas.Where(a => a.id_contenedor == id));
                    context.SaveChanges();
                }
            }
            catch
            {
                throw new KeyNotFoundException("Contenedor con id ={ " + id + " } NO fue encontrado.");
            }
        }

        public List<Dictionary<string, object>> GetContenedoresFiltrados(Dictionary<string, object> data)
        {
            try
            {
                List<Contenedor> contenedores;
                using (ChepiEntities context = new ChepiEntities())
                {
                    long? id = null;
                    string nickname_admin = null;
                    bool? lleno = null;

                    if (data.ContainsKey("nickname_admin"))
                    {
                        nickname_admin = (string)data["nickname_admin"];
                    }

                    if (data.ContainsKey("lleno"))
                    {
                        lleno = (bool)data["lleno"];
                    }

                    if (data.ContainsKey("id"))
                    {
                        id = (long)data["id"];
                    }

                    IQueryable<Contenedor> query = null;

                    if (id != null)
                    {
                        if (query == null)
                        {
                            query = context.Contenedors.Where(c => c.id == id);
                        }
                        else
                        {
                            query = query.Where(c => c.id == id);
                        }
                    }

                    if (!new UtilsHelper().UselessString(nickname_admin))
                    {
                        if (query == null)
                        {
                            query = context.Contenedors.Where(c => c.nickname_admin.Contains(nickname_admin));
                        }
                        else
                        {
                            query = query.Where(c => c.nickname_admin.Contains(nickname_admin));
                        }
                    }

                    if ( lleno != null )
                    {
                        if (query == null)
                        {
                            query = context.Contenedors.Where(c => c.lleno == lleno);
                        }
                        else
                        {
                            query = query.Where(c => c.lleno == lleno);
                        }
                    }

                    contenedores = query.ToList();
                }
                List<Dictionary<string,object>> result = new List<Dictionary<string,object>>();

                ContenedorSelect selectContenedor = new ContenedorSelect();
                List<int> aux = new List<int>
                {
                    selectContenedor.id,
                    selectContenedor.id_material,
                    selectContenedor.lleno,
                    selectContenedor.ubicacion
                };
                InfoFilter filtroContenedor = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                foreach (Contenedor contenedor in contenedores)
                {
                    result.Add(contenedor.GetInfo(filtroContenedor));
                }
                return result;
            }
            catch
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public List<Dictionary<string,object>> GetContenedoresByAdmin(string nickname)
        {
            try
            {
                List<Contenedor> listaContenedores = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    //listaContenedores = context.Contenedors.Select(s => s.nickname_admin == nickname).ToList();
                    listaContenedores = context.Contenedors.Where(s => s.nickname_admin == nickname).ToList();
                }
                List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();

                ContenedorSelect selectContenedor = new ContenedorSelect();
                List<int> aux = new List<int>
                {
                    selectContenedor.id,
                    selectContenedor.id_material,
                    selectContenedor.lleno,
                    selectContenedor.ubicacion
                };
                InfoFilter filtroContenedor = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                foreach (Contenedor con in listaContenedores)
                {
                    resultado.Add(con.GetInfo(filtroContenedor));
                }
                return resultado;
            }
            catch
            {
                throw new Exception("Admin de nickname ={ " + nickname + " } NO tiene contenedores.");
            }
        }

        public long AddContenedor(string nickname_admin, Dictionary<string,object> data)
        {
            try
            {
                bool? lleno = null;
                long? id_material = null;
                JObject ubicacionData = null;

                if (data.ContainsKey("lleno"))
                {
                    lleno = (bool)data["lleno"];
                }
                else
                {
                    throw new ArgumentNullException("lleno");
                }

                if (data.ContainsKey("id_material"))
                {
                    id_material = (long?)data["id_material"];
                }
                else
                {
                    throw new ArgumentNullException("lleno");
                }

                if (data.ContainsKey("ubicacion"))
                {
                    ubicacionData = (JObject)data["ubicacion"];
                }
                else
                {
                    throw new ArgumentNullException("ubicacion");
                }

                Dictionary<string, object> ubicacionDic = new Dictionary<string, object>();
                if (ubicacionData.TryGetValue("latitud", out JToken latitud))
                {
                    ubicacionDic.Add("latitud", (double)latitud);
                }
                if (ubicacionData.TryGetValue("longitud", out JToken longitud))
                {
                    ubicacionDic.Add("longitud", (double)longitud);
                }

                Contenedor contenedor;
                using (ChepiEntities context = new ChepiEntities())
                {
                    contenedor = new Contenedor(nickname_admin, (long)id_material ,(bool)lleno);
                    context.Contenedors.Add(contenedor);
                    ubicacionDic.Add("id_contenedor", contenedor.id);
                    context.Ubicacions.Add(new Ubicacion(ubicacionDic));
                    context.SaveChanges();
                }
                return contenedor.id;
            }
            catch(ArgumentNullException ex)
            {
                throw ex;
            }
        }

        public void RemoveContenedor(long id)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    List<Alerta> alertas = context.Alertas.Where(a => a.id_contenedor == id).ToList();
                    context.Alertas.RemoveRange(alertas);
                    Ubicacion ubicacion = context.Ubicacions.SingleOrDefault(u => u.id_contenedor == id);
                    context.Ubicacions.Remove(ubicacion);
                    Contenedor contenedor = context.Contenedors.SingleOrDefault(s => s.id == id);
                    context.Contenedors.Remove(contenedor);
                    context.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Contenedor con id ={ " + id + " } NO fue borrado.");
            }
        }

        public void UpdateContenedor(Dictionary<string, object> data)
        {
            long id = (long)data["id"];
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    Contenedor contenedor = context.Contenedors.SingleOrDefault(s => s.id == id);
                    if ( data.ContainsKey("lleno"))
                    {
                        contenedor.lleno = (bool)data["lleno"];
                    }
                    
                    if( data.ContainsKey("id_material"))
                    {
                        contenedor.id_material = (long)data["id_material"];
                    }

                    context.SaveChanges();
                }
            }
            catch
            {
                throw new Exception("Contenedor con id ={ " + id + " } NO se actualizó.");
            }
        }

        public List<Dictionary<string, object>> GetContenedoresAlertados(List<long> materiales = null)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    List<long> idContenedores = context.Alertas.Select(a => a.id_contenedor).Distinct().ToList();
                    List<Contenedor> listaContenedores = new List<Contenedor>();

                    foreach(long id in idContenedores)
                    {
                        listaContenedores.Add(context.Contenedors.SingleOrDefault(c => c.id == id));
                    }

                    listaContenedores = listaContenedores.OrderByDescending(c => c.Alertas.Count()).ToList();

                    if (materiales != null)
                    {
                        List<long> ids_materiales = listaContenedores.Select(c => c.Material.id).Distinct().ToList();

                        foreach (long id in ids_materiales)
                        {
                            if (!materiales.Contains(id))
                            {
                                listaContenedores = listaContenedores.Where(c => c.Material.id != id).ToList();
                            }
                        }
                    }
                    List<Dictionary<string, object>> contenedores = new List<Dictionary<string, object>>();
                    Dictionary<string, object> contenedor = null;

                    ContenedorSelect selectContenedor = new ContenedorSelect();

                    List<int> aux = new List<int>
                    {
                        selectContenedor.nickname_admin,
                        selectContenedor.material,
                        selectContenedor.admin
                    };

                    InfoFilter filtro_contenedor = new InfoFilter(aux, GetInfoModes.Modes.Pessimistic);

                    UbicacionSelect selectUbicacion = new UbicacionSelect();

                    aux = new List<int>
                    {
                        selectUbicacion.nickname_reciclador
                    };

                    InfoFilter filtro_ubicacion = new InfoFilter(aux, GetInfoModes.Modes.Pessimistic);

                    AlertaSelect selectAlerta = new AlertaSelect();

                    aux = new List<int>
                    {
                        selectAlerta.id_contenedor
                    };

                    InfoFilter filtro_alerta = new InfoFilter(aux, GetInfoModes.Modes.Pessimistic);

                    foreach (Contenedor con in listaContenedores)
                    {
                        contenedores.Add(con.GetInfo(filtro_contenedor, filtro_ubicacion, filtro_alerta));
                    }

                    return contenedores;
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Query Failed!");
            }
        }
    }
}
