﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Selects;
using CrossCutConcerns.Utils;
using DataAccess;
using Newtonsoft.Json.Linq;

namespace BusinessLogic.Repository
{
    class RRecolector : RUsuario
    {
        public List<Dictionary<string, object>> GetTop(string filtro, int? orden)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    List<Dictionary<string, object>> recolectores = new List<Dictionary<string, object>>();
                    int estado = (int)EstadosRecoleccion.Estados.completa;
                    int? recolecciones;
                    List<string> nombresRecolectores = null;
                    if( orden == null)
                    {
                        orden = (int)OrdenTopRecolector.Orden.puntaje;
                    }
                    if (filtro != null)
                    {
                        filtro = filtro.ToLower();
                        IEnumerable<Recolector> aux = context.Recolectors.Where(r => r.nickname.ToLower().Contains(filtro));
                        
                        if( orden == (int)OrdenTopRecolector.Orden.retiros)
                        {
                            nombresRecolectores = aux.OrderByDescending(
                            r => context.Recolector_Recoleccion.Count(rr => rr.nickname_recolector == r.nickname && rr.Recoleccion.estado == estado))
                            .ThenByDescending( r => r.puntaje ).Select(r => r.nickname).ToList();
                        }

                        if( orden == (int)OrdenTopRecolector.Orden.puntaje)
                        {
                            nombresRecolectores = aux.OrderByDescending(r => r.puntaje)
                            .ThenByDescending(r => context.Recolector_Recoleccion.Count(rr => rr.nickname_recolector == r.nickname && rr.Recoleccion.estado == estado))
                            .Select( r => r.nickname).ToList();
                        }
                    }
                    else
                    {
                        IEnumerable<Recolector> aux = context.Recolectors;

                        if (orden == (int)OrdenTopRecolector.Orden.retiros)
                        {
                            nombresRecolectores = aux.OrderByDescending(
                            r => context.Recolector_Recoleccion.Count(rr => rr.nickname_recolector == r.nickname && rr.Recoleccion.estado == estado))
                            .Select(r => r.nickname).ToList();
                        }

                        if (orden == (int)OrdenTopRecolector.Orden.puntaje)
                        {
                            nombresRecolectores = aux.OrderByDescending(r => r.puntaje)
                            .ThenByDescending(r => context.Recolector_Recoleccion.Count(rr => rr.nickname_recolector == r.nickname && rr.Recoleccion.estado == estado))
                            .Select(r => r.nickname).ToList();
                        }
                    }

                    foreach (string nickname in nombresRecolectores)
                    {
                        recolecciones = context.Recoleccions.Count(r => r.Recolector_Recoleccion.nickname_recolector == nickname && r.estado == estado);

                        if (recolecciones > 0)
                        {
                            recolectores.Add(new Dictionary<string, object>
                            {
                                { "nickname", nickname },
                                { "recolecciones", recolecciones},
                                { "puntaje", context.Recolectors.SingleOrDefault( r => r.nickname == nickname ).puntaje }
                            });
                        }
                    }

                    return recolectores;
                }
            }
            catch (Exception ex)
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public List<Dictionary<string, object>> GetRecolectoresAll()
        {
            try
            {
                List<Recolector> recolectores = null;
                
                using (ChepiEntities context = new ChepiEntities())
                {
                    recolectores = context.Recolectors.ToList();
                }
                List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();

                RecolectorSelect selectRecolector = new RecolectorSelect();
                UsuarioSelect selectUsuario = new UsuarioSelect();
                List<int> aux = new List<int>
                {
                    selectUsuario.nickname,
                    selectUsuario.nombre,
                    selectUsuario.apellido
                };
                InfoFilter filtroUsuario = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                aux = new List<int>
                {
                    selectRecolector.usuario
                };
                InfoFilter filtroRecolector = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                foreach (Recolector rec in recolectores)
                {
                    resultado.Add(rec.GetInfo(filtroRecolector, filtroUsuario));
                }
                return resultado;
            }
            catch
            {
                throw new KeyNotFoundException("Query failed !");
            }
        }

        public Dictionary<string, object> GetRecolector(string nickname, UserData userData)
        {
            try
            {
                Recolector recolector = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    recolector = context.Recolectors.SingleOrDefault(s => s.nickname == nickname);
                }

                if( nickname != userData.nickname)
                {
                    RecolectorSelect selectRecolector = new RecolectorSelect();
                    UsuarioSelect selectUsuario = new UsuarioSelect();
                    List<int> aux = new List<int>
                    {
                        selectUsuario.nickname,
                        selectUsuario.nombre,
                        selectUsuario.apellido
                    };
                    InfoFilter filtroUsuario = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                    aux = new List<int>
                    {
                        selectRecolector.usuario
                    };
                    InfoFilter filtroRecolector = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                    return recolector.GetInfo(filtroRecolector, filtroUsuario);
                }
                else
                {
                    return recolector.GetInfo();
                }
            }
            catch(Exception ex)
            {
                throw new KeyNotFoundException("Recolector de nickname ={ " + nickname + " } NO fue encontrado.");
            }
        }

        public List<Dictionary<string, object>> GetMisRuta(string nickname, int? tiempo)
        {
            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    List<Ruta> listaRutas = context.Rutas.Where(r => r.nickname_recolector == nickname).OrderBy( r => r.id).ToList();
                    List<Dictionary<string, object>> rutas = new List<Dictionary<string, object>>();

                    if (tiempo != null)
                    {
                        UtilsHelper utils = new UtilsHelper();
                        DateTime dateTime = new UtilsHelper().GetFiltroTiempo((int)tiempo);
                        if( tiempo == TiemposMisRecolecciones.dia)
                        {
                            listaRutas = listaRutas.Where(r => r.fecha == dateTime).ToList();
                        }
                        else
                        {
                            DateTime today = utils.EndOfDay(DateTime.Today);
                            listaRutas = listaRutas.Where(r => r.fecha >= dateTime && r.fecha <= today).ToList();
                        }
                    }

                    if ( listaRutas.Any())
                    {
                        RutaSelect rutaSelect = new RutaSelect();
                        InfoFilter filtroRuta = new InfoFilter(new List<int> { rutaSelect.nickname_recolector }, GetInfoModes.Modes.Pessimistic);
                        RecoleccionSelect recoleccionSelect = new RecoleccionSelect();
                        UbicacionSelect ubiSelect = new UbicacionSelect();
                        Recoleccion_MaterialSelect recoleccion_MaterialSelect = new Recoleccion_MaterialSelect();
                        List<int> aux = new List<int>
                        {
                             recoleccionSelect.id_ubicacion,
                             recoleccionSelect.estado,
                             recoleccionSelect.publicacion
                        };
                        InfoFilter filtroRecoleccion = new InfoFilter( aux, GetInfoModes.Modes.Pessimistic);
                        InfoFilter filtroUbicacion = new InfoFilter(new List<int>
                        {
                            ubiSelect.id,
                            ubiSelect.nombre,
                            ubiSelect.latitud,
                            ubiSelect.longitud,
                            ubiSelect.direccion
                        }, GetInfoModes.Modes.Optimistic);

                        InfoFilter filtroMaterial = new InfoFilter(new List<int>
                        {
                            recoleccion_MaterialSelect.id_material,
                            recoleccion_MaterialSelect.cantidad
                        }, GetInfoModes.Modes.Optimistic);

                        foreach (Ruta ruta in listaRutas)
                        {
                            rutas.Add(ruta.GetInfo(filtroRuta, filtroRecoleccion, filtroUbicacion, filtroMaterial));
                        }
                    }
                    
                    return rutas;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed !");
            }
        }

        public List<Dictionary<string, object>> GetMisRecolectores(string nickname_reciclador)
        {
            try
            {
                List<Dictionary<string,object>> recolectores = new List<Dictionary<string,object>>();
                List<string> nicknames_recolectores = new List<string>();
                using (ChepiEntities context = new ChepiEntities())
                {
                    List<Recoleccion> query = context.Recoleccions.Where( r => r.nickname_reciclador == nickname_reciclador ).ToList();
                    foreach( Recoleccion recoleccion in query)
                    {
                        if( recoleccion.Recolector_Recoleccion != null)
                        {
                            Recolector recolector = context.Recolectors.SingleOrDefault
                            (r => r.nickname == recoleccion.Recolector_Recoleccion.nickname_recolector);
                            if ( recolector.Usuario.estado && ! nicknames_recolectores.Contains(recolector.nickname) )
                            {
                                nicknames_recolectores.Add(recolector.nickname);
                            }
                        }
                    }

                    RecolectorSelect selectRecolector = new RecolectorSelect();
                    UsuarioSelect selectUsuario = new UsuarioSelect();
                    List<int> aux = new List<int>
                    {
                        selectUsuario.nickname,
                        selectUsuario.nombre,
                        selectUsuario.apellido
                    };
                    InfoFilter filtroUsuario = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                    aux = new List<int>
                    {
                        selectRecolector.usuario
                    };
                    InfoFilter filtroRecolector = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                    foreach (string nickname in nicknames_recolectores)
                    {
                        Recolector rec = context.Recolectors.SingleOrDefault(r => r.nickname == nickname);
                        Dictionary<string,object> data = rec.GetInfo(filtroRecolector, filtroUsuario);
                        if ( rec.Usuario.Bloqueado(nickname_reciclador) )
                        {
                            data.Add("bloqueado", true );
                        }
                        else
                        {
                            data.Add("bloqueado", false );
                        }
                        recolectores.Add(data);
                    }
                }
                return recolectores;

            }
            catch(Exception ex)
            {
                throw new KeyNotFoundException("Query failed !");
            }
        }

        public List<string> GetMisRecolectores(string nickname_reciclador, bool nicknames_only)
        {
            try
            {
                List<string> recolectores = new List<string>();
                List<string> nicknames_recolectores = new List<string>();
                using (ChepiEntities context = new ChepiEntities())
                {
                    List<Recoleccion> query = context.Recoleccions.Where(r => r.nickname_reciclador == nickname_reciclador).ToList();
                    foreach (Recoleccion recoleccion in query)
                    {
                        if (recoleccion.Recolector_Recoleccion != null)
                        {
                            Recolector recolector = context.Recolectors.SingleOrDefault
                            (r => r.nickname == recoleccion.Recolector_Recoleccion.nickname_recolector);
                            if (recolector.Usuario.estado && !nicknames_recolectores.Contains(recolector.nickname))
                            {
                                nicknames_recolectores.Add(recolector.nickname);
                            }
                        }
                    }

                    RecolectorSelect selectRecolector = new RecolectorSelect();
                    UsuarioSelect selectUsuario = new UsuarioSelect();
                    List<int> aux = new List<int>
                    {
                        selectUsuario.nickname
                    };
                    InfoFilter filtroUsuario = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                    aux = new List<int>
                    {
                        selectRecolector.usuario
                    };
                    InfoFilter filtroRecolector = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                    foreach (string nickname in nicknames_recolectores)
                    {
                        Recolector rec = context.Recolectors.SingleOrDefault(r => r.nickname == nickname);
                        if( rec != null)
                        {
                            recolectores.Add(rec.nickname);
                        }
                    }
                }
                return recolectores;

            }
            catch (Exception ex)
            {
                throw new KeyNotFoundException("Query failed !");
            }
        }

        public string AddRecolector(Dictionary<string,object> data)
        {
            Recolector recolector = null;
            try
            {
                JObject ubicacionData = null;
                string nickname;
                if (data.ContainsKey("nickname"))
                {
                    nickname = (string)data["nickname"];
                }
                else
                {
                    throw new ArgumentNullException("nickname");
                }

                Usuario aux = null;
                using (ChepiEntities context = new ChepiEntities())
                {
                    aux = context.Usuarios.SingleOrDefault(u => u.nickname == nickname);
                }

                if (aux != null)
                {
                    throw new Exception("Usuario ya existe");
                }

                using (ChepiEntities context = new ChepiEntities())
                {
                    Usuario usuario = new Usuario(data);
                    context.Usuarios.Add(usuario);
                    recolector = new Recolector(data);
                    context.Recolectors.Add(recolector);
                    recolector.Usuario = usuario;
                    usuario.Recolector = recolector;
                    if (data.ContainsKey("ubicacion"))
                    {
                        ubicacionData = (JObject)data["ubicacion"];
                    }
                    else
                    {
                        throw new ArgumentNullException("ubicacion");
                    }

                    Dictionary<string, object> ubicacionDic = new Dictionary<string, object>();
                    if (ubicacionData.TryGetValue("latitud", out JToken latitud))
                    {
                        ubicacionDic.Add("latitud", (double)latitud);
                    }
                    if (ubicacionData.TryGetValue("longitud", out JToken longitud))
                    {
                        ubicacionDic.Add("longitud", (double)longitud);
                    }
                    if (ubicacionData.TryGetValue("direccion", out JToken direccion))
                    {
                        ubicacionDic.Add("direccion", (string)direccion);
                    }
                    ubicacionDic.Add("nickname_recolector", recolector.nickname);
                    context.Ubicacions.Add(new Ubicacion(ubicacionDic));
                    context.SaveChanges();
                }
                return recolector.nickname;
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveRecolector(string nickname)
        {
            this.InactiveCheck(nickname);

            try
            {
                using (ChepiEntities context = new ChepiEntities())
                {
                    Recolector recolector = context.Recolectors.SingleOrDefault(s => s.nickname == nickname);
                    context.Recolectors.Remove(recolector);
                    Usuario usuario = context.Usuarios.SingleOrDefault(x => x.nickname == nickname);
                    context.Usuarios.Remove(usuario);
                    context.SaveChanges();
                }
            }
            catch
            {
                throw new Exception("Recolector de nickname ={ " + nickname + " } NO fue borrado.");
            }
        }
        
        public List<Dictionary<string, object>> GetRecolectoresFiltrados(Dictionary<string,object> data)
        {
            try
            {
                List<Recolector> recolectores;
                using (ChepiEntities context = new ChepiEntities())
                {
                    string nickname = null;
                    string nombre = null;
                    string apellido = null;
                    string mail = null;
                    byte? edad = null;
                    long? creditos = null;
                    bool? estado = null;
                    string puntajeText = null;
                    double? puntaje = null;
                    if (data.ContainsKey("puntaje"))
                    {
                        puntajeText = data["puntaje"].ToString();
                        if (!puntajeText.Contains("."))
                        {
                            puntajeText = puntajeText + ".00";
                        }

                        bool cond = double.TryParse(puntajeText, out double number);
                        if (cond)
                        {
                            puntaje = number;
                        }
                    }

                    if (data.ContainsKey("nickname"))
                    {
                        nickname = (string)data["nickname"];
                    }

                    if (data.ContainsKey("nombre"))
                    {
                        nombre = (string)data["nombre"];
                    }

                    if (data.ContainsKey("apellido"))
                    {
                        apellido = (string)data["apellido"];
                    }

                    if (data.ContainsKey("mail"))
                    {
                        mail = (string)data["mail"];
                    }

                    if (data.ContainsKey("edad"))
                    {
                        edad = Byte.Parse(data["edad"].ToString());
                    }

                    if (data.ContainsKey("creditos"))
                    {
                        creditos = (long)data["creditos"];
                    }

                    if (data.ContainsKey("estado"))
                    {
                        estado = (bool)data["estado"];
                    }

                    IQueryable<Recolector> query = null;

                    if (!new UtilsHelper().UselessString(nickname))
                    {
                        if (query == null)
                        {
                            query = context.Recolectors.Where(r => r.nickname.Contains(nickname));
                        }
                        else
                        {
                            query = query.Where(r => r.nickname.Contains(nickname));
                        }
                    }

                    if (!new UtilsHelper().UselessString(nombre))
                    {
                        if (query == null)
                        {
                            query = context.Recolectors.Where(r => r.Usuario.nombre.Contains(nombre));
                        }
                        else
                        {
                            query = query.Where(r => r.Usuario.nombre.Contains(nombre));
                        }
                    }

                    if (!new UtilsHelper().UselessString(apellido))
                    {
                        if (query == null)
                        {
                            query = context.Recolectors.Where(r => r.Usuario.apellido.Contains(apellido));
                        }
                        else
                        {
                            query = query.Where(r => r.Usuario.apellido.Contains(apellido));
                        }
                    }

                    if (!new UtilsHelper().UselessString(mail))
                    {
                        if (query == null)
                        {
                            query = context.Recolectors.Where(r => r.Usuario.mail.Contains(mail));
                        }
                        else
                        {
                            query = query.Where(r => r.Usuario.mail.Contains(mail));
                        }
                    }

                    if (edad != null)
                    {
                        if (query == null)
                        {
                            query = context.Recolectors.Where(s => s.Usuario.edad == edad);
                        }
                        else
                        {
                            query = query.Where(s => s.Usuario.edad == edad);
                        }
                    }

                    if (creditos != null)
                    {
                        if (query == null)
                        {
                            query = context.Recolectors.Where(s => s.Usuario.creditos == creditos);
                        }
                        else
                        {
                            query = query.Where(s => s.Usuario.creditos == creditos);
                        }
                    }

                    if (estado != null)
                    {
                        if (query == null)
                        {
                            query = context.Recolectors.Where(s => s.Usuario.estado == estado);
                        }
                        else
                        {
                            query = query.Where(s => s.Usuario.estado == estado);
                        }
                    }

                    if (puntaje != null)
                    {
                        if (query == null)
                        {
                            query = context.Recolectors.Where(s => s.puntaje == puntaje);
                        }
                        else
                        {
                            query = query.Where(s => s.puntaje == puntaje);
                        }
                    }

                    recolectores = query.ToList();
                }

                List<Dictionary<string, object>> resultado = new List<Dictionary<string, object>>();

                RecolectorSelect selectRecolector = new RecolectorSelect();
                UsuarioSelect selectUsuario = new UsuarioSelect();
                List<int> aux = new List<int>
                {
                    selectUsuario.nickname,
                    selectUsuario.nombre,
                    selectUsuario.apellido
                };
                InfoFilter filtroUsuario = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);
                aux = new List<int>
                {
                    selectRecolector.usuario
                };
                InfoFilter filtroRecolector = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                foreach (Recolector recolector in recolectores)
                {
                    resultado.Add(recolector.GetInfo(filtroRecolector, filtroUsuario));
                }
                return resultado;
            }
            catch
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public List<Dictionary<string, object>> GetMisRecolecciones(string nickname, int tiempo)
        {
            try
            {
                DateTime dateTime = new UtilsHelper().GetFiltroTiempo(tiempo);
                
                using (ChepiEntities context = new ChepiEntities())
                {
                    List<Recolector_Recoleccion> query = context.Recolector_Recoleccion.Where
                                                           (r => r.nickname_recolector == nickname && 
                                                           r.Recoleccion.fecha >= dateTime && 
                                                           r.Recoleccion.estado == (int)EstadosRecoleccion.Estados.completa )
                                                           .OrderByDescending( r => r.Recoleccion.fecha ).ToList();
                    List<Dictionary<string, object>> recolecciones = new List<Dictionary<string, object>>();

                    if (query.Any())
                    {
                        UbicacionSelect ubicacionSelect = new UbicacionSelect();
                        RecoleccionSelect recoleccionSelect = new RecoleccionSelect();
                        Recoleccion_MaterialSelect materialSelect = new Recoleccion_MaterialSelect();
                        List<int> aux = new List<int>
                        {
                            ubicacionSelect.nickname_reciclador,
                            ubicacionSelect.nombre,
                            ubicacionSelect.direccion,
                            ubicacionSelect.id
                        };
                        InfoFilter filtroUbicacion = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                        aux = new List<int>
                        {
                            recoleccionSelect.estado
                        };
                        InfoFilter filtroRecoleccion = new InfoFilter(aux, GetInfoModes.Modes.Pessimistic);
                        aux = new List<int>
                        {
                            materialSelect.id_material,
                            materialSelect.cantidad
                        };
                        InfoFilter filtroMaterial = new InfoFilter(aux, GetInfoModes.Modes.Optimistic);

                        foreach (Recolector_Recoleccion r in query)
                        {
                            Recoleccion recoleccion = context.Recoleccions.SingleOrDefault(j => j.id == r.id_recoleccion);
                            recolecciones.Add( recoleccion.GetInfo(filtroRecoleccion, filtroUbicacion, filtroMaterial) );
                        }
                    }

                    return recolecciones;
                }
            }
            catch(Exception ex)
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }

        public List<Dictionary<string, object>> GetCantidadMateriales(string nickname, int tiempo)
        {
            try
            {
                DateTime dateTime = new UtilsHelper().GetFiltroTiempo(tiempo);
                using (ChepiEntities context = new ChepiEntities())
                {
                    List<Dictionary<string, object>> materiales = new List<Dictionary<string, object>>();
                    List<long> idMateriales = new List<long>();
                    
                    IQueryable<Recoleccion_Material> recolecciones_materiales = context.Recoleccion_Material.
                        Where(r => r.Recoleccion.Recolector_Recoleccion.nickname_recolector == nickname &&
                        r.Recoleccion.estado == (int)EstadosRecoleccion.Estados.completa &&
                        r.Recoleccion.fecha >= dateTime).OrderBy( r => r.id_material);
                    
                    var algo = recolecciones_materiales.Select( rm => new { rm.id_material, rm.cantidad }).ToList();
                
                    foreach( long id in algo.Select( rm => rm.id_material ).Distinct())
                    {
                        materiales.Add(new Dictionary<string, object>
                        {
                            { "id_material", id },
                            { "cantidad", algo.Where( rm => rm.id_material == id ).Select( rm => rm.cantidad).Sum() }
                        });
                    }
                    return materiales;
                }
            }
            catch (Exception ex)
            {
                throw new KeyNotFoundException("Query Failed !");
            }
        }


    }
}
