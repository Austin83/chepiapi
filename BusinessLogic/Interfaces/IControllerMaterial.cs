﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    interface IControllerMaterial
    {
        List<Dictionary<string,object>> GetMaterialesAll(string userType);
        List<Dictionary<string, object>> GetMaterialesFiltrados(Dictionary<string, object> data);
        long AddMaterial(Dictionary<string, object> data);
        void RemoveMaterial(long id);
        long UpdateMaterial(Dictionary<string, object> data);
        void EditarMaterial(Dictionary<string, object> data);
        List<Dictionary<string, object>> GetTopMateriales();
    }
}
