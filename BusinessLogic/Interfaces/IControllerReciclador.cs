﻿using CrossCutConcerns.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IControllerReciclador
    {
        List<Dictionary<string, object>> GetRecicladoresAll();
        List<Dictionary<string, object>> GetRecicladoresFiltrados(Dictionary<string, object> data);
        Dictionary<string, object> GetReciclador(string nickname, UserData userData);
        string AddReciclador(Dictionary<string, object> data);
        void RemoveReciclador(string nickname);
        void UpdateReciclador(Dictionary<string, object> data);
    }
}
