﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Utils;

namespace BusinessLogic.Interfaces
{
    public interface IControllerUbicacion
    {
        List<Dictionary<string,object>> GetUbicacionesAll();
        Dictionary<string, object> GetUbicacion(long id);
        List<Dictionary<string, object>> GetUbicacionesFiltradas(Dictionary<string, object> data);
        long AddUbicacion(Dictionary<string, object> dto);
        void RemoveUbicacion(long id_contenedor);
        void RemoveUbicacion(Dictionary<string,object> data, string nickname_reciclador);
        void UpdateUbicacion(Dictionary<string, object> dto);
        double GetDistance(Coordinates a, Coordinates b); 
    }
}
