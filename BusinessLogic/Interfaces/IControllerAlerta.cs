﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    interface IControllerAlerta
    {
        Dictionary<string, object> AddAlerta(long id, string nickname_reciclador);
        Dictionary<string, object> GetAlerta(long id, string nickname_reciclador);
        List<Dictionary<string, object>> GetAlertasAll();
        List<Dictionary<string, object>> GetAlertasFiltradas(Dictionary<string,object> data);
    }
}
