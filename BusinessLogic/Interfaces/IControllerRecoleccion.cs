﻿using CrossCutConcerns.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IControllerRecoleccion
    {
        List<Dictionary<string,object>> GetRecoleccionesAll();
        List<Dictionary<string, object>> GetRecoleccionesFiltradas(Dictionary<string, object> data);
        List<Dictionary<string, object>> GetRecoleccionesReciclador(string nickname, int? tiempo = null,
                                                                    int? estado = null, string nickname_recolector = null);
        List<Dictionary<string, object>> GetRecoleccionesRecolectorCompletas(string nickname);
        Dictionary<string, object> GetRecoleccion(long id);
        long AddRecoleccion(Dictionary<string, object> data);
        void RemoveRecoleccion(long id);
        long UpdateRecoleccion(Dictionary<string, object> data);
        void ReservarRecoleccion(Dictionary<string, object> data);
        long[] GetRuta(string nickname, long[] ids_recolecciones);
        long[] ConfirmarRuta(string nickname, Dictionary<string, object> data);
        void LiberarRecolecciones(Dictionary<string,object> data);
        List<Dictionary<string, object>> GetRecoleccionesReservables(string nickname, string fecha, string materialesText, string hora);
        void RateRecoleccion(Dictionary<string,object> data, string nickname_reciclador);
    }
}
