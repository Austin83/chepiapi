﻿using CrossCutConcerns.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IControllerContenedor
    {
        List<Dictionary<string, object>> GetContenedoresAll(UserData userdata, string materialesText);
        Dictionary<string, object> GetContenedor(long id);
        List<Dictionary<string, object>> GetContenedoresByAdmin(string nickname);
        long AddContenedor(string nickname_admin, Dictionary<string,object> data);
        void RemoveContenedor(long id);
        void UpdateContenedor(Dictionary<string,object> data);
        List<Dictionary<string, object>> GetContenedoresFiltrados(Dictionary<string, object> data);
        List<Dictionary<string, object>> GetContenedoresAlertados(string materialesText);
        void LlenarAlertado(Dictionary<string,object> data);
    }
}
