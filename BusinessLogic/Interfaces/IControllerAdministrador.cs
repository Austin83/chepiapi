﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IControllerAdministrador
    {
        List<Dictionary<string, object>> GetAdministradoresAll();
        List<Dictionary<string, object>> GetAdministradoresFiltrados(Dictionary<string, object> data);
        Dictionary<string, object> GetAdministrador(string nickname);
        string AddAdministrador(Dictionary<string,object> data);
        void RemoveAdministrador(string nickname);
        void UpdateAdministrador(Dictionary<string, object> data);
        string Login( Dictionary<string, object> data );
        void Bloquear(Dictionary<string, object> data);
        void Desbloquear(Dictionary<string, object> data);
    }
}
