﻿using CrossCutConcerns.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IControllerRecolector
    {
        List<Dictionary<string, object>> GetRecolectoresAll();
        List<Dictionary<string, object>> GetRecolectoresFiltrados(Dictionary<string,object> data);
        List<Dictionary<string, object>> GetMisRecolectores(string nickname_reciclador);
        List<string> GetMisRecolectores(string nickname_reciclador, bool nicknames_only);
        List<Dictionary<string, object>> GetMisRecolecciones(string nickname, int tiempo);
        List<Dictionary<string, object>> GetCantidadMateriales(string nickname, int tiempo);
        List<Dictionary<string, object>> GetRecoleccionesRuta(string nickname, int? tiempo);
        Dictionary<string, object> GetRecolector(string nickname, UserData userData);
        string AddRecolector(Dictionary<string,object> data);
        void RemoveRecolector(string nickname);
        void UpdateRecolector(Dictionary<string, object> data);
        List<Dictionary<string, object>> GetTop(string nickname, int? orden);
    }
}
