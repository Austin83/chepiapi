﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interfaces;
using BusinessLogic.Repository;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Utils;

namespace BusinessLogic.Controllers
{
    public class ControllerRecolector : IControllerRecolector
    {
        public string AddRecolector(Dictionary<string, object> data)
        {
            return new RRecolector().AddRecolector(data);
        }

        public List<Dictionary<string, object>> GetMisRecolecciones(string nickname, int tiempo)
        {
            if (new UtilsHelper().UselessString(nickname))
            {
                throw new KeyNotFoundException("nickname is null !");
            }
            if (new UtilsHelper().AnyNulls( new List<object> { tiempo }))
            {
                throw new KeyNotFoundException("nickname is null !");
            }
            return new RRecolector().GetMisRecolecciones(nickname, tiempo);
        }

        public List<Dictionary<string, object>> GetCantidadMateriales(string nickname, int tiempo)
        {
            if (new UtilsHelper().UselessString(nickname))
            {
                throw new KeyNotFoundException("nickname is null !");
            }
            if (new UtilsHelper().AnyNulls(new List<object> { tiempo }))
            {
                throw new KeyNotFoundException("nickname is null !");
            }
            return new RRecolector().GetCantidadMateriales(nickname, tiempo);
        }

        public List<Dictionary<string, object>> GetMisRecolectores(string nickname_reciclador)
        {
            return new RRecolector().GetMisRecolectores(nickname_reciclador);
        }

        public Dictionary<string, object> GetRecolector(string nickname, UserData userData)
        {
            return new RRecolector().GetRecolector(nickname, userData);
        }

        public List<Dictionary<string, object>> GetRecolectoresAll()
        {
            return new RRecolector().GetRecolectoresAll();
        }

        public List<Dictionary<string, object>> GetRecolectoresFiltrados(Dictionary<string, object> data)
        {
            return new RRecolector().GetRecolectoresFiltrados(data);
        }

        public void RemoveRecolector(string nickname)
        {
            throw new NotImplementedException();
        }

        public void UpdateRecolector(Dictionary<string, object> data)
        {
            throw new NotImplementedException();
        }
        
        public List<Dictionary<string, object>> GetRecoleccionesRuta(string nickname, int? tiempo)
        {
            if( tiempo < TiemposMisRecolecciones.rango[0] || tiempo > TiemposMisRecolecciones.rango[1])
            {
                throw new Exception("Tiempo fuera de rango");
            }

            return new RRecolector().GetMisRuta(nickname, tiempo);
        }

        public List<string> GetMisRecolectores(string nickname_reciclador, bool nicknames_only)
        {
            return new RRecolector().GetMisRecolectores(nickname_reciclador, nicknames_only);
        }

        public List<Dictionary<string, object>> GetTop(string nickname, int? orden)
        {
            return new RRecolector().GetTop(nickname, orden);
        }
    }
}
