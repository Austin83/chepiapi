﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interfaces;
using BusinessLogic.Repository;
using CrossCutConcerns.Utils;
using Newtonsoft.Json.Linq;

namespace BusinessLogic.Controllers
{
    public class ControllerContenedor : IControllerContenedor
    {
        public long AddContenedor(string nickname_admin, Dictionary<string,object> data)
        {
            return new RContenedor().AddContenedor( nickname_admin, data);
        }

        public Dictionary<string, object> GetContenedor(long id)
        {
            return new RContenedor().GetContenedor(id);
        }

        public List<Dictionary<string, object>> GetContenedoresAll(UserData userdata, string materialesText)
        {
            if( materialesText != null)
            {
                string[] parts = null;
                if (!materialesText.Contains(','))
                {
                    parts = new string[1];
                    parts[0] = materialesText;
                }
                else
                {
                    parts = materialesText.Split(',');
                }
                List<long> ids = new List<long>();

                foreach( string part in parts)
                {
                    ids.Add(long.Parse(part));
                }

                return new RContenedor().GetContenedoresAll(userdata, ids);
            }
            
            return new RContenedor().GetContenedoresAll(userdata);
        }

        public List<Dictionary<string, object>> GetContenedoresByAdmin(string nickname)
        {
            return new RContenedor().GetContenedoresByAdmin(nickname);
        }

        public List<Dictionary<string, object>> GetContenedoresFiltrados(Dictionary<string, object> data)
        {
            return new RContenedor().GetContenedoresFiltrados(data);
        }

        public void RemoveContenedor(long id)
        {
            new RContenedor().RemoveContenedor(id);
        }

        public void UpdateContenedor(Dictionary<string,object> data)
        {
            UtilsHelper helper = new UtilsHelper();

            if (helper.AnyNulls(helper.CreateObjectList(data, new List<string> { "id_material" })))
            {
                throw new ArgumentException("Invalid numeric arguments");
            }
            new ControllerContenedor().GetContenedor((long)data["id"]);
            new RContenedor().UpdateContenedor(data);
        }

        public List<Dictionary<string, object>> GetContenedoresAlertados(string materialesText)
        {
            if (materialesText != null)
            {
                string[] parts = null;
                if (!materialesText.Contains(','))
                {
                    parts = new string[1];
                    parts[0] = materialesText;
                }
                else
                {
                    parts = materialesText.Split(',');
                }
                List<long> ids_materiales = new List<long>();

                foreach (string part in parts)
                {
                    ids_materiales.Add(long.Parse(part));
                }
                return new RContenedor().GetContenedoresAlertados(ids_materiales);
            }

            return new RContenedor().GetContenedoresAlertados();
        }

        public void LlenarAlertado(Dictionary<string, object> data)
        {
            if( ! data.ContainsKey("id"))
            {
                throw new ArgumentException("La clave id no está");
            }

            if( ! long.TryParse(data["id"].ToString(), out long result))
            {
                throw new ArgumentException("id formato inválido");
            }

            new RContenedor().MarcarContenedor(result);
        }
    }
}
