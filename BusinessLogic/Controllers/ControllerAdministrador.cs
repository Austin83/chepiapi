﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interfaces;
using BusinessLogic.Repository;
using CrossCutConcerns.Utils;
using CrossCutConcerns.Constants;
using System.Security.Cryptography;

namespace BusinessLogic.Controllers
{
    public class ControllerAdministrador : IControllerAdministrador
    {
        public string AddAdministrador(Dictionary<string,object> data)
        {
            return new RAdministrador().AddAdministrador(data);
        }

        public Dictionary<string, object> GetAdministrador(string nickname)
        {
            return new RAdministrador().GetAdministrador(nickname);
        }

        public List<Dictionary<string,object>> GetAdministradoresAll()
        {
            return new RAdministrador().GetAdministradoresAll();
        }
        
        public List<Dictionary<string,object>> GetAdministradoresFiltrados( Dictionary<string,object> data )
        {
            return new RAdministrador().GetAdministradoresFiltrados( data );
        }
        
        public void RemoveAdministrador(string nickname)
        {
            new RAdministrador().RemoveAdministrador(nickname);
        }

        public void UpdateAdministrador(Dictionary<string, object> data)
        {
            new RAdministrador().UpdateAdministrador( data );
        }

        public Dictionary<string, object> GetAdministradorByEmail(string email)
        {
            return new RAdministrador().GetAdministradorByEmail(email);
        }

        public string Login( Dictionary<string,object> data )
        {
            return new RAdministrador().Login(data);
        }

        public void Bloquear(Dictionary<string,object> data)
        {
            string key = (string)data["key"];
            string value = (string)data["value"];

            List<string> palabras = new List<string>
            {
                key,
                value
            };

            if (new UtilsHelper().UselessStrings(palabras))
            {
                throw new KeyNotFoundException("Argumentos incompletos");
            }
            new RUsuario().CambiarEstado(key, value, false);
        }

        public void Desbloquear(Dictionary<string,object> data)
        {
            string key = (string)data["key"];
            string value = (string)data["value"];
            List<string> palabras = new List<string>
                {
                     key,
                     value
                };

            if (new UtilsHelper().UselessStrings(palabras))
            {
                throw new KeyNotFoundException("Argumentos incompletos");
            }

            new RUsuario().CambiarEstado(key, value, true);
        }
    }
}
