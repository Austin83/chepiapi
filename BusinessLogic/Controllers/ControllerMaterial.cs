﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interfaces;
using BusinessLogic.Repository;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Utils;

namespace BusinessLogic.Controllers
{
    public class ControllerMaterial : IControllerMaterial
    {
        public long AddMaterial(Dictionary<string, object> data)
        {
            UtilsHelper helper = new UtilsHelper();
            if (helper.UselessStrings(helper.CreateStringList(data, new List<string> { "nombre", "color" })))
            {
                throw new ArgumentException("Invalid string arguments");
            }

            if (helper.AnyNulls(helper.CreateObjectList(data, new List<string> { "cuantificable" })))
            {
                throw new ArgumentException("Invalid numeric arguments");
            }
            return new RMaterial().AddMaterial(data);
        }

        public void EditarMaterial(Dictionary<string, object> data)
        {
            UtilsHelper helper = new UtilsHelper();
            if (helper.UselessStrings(helper.CreateStringList(data, new List<string> { "nombre", "color" })))
            {
                throw new ArgumentException("Invalid string arguments");
            }

            if (helper.AnyNulls(helper.CreateObjectList(data, new List<string> { "id","estado" })))
            {
                throw new ArgumentException("Invalid numeric arguments");
            }
            new RMaterial().UpdateMaterial(data);
        }

        public Dictionary<string, object> GetMaterial(long id)
        {
            return new RMaterial().GetMaterial(id);
        }

        public List<Dictionary<string, object>> GetMaterialesAll(string userType)
        {
            UserTypes types = new UserTypes();
            if ( userType == types.reciclador || userType == types.recolector)
            {
                return new RMaterial().GetMaterialesFiltrados(new Dictionary<string, object> { { "bloqueado", false } });
            }
            return new RMaterial().GetMaterialesAll();
        }

        public List<Dictionary<string, object>> GetMaterialesFiltrados(Dictionary<string, object> data)
        {
            return new RMaterial().GetMaterialesFiltrados(data);
        }

        public List<Dictionary<string, object>> GetTopMateriales()
        {
            return new RMaterial().GetTop();
        }

        public void RemoveMaterial(long id)
        {
            throw new NotImplementedException();
        }

        public long UpdateMaterial(Dictionary<string, object> data)
        {
            throw new NotImplementedException();
        }
    }
}
