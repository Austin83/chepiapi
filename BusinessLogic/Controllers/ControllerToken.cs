﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json;
using System.Security.Claims;

using CrossCutConcerns.Constants;
using CrossCutConcerns.Utils;
using BusinessLogic.Repository;
using System.Net.Http.Headers;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;

namespace BusinessLogic.Controllers
{
    public class ControllerToken
    {
        private ClaimsIdentity CreateClaimsIdentities(UserData userData)
        {
            ClaimsIdentity claimsIdentity = new ClaimsIdentity();
            claimsIdentity.AddClaim(new Claim(ClaimTypes.UserData, JsonConvert.SerializeObject(userData)));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.UserData, JsonConvert.SerializeObject(userData)));
            return claimsIdentity;
        }

        public string CreateJWT(UserData userData)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            ClaimsIdentity claims = CreateClaimsIdentities(userData);

            // Create JWToken
            JwtSecurityToken token = tokenHandler.CreateJwtSecurityToken(issuer: "https://chepi.com",
                audience: "https://chepi.com",
                subject: claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(60.00),
                signingCredentials:
                new SigningCredentials(
                    new SymmetricSecurityKey(
                        Encoding.Default.GetBytes(new ChepiConstants().GetPrivateKey())),
                        SecurityAlgorithms.HmacSha256Signature));

            return tokenHandler.WriteToken(token);
        }

        public string CreateInvalidJWT(UserData userData)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            ClaimsIdentity claims = CreateClaimsIdentities(userData);

            // Create JWToken
            JwtSecurityToken token = tokenHandler.CreateJwtSecurityToken(issuer: "https://chepi.com",
                audience: "https://chepi.com",
                subject: claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(5.00),
                signingCredentials:
                new SigningCredentials(
                    new SymmetricSecurityKey(
                        Encoding.Default.GetBytes(new ChepiConstants().invalid_testing_key)),
                        SecurityAlgorithms.HmacSha256Signature));

            return tokenHandler.WriteToken(token);
        }

        public SecurityToken DecipherToken( HttpRequestHeaders headers )
        {
            if( headers.Authorization == null )
            {
                throw new UnauthorizedAccessException("Access denied");
            }

            var validations = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.Default.GetBytes(new ChepiConstants().GetPrivateKey())),
                ValidateIssuer = false,
                ValidateAudience = false
            };

            string hashedToken = headers.Authorization.Parameter;
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            ClaimsPrincipal token = handler.ValidateToken(hashedToken, validations, out SecurityToken tokenSecure);
            return tokenSecure;
        }

        public UserData DecipherTokenPayload(string hashedToken)
        {
            var validations = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.Default.GetBytes(new ChepiConstants().GetPrivateKey())),
                ValidateIssuer = false,
                ValidateAudience = false
            };

            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var token = handler.ValidateToken(hashedToken, validations, out var tokenSecure);
            string userData = token.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.UserData).Value;
            return JsonConvert.DeserializeObject<UserData>(userData);
        }

        public void GrantCommonAccess(HttpRequestHeaders headers)
        {
            try
            {
                if (headers.Authorization == null)
                {
                    throw new UnauthorizedAccessException("Access denied !");
                }

                string hashedtoken = headers.Authorization.Parameter;
                if (string.IsNullOrEmpty(hashedtoken))
                {
                    throw new UnauthorizedAccessException("Access denied !");
                }
                UserData data = new ControllerToken().DecipherTokenPayload(headers.Authorization.Parameter);
                if (data.userType != new UserTypes().reciclador && data.userType != new UserTypes().recolector)
                {
                    throw new UnauthorizedAccessException("Access denied !");
                }
            }
            catch
            {
                throw new UnauthorizedAccessException("Access denied !");
            }
        }

        public void GrantBasicAccess(HttpRequestHeaders headers)
        {
            if (this.DecipherToken(headers) == null)
            {
                throw new UnauthorizedAccessException("Access denied");
            }
        }

        public void GrantAdminAccess(HttpRequestHeaders headers)
        {
            try
            {
                if (headers.Authorization == null)
                {
                    throw new UnauthorizedAccessException("Access denied !");
                }

                string hashedtoken = headers.Authorization.Parameter;
                if (string.IsNullOrEmpty(hashedtoken))
                {
                    throw new UnauthorizedAccessException("Access denied !");
                }
                UserData data = new ControllerToken().DecipherTokenPayload(headers.Authorization.Parameter);
                try
                {
                    new RAdministrador().GetAdministrador(data.nickname);
                }
                catch(KeyNotFoundException)
                {
                    throw new UnauthorizedAccessException("Access denied !");
                }

                if (data.userType != new UserTypes().admin)
                {
                    throw new UnauthorizedAccessException("Access denied !");
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void GrantRecicladorAccess(HttpRequestHeaders headers)
        {
            if (headers.Authorization == null)
            {
                throw new UnauthorizedAccessException("Access denied !");
            }

            UserData tokenData = DecipherTokenPayload(headers.Authorization.Parameter);

            if (tokenData.userType != new UserTypes().reciclador)
            {
                throw new UnauthorizedAccessException("Access Denied");
            }

            try
            {
                new RReciclador().GetReciclador(tokenData.nickname, tokenData);
            }
            catch
            {
                throw new UnauthorizedAccessException("Access Denied");
            }
        }

        public void GrantRecolectorAccess(HttpRequestHeaders headers)
        {
            if (headers.Authorization == null)
            {
                throw new UnauthorizedAccessException("Access denied !");
            }

            UserData tokenData = DecipherTokenPayload(headers.Authorization.Parameter);

            if (tokenData.userType != new UserTypes().recolector)
            {
                throw new UnauthorizedAccessException("Access Denied");
            }

            try
            {
                new RRecolector().GetRecolector(tokenData.nickname, tokenData);
            }
            catch
            {
                throw new UnauthorizedAccessException("Access Denied");
            }
        }

        public void IdentityCheck(HttpRequestHeaders headers, string target)
        {
            UserData tokenData = DecipherTokenPayload(headers.Authorization.Parameter);
            if (tokenData.nickname != target)
            {
                throw new Exception("Access Denied! Identity Theft!");
            }
        }

        public JObject RequestGet(string uri)
        {
            string aux = null;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                aux = reader.ReadToEnd();
            }
            return JObject.Parse(aux);
        }
    }
}
