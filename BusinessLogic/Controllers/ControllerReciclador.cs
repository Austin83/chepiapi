﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interfaces;
using BusinessLogic.Repository;
using CrossCutConcerns.Utils;

namespace BusinessLogic.Controllers
{
    public class ControllerReciclador : IControllerReciclador
    {
        public string AddReciclador(Dictionary<string, object> data)
        {
            return new RReciclador().AddReciclador(data);
        }

        public Dictionary<string, object> GetReciclador(string nickname, UserData userData)
        {
            return new RReciclador().GetReciclador(nickname, userData);
        }

        public List<Dictionary<string, object>> GetRecicladoresAll()
        {
            return new RReciclador().GetRecicladoresAll();
        }

        public List<Dictionary<string, object>> GetRecicladoresFiltrados(Dictionary<string, object> data)
        {
            return new RReciclador().GetRecicladoresFiltrados(data);
        }

        public void RemoveReciclador(string nickname)
        {
            throw new NotImplementedException();
        }

        public void UpdateReciclador(Dictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        public List<Dictionary<string, object>> GetTopRecicladores(string nickname)
        {
            return new RReciclador().GetTopRecicladores(nickname);
        }
    }
}
