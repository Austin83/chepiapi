﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interfaces;
using BusinessLogic.Repository;

namespace BusinessLogic.Controllers
{
    public class ControllerAlerta : IControllerAlerta
    {
        public Dictionary<string, object> AddAlerta(long id, string nickname_reciclador)
        {
            return new RAlerta().AddAlerta(id, nickname_reciclador);
        }

        public Dictionary<string, object> GetAlerta(long id, string nickname_reciclador)
        {
            return new RAlerta().GetAlerta(id, nickname_reciclador);
        }

        public List<Dictionary<string, object>> GetAlertasAll()
        {
            return new RAlerta().GetAlertasAll();
        }

        public List<Dictionary<string, object>> GetAlertasFiltradas(Dictionary<string, object> data)
        {
            return new RAlerta().GetAlertasFiltradas(data);
        }
    }
}
