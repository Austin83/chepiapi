﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Utils;

namespace BusinessLogic.Controllers
{
    public class ControllerEmail
    {
        public void SendEmail(EmailData emailData)
        {
            using (MailMessage mail = new MailMessage())
            {
                ChepiConstants constants = new ChepiConstants();

                mail.From = new MailAddress( constants.smtp.from );
                foreach( string to in emailData.to)
                {
                    mail.To.Add( to );
                }
                mail.Subject = emailData.subject;
                mail.Body = emailData.body;
                mail.IsBodyHtml = true;
                //mail.Attachments.Add(new Attachment("C:\\file.zip"));

                using (SmtpClient smtp = new SmtpClient( constants.smtp.host , constants.smtp.port ))
                {
                    smtp.Credentials = new NetworkCredential(constants.smtp.username, constants.smtp.password);
                    smtp.EnableSsl = true;
                    smtp.Send(mail);
                }
            }
        }
    }
}
