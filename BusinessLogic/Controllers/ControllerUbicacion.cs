﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interfaces;
using BusinessLogic.Repository;
using CrossCutConcerns.Utils;
using GeoCoordinatePortable;

namespace BusinessLogic.Controllers
{
    public class ControllerUbicacion : IControllerUbicacion
    {
        public long AddUbicacion(Dictionary<string, object> data)
        {


            return new RUbicacion().AddUbicacion(data);
        }

        public Dictionary<string, object> GetUbicacion(long id)
        {
            return new RUbicacion().GetUbicacion(id);
        }

        public List<Dictionary<string,object>> GetUbicacionesAll()
        {
            return new RUbicacion().GetUbicacionesAll();
        }

        public void RemoveUbicacion(long id_contenedor)
        {
            new RUbicacion().RemoveUbicacion(id_contenedor);
        }

        public void UpdateUbicacion(Dictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        public double GetDistance(Coordinates a, Coordinates b)
        {
            return new RUbicacion().GetDistance(a, b);
        }

        public List<Dictionary<string, object>> GetUbicacionesFiltradas(Dictionary<string, object> data)
        {
            return new RUbicacion().GetUbicacionesFiltradas(data);
        }

        public void RemoveUbicacion(Dictionary<string,object> data, string nickname_reciclador)
        {
            UtilsHelper helper = new UtilsHelper();

            if (helper.AnyNulls(helper.CreateObjectList(data, new List<string> { "id" })))
            {
                throw new ArgumentException("Invalid numeric arguments");
            }

            new RUbicacion().RemoveUbicacion( (long)data["id"], nickname_reciclador);
        }
    }
}
