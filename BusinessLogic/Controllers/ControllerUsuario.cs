﻿using BusinessLogic.Repository;
using CrossCutConcerns.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Controllers
{
    public class ControllerUsuario
    {
        public string Login(Dictionary<string,object> login)
        {
            return new RUsuario().Login(login);
        }

        public string LoginGoogle(Dictionary<string, object> data)
        {
            if( ! data.ContainsKey("token"))
            {
                throw new UnauthorizedAccessException("No Google token !");
            }

            UtilsHelper helper = new UtilsHelper();
            if ( helper.UselessString((string)data["token"]))
            {
                throw new KeyNotFoundException("Credenciales incompletas");
            }
            return new RUsuario().LoginGoogle(data);
        }

        public List<Dictionary<string,object>> GetUsuariosAll(string palabras, int? edad, int? type)
        {
            return new RUsuario().GetUsuariosAll(palabras, edad, type);
        }

        public bool ExisteUsuario( string key, string value )
        {
            if( key == "nickname" )
            {
                return new RUsuario().GetUsuario(value) != null;
            }
            if( key == "mail" )
            {
                return new RUsuario().GetUsuarioByEmail(value) != null;
            }
            return false;
        }

        public void Bloquear(string nickname_bloqueador, string nickname_bloqueado)
        {
            new RUsuario().Bloquear(nickname_bloqueador, nickname_bloqueado);
        }

        public Dictionary<string, object> Desbloquear(string nickname_bloqueador, string nickname_bloqueado)
        {
            return new RUsuario().Desbloquear(nickname_bloqueador, nickname_bloqueado);
        }

        public Dictionary<string,object> GetBloqueo(string nickname_bloqueador, string nickname_bloqueado)
        {
            return new RUsuario().GetBloqueo(nickname_bloqueador, nickname_bloqueado);
        }

        public void UpdateUsuario( string nickname, Dictionary<string,object> data)
        {
            UtilsHelper helper = new UtilsHelper();
            if ( helper.UselessStrings( helper.CreateStringList(data, new List<string> { "nombre", "apellido" } )))
            {
                throw new ArgumentException("Invalid string arguments");
            }

            if (helper.AnyNulls( helper.CreateObjectList(data, new List<string> { "edad" } )))
            {
                throw new ArgumentException("Invalid numeric arguments");
            }

            new RUsuario().UpdateUsuario(nickname, data);
        }
    }
}
