﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Interfaces;
using BusinessLogic.Repository;
using CrossCutConcerns.Constants;
using CrossCutConcerns.Utils;
using Newtonsoft.Json.Linq;

namespace BusinessLogic.Controllers
{
    public class ControllerRecoleccion : IControllerRecoleccion
    {
        public long AddRecoleccion(Dictionary<string, object> data)
        {
            return new RRecoleccion().AddRecoleccion(data);
        }
        
        public Dictionary<string, object> GetRecoleccion(long id)
        {
            return new RRecoleccion().GetRecoleccion(id);
        }

        public List<Dictionary<string, object>> GetRecoleccionesAll()
        {
            return new RRecoleccion().GetRecoleccionesAll();
        }

        public List<Dictionary<string, object>> GetRecoleccionesFiltradas(Dictionary<string, object> data)
        {
            return new RRecoleccion().GetRecoleccionesFiltradas(data);
        }

        public List<Dictionary<string, object>> GetRecoleccionesReciclador(string nickname, int? tiempo,
                                                                    int? estado, string nickname_reco)
        {
            if( tiempo != null)
            {
                if ( tiempo < TiemposMisRecolecciones.rango[0]+1  || tiempo > TiemposMisRecolecciones.rango[1])
                {
                    throw new ArgumentException("Tiempo fuera de rango");
                }
            }

            if ( estado != null )
            {
                if( estado < EstadosRecoleccion.rango[0] || estado > EstadosRecoleccion.rango[1])
                {
                    throw new ArgumentException("Estado fuera de rango");
                }
            }

            if (new UtilsHelper().EmptyStrings(new List<string> { nickname_reco }))
            {
                throw new ArgumentException("Invalid string arguments");
            }

            return new RRecoleccion().GetRecoleccionesReciclador(nickname, tiempo, estado, nickname_reco);
        }

        public List<Dictionary<string, object>> GetRecoleccionesRecolectorCompletas(string nickname)
        {
            return new RRecoleccion().GetRecoleccionesRecolectorCompletas(nickname);
        }

        public long[] GetRuta( string nickname, long[] ids_recolecciones)
        {
            return new RRecoleccion().GetRuta(nickname, ids_recolecciones); 
        }

        public long[] ConfirmarRuta(string nickname, Dictionary<string, object> data)
        {
            UtilsHelper helper = new UtilsHelper();
            if (helper.UselessStrings(helper.CreateStringList(data, new List<string> { "fecha" })))
            {
                throw new ArgumentException("Invalid string arguments");
            }

            if (helper.AnyNulls(helper.CreateObjectList(data, new List<string> { "ids", "hora" })))
            {
                throw new ArgumentException("Invalid numeric arguments");
            }
            return new RRecoleccion().ConfirmarRuta(nickname, data);
        }

        public void RemoveRecoleccion(long id)
        {
            throw new NotImplementedException();
        }

        public void ReservarRecoleccion(Dictionary<string, object> data)
        {
            new RRecoleccion().ReservarRecolecion(data);
        }

        public long UpdateRecoleccion(Dictionary<string, object> data)
        {
            throw new NotImplementedException();
        }

        public List<Dictionary<string, object>> GetTopResiduos()
        {
            return new RRecoleccion().GetTopResiduos();
        }

        public void LiberarRecolecciones(Dictionary<string, object> data)
        {
            JArray parts = (JArray)data["ids"];
            long[] ids = new long[parts.Count()];
            for (int i = 0; i < parts.Count(); i++)
            {
                ids[i] = (long)parts[i];
            }
            new RRecoleccion().LiberarRecolecciones(ids);
        }

        public List<Dictionary<string, object>> GetRecoleccionesReservables(string nickname, string fecha, string materialesText, string hora)
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            long[] materiales = null;

            if ( fecha != null)
            {
                data.Add("fecha", fecha);
            }

            if( hora != null )
            {
                data.Add("hora", hora);
            }

            if (materialesText != null)
            {
                string[] parts = null;
                if (!materialesText.Contains(','))
                {
                    parts = new string[1];
                    parts[0] = materialesText;
                }
                else
                {
                    parts = materialesText.Split(',');
                }

                materiales = new long[parts.Count()];
                for (int i = 0; i < parts.Count(); i++)
                {
                    materiales[i] = long.Parse(parts[i]);
                }
            }

            if( materiales != null && materiales.Any())
            {
                data.Add("materiales", materiales);
            }
            return new RRecoleccion().GetRecoleccionesReservables(nickname, data);
        }

        public void RateRecoleccion(Dictionary<string, object> data, string nickname_reciclador)
        {
            UtilsHelper helper = new UtilsHelper();

            if (helper.AnyNulls(helper.CreateObjectList(data, new List<string> { "id", "calificacion" })))
            {
                throw new ArgumentException("Invalid numeric arguments");
            }

            long id_recoleccion = (long)data["id"];
            int calificacion = int.Parse(data["calificacion"].ToString());
            ChepiConstants constans = new ChepiConstants();

            new RRecoleccion().GetRecoleccion(id_recoleccion);

            if( calificacion < constans.puntaje_bounds[0] || calificacion > constans.puntaje_bounds[1])
            {
                throw new ArgumentException("Invalid numeric arguments");
            }

            new RRecoleccion().RateRecoleccion(id_recoleccion, nickname_reciclador ,calificacion);
        }
    }
}
