﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutConcerns.Utils
{
    public struct Coordinates
    {
        public readonly double latitude, longitude;

        public Coordinates(double latitude, double longitude)
        {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public override string ToString()
        {
            return "{" +
                "Latitude:" + latitude + 
                "Longitude:" + longitude +
                "}";
        }
    }
}
