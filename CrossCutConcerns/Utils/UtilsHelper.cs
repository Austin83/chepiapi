﻿using CrossCutConcerns.Constants;
using GeoCoordinatePortable;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CrossCutConcerns.Utils
{
    public class UtilsHelper
    {
        public string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        public bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Check for null, empty or whitespaceful string
        public bool UselessStrings(List<string> palabras)
        {
            foreach (string palabra in palabras)
            {
                if (palabra == null || Regex.Replace(palabra, @"\s+", "") == "")
                {
                    return true;
                }
            }
            return false;
        }

        //Check for null, empty or whitespaceful string
        public bool EmptyStrings(List<string> palabras)
        {
            foreach (string palabra in palabras)
            {
                if (palabra == null)
                    continue;
                if (Regex.Replace(palabra, @"\s+", "") == "")
                {
                    return true;
                }
            }
            return false;
        }

        public bool UselessString(string palabra)
        {
            if (palabra == null || Regex.Replace(palabra, @"\s+", "") == "")
            {
                return true;
            }
            return false;
        }

        public bool AnyNulls(List<object> list)
        {
            foreach( Object obj in list)
            {
                if( obj == null)
                {
                    return true;
                }
            }
            return false;
        }

        public bool InRange(double value, double min, double max)
        {
            return value <= max && value >= min;
        }

        public Dictionary<string,object> CreateErrorDictionary(string error)
        {
            return new Dictionary<string, object>
            {
                { "error", error }
            };
        }

        public Dictionary<string, object> CreateSuccessfulDictionary(object data)
        {
            return new Dictionary<string, object>
            {
                { "success", true },
                { "data", data }
            };
        }

        public Dictionary<string, object> CreateSuccessfulDictionary()
        {
            return new Dictionary<string, object>
            {
                { "success", true }
            };
        }

        public List<string> CreateStringList(Dictionary<string, object> data, List<string> claves)
        {
            List<string> resultado = new List<string>();
            foreach( String clave in claves)
            {
                if( data.ContainsKey(clave))
                {
                    resultado.Add( (string)data[clave] );
                }
            }
            return resultado;
        }

        public List<object> CreateObjectList( Dictionary<string, object> data, List<string> claves)
        {
            List<object> resultado = new List<object>();
            foreach (String clave in claves)
            {
                if (data.ContainsKey(clave))
                {
                    resultado.Add(data[clave]);
                }
            }
            return resultado;
        }

        public DateTime EndOfDay(DateTime some_date)
        {
            return some_date.Date.AddDays(1).AddTicks(-1);
        }

        public DateTime GetFiltroTiempo( int tiempo)
        {
            if (tiempo >= TiemposMisRecolecciones.rango[0] && tiempo <= TiemposMisRecolecciones.rango[1])
            {
                if( tiempo == TiemposMisRecolecciones.dia)
                {
                    return DateTime.Today;
                }

                if( tiempo == TiemposMisRecolecciones.semana)
                {
                    return DateTime.Today.AddDays(-7);
                }

                if( tiempo == TiemposMisRecolecciones.mes)
                {
                    return DateTime.Today.AddMonths(-1);  
                }

                if( tiempo == TiemposMisRecolecciones.trimestre)
                {
                    return DateTime.Today.AddMonths(-3);
                }

                if( tiempo == TiemposMisRecolecciones.anio)
                {
                    return DateTime.Today.AddYears(-1);
                }
            }
            else
            {
                throw new ArgumentException("tiempo inválido");
            }
            return DateTime.MinValue;
        }
    }
}
