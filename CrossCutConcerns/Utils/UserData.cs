﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutConcerns.Utils
{
    public struct UserData
    {
        public readonly string nickname, userType;

        public UserData(string nickname, string userType)
        {
            this.nickname = nickname;
            this.userType = userType;
        }
    }
}
