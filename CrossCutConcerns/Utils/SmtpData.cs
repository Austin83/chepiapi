﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutConcerns.Utils
{
    public struct SmtpData
    {
        public readonly string from, host, username, password;
        public readonly int port;

        public SmtpData( string from, string host, int port , string username, string password )
        {
            this.from = from;
            this.host = host;
            this.port = port;
            this.username = username;
            this.password = password;
        }
    }
}
