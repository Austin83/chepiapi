﻿using CrossCutConcerns.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutConcerns.Utils
{
    public class InfoFilter
    {
        public readonly List<int> filter;
        public readonly GetInfoModes.Modes mode;

        public InfoFilter(List<int> filter, GetInfoModes.Modes mode)
        {
            this.filter = filter ?? throw new ArgumentNullException("filter");
            this.mode = mode;
        }
    }
}
