﻿using GeoCoordinatePortable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutConcerns.Utils
{
    public class UbicacionHelper
    {
        public double GetDistance(Coordinates a, Coordinates b)
        {
            string msg = null;
            try
            {
                GeoCoordinate geo_a = new GeoCoordinate(a.latitude, a.longitude);
                GeoCoordinate geo_b = new GeoCoordinate(b.latitude, b.longitude);
                msg = geo_a.ToString() +
                    geo_b.ToString();

                return geo_a.GetDistanceTo(geo_b);
            }
            catch (Exception)
            {
                throw new Exception(msg);
            }
        }
    }
}
