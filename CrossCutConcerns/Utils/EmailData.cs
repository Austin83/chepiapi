﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutConcerns.Utils
{
    public struct EmailData
    {
        public readonly string subject, body;
        public readonly string[] to;
        
        public EmailData( string subject, string body, string[] to)
        {
            this.subject = subject;
            this.body = body;
            this.to = to;
        }
    }
}
