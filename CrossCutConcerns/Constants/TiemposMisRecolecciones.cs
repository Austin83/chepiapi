﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutConcerns.Constants
{
    public class TiemposMisRecolecciones
    {
        public static readonly int dia = 0, semana = 1, mes = 2, trimestre = 3, anio = 4;
        public static readonly int[] rango = { 0, 4 };
    }
}
