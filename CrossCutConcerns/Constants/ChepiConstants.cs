﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossCutConcerns.Utils;

namespace CrossCutConcerns.Constants
{
    public class ChepiConstants
    {
        private readonly string private_key_location;
        private readonly string smtp_pass_location;
        private readonly string user;
        private readonly string smtp_from = "chepinet@gmail.com";
        private readonly string smtp_host = "smtp.gmail.com";
        private readonly int smtp_port = 587;
        private readonly string smtp_user = "chepinet";

        public string GetPrivateKey() { return System.IO.File.ReadAllText(this.private_key_location); }
        public readonly string invalid_testing_key = "J6k2eVCTXDp5b97u6gNH5GaaqHDxCmzz2wv3PRPFRsuW2UavK8LGPRauC4VSeaetKTMtVmVzAC8fh8Psvp8PFybEvpYnULHfRpM8TA2an7GFehrLLvawVJdSRqh2unCnWehhh2SJMMg5bktRRapA8EGSgQUV8TCafqdSEHNWnGXTjjsMEjUpaxcADDNZLSYPMyPSfp6qe5LMcd5S9bXH97KeeMGyZTS2U8gp3LGk2kH4J4F3fsytfpe9H9qKwgjb";
        public readonly SmtpData smtp;

        public readonly string OAuth_client_ID = "761100940267-95fb24424bn9f4h9eg58r47fcpi8vk9f.apps.googleusercontent.com";
        public readonly string OAuth_client_secret = "4O_HsLXJXX5VeGY7BS5NDBaN";
        public readonly string OAuth_redirect = "http://localhost:37627/api/tokens/google";
        public readonly int cookie_life = 4;
        public readonly string api_url = "http://192.168.2.102";
        public readonly double maxLat = 90;
        public readonly double minLat = -90;
        public readonly double maxLng = 180;
        public readonly double minLng = -180;
        public readonly int[] puntaje_bounds = new int[] { 1, 5 };

        public ChepiConstants()
        {
            user = "agust";
            private_key_location = @"C:\Users\" + user + "\\Documents\\Chepi_private\\.key.txt";
            smtp_pass_location = @"C:\Users\" + user + "\\Documents\\Chepi_private\\.pass.txt";
            smtp = new SmtpData( this.smtp_from, this.smtp_host, this.smtp_port, this.smtp_user, System.IO.File.ReadAllText(this.smtp_pass_location));
        }
    }
}
