﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutConcerns.Constants
{
    public class UserTypes
    {
        public readonly string recolector, reciclador, admin;
        public readonly int n_reciclador = 0, n_recolector = 1; 
        public UserTypes()
        {
            recolector = "Recolector";
            reciclador = "Reciclador";
            admin = "Admin";
        }
    }
}
